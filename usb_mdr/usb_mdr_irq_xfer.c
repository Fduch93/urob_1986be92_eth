#include <MDR32Fx.h>
#include <string.h>
#include "TICK.h"
#include "usbd_hal.h"
#include "usbd_endpoint0.h"
#include "usbd_log.h"

void USBD_IRQ_HandleTransactionDone(void);
static void USBD_IRQ_HandleEndpointTransactionDone(uint8_t epIdx);
static void USBD_IRQ_HandleEndpointSetup(uint8_t epIdx);
static void USBD_IRQ_HandleEndpointIn(uint8_t epIdx);
static void USBD_IRQ_HandleEndpointOut(uint8_t epIdx);

uint8_t epLastCtrl[4] = {0};

void USBD_IRQ_HandleTransactionDone(void)
{
	for (int epIdx = 0; epIdx < 4; epIdx++)
	{
		if ((MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPEN) &&
			USBD_HasEndpointReadyChanged(epIdx))
		{
			USBD_IRQ_HandleEndpointTransactionDone(epIdx);
		}
	}
}

static void USBD_IRQ_HandleEndpointTransactionDone(uint8_t epIdx)
{
	if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCCRCERR)
	{
		USBD_LOG("USBD EP%d transaction CRC error", epIdx);
	}
	else if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCBSERR)
	{
		USBD_LOG("USBD EP%d transaction STUFF error", epIdx);
	}
	else if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCRXOF)
	{
		USBD_LOG("USBD EP%d transaction RX_OVERFLOW error", epIdx);
	}
	else if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCRXTO)
	{
		USBD_LOG("USBD EP%d transaction RX_TIMEOUT error", epIdx);
	}
	else if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCSTALLSENT)
	{
		USBD_LOG("USBD EP%d STALL response sent", epIdx);

		#define USB_SEP_TS_SETUP	0
		#define USB_SEP_TS_IN		1
		#define USB_SEP_TS_OUT		2
		switch (MDR_USB->USB_SEP[epIdx].TS)
		{
			case USB_SEP_TS_SETUP:
				USBD_IRQ_HandleEndpointSetup(epIdx);
				MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;
//				MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;
			break;
			case USB_SEP_TS_IN:
			case USB_SEP_TS_OUT:
			break;
			default:
				break;
		}
	}
	else
	{
//		if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCNAKSENT)
//		{
//			switch (MDR_USB->USB_SEP[epIdx].NTS)
//			{
//				case 0:	LOG("USBD EP%d NAK was sent on SETUP request", epIdx); break;
//				case 1:	LOG("USBD EP%d NAK was sent on IN request", epIdx); break;
//				case 2:	LOG("USBD EP%d NAK was sent on OUT request", epIdx); break;
//				default: LOG("USBD EP%d NAK was sent on WTF request", epIdx); break;
//			}
//		}
		
		#define USB_SEP_TS_SETUP	0
		#define USB_SEP_TS_IN		1
		#define USB_SEP_TS_OUT		2
		switch (MDR_USB->USB_SEP[epIdx].TS)
		{
			case USB_SEP_TS_SETUP:
				USBD_IRQ_HandleEndpointSetup(epIdx);
//				MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;
			break;
			case USB_SEP_TS_IN:
				MDR_USB->USB_SEP_FIFO[epIdx].TXFDC = 1;
				if (MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCACKRXED)
				{
					USBD_IRQ_HandleEndpointIn(epIdx);
				}
				else
				{
					USBD_LOG("USBD EP%d IN transaction done, but ACK received?!", epIdx);
				}
			break;
			case USB_SEP_TS_OUT:
				USBD_IRQ_HandleEndpointOut(epIdx);
//				MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;
			break;
			default:
				break;
		}
	}
}

static void USBD_IRQ_HandleEndpointSetup(uint8_t epIdx)
{
	USBD_Device_T *device = USBD_GetDevice();
	USBD_Endpoint_T *endpoint = USBD_GetEndpoint(device, epIdx);

	uint32_t len = USBD_HAL_EndpointRxCount(endpoint);
	if (len > endpoint->bufferLen)
	{
		USBD_XFER_LOG("USBD EP%d SETUP done, DATA%d, packet size %d is too large!!!",
			epIdx, !!(MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCDATASEQ), len);
		len = endpoint->bufferLen;
	}
	else
	{
		USBD_XFER_LOG("USBD EP%d SETUP done, DATA%d, packet size %d",
			epIdx, !!(MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCDATASEQ), len);
	}

	for (uint32_t i = 0; i < len; i++)
		endpoint->buffer[i] = MDR_USB->USB_SEP_FIFO[epIdx].RXFD;

	device->endpoint0.Setup();
}

static void USBD_IRQ_HandleEndpointIn(uint8_t epIdx)
{
	USBD_XFER_LOG("USBD EP%d IN done, DATA%d",
		epIdx, !!(MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPDATASEQ));

	USBD_Device_T *device = USBD_GetDevice();
	USBD_Endpoint_T *endpoint = USBD_GetEndpoint(device, epIdx | ENDPOINT_IN);

	if (endpoint->bufferLen > 0) // still have data to send
	{
		if (endpoint->bufferLen < endpoint->desc.maxPacketSize) // data left to send is < MPS, no ZLP needed
		{
			for (uint32_t i = 0; i < endpoint->bufferLen; i++)
				MDR_USB->USB_SEP_FIFO[epIdx].TXFD = endpoint->buffer[i];

			endpoint->buffer = 0; // prevent ZLP
			endpoint->bufferLen = 0; // no data left to send
		}
		else  // data left to send is >= MPS
		{
			for (int i = 0; i < endpoint->desc.maxPacketSize; i++)
				MDR_USB->USB_SEP_FIFO[epIdx].TXFD = endpoint->buffer[i];

			if (endpoint->bufferLen == endpoint->desc.maxPacketSize &&
				endpoint->desc.transferType != EP_TYPE_CTRL) // exactly one packet left to send on non-control endpoint
			{
				endpoint->buffer = 0; // don't send ZLP, just finish transfer
			}
			else // either exactly one packet on control endpoint left to send, or more than one packet left to send
			{
				endpoint->buffer += endpoint->desc.maxPacketSize; // leave buffer pointer non-null to send ZLP packet on control endpoint
			}

			endpoint->bufferLen -= endpoint->desc.maxPacketSize; // decrease data count left to send
		}

		MDR_USB->USB_SEP[epIdx].CTRL ^= USB_SEP_CTRL_EPDATASEQ;
		USBD_SetEndpointReady(epIdx);
	}
	else if (endpoint->buffer && epIdx == 0) // last packet length was MPS, so send ZLP packet
	{
		endpoint->buffer = 0;

		MDR_USB->USB_SEP[epIdx].CTRL ^= USB_SEP_CTRL_EPDATASEQ;
		USBD_SetEndpointReady(epIdx);
	}
	else // transfer complete
	{
		if (epIdx == 0 && device->state != USBD_STATE_CONFIGURED)
			if (device->address && !MDR_USB->SA)
				MDR_USB->SA = device->address;

		MDR_USB->USB_SEP[epIdx].CTRL ^= USB_SEP_CTRL_EPDATASEQ;
		if (endpoint->DataIn)
			endpoint->DataIn();
	}
}

static void USBD_IRQ_HandleEndpointOut(uint8_t epIdx)
{
	USBD_Device_T *device = USBD_GetDevice();
	USBD_Endpoint_T *endpoint = USBD_GetEndpoint(device, epIdx);

	uint32_t len = USBD_HAL_EndpointRxCount(endpoint);

	if (len == 0)
		USBD_XFER_LOG("USBD EP%d OUT done, DATA%d, ZLP",
			epIdx, !!(MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCDATASEQ));
	else
		USBD_XFER_LOG("USBD EP%d OUT done, DATA%d, %d bytes",
			epIdx, !!(MDR_USB->USB_SEP[epIdx].STS & USB_SEP_STS_SCDATASEQ), len);

	if (len > endpoint->bufferLen)
	{
		USBD_XFER_LOG("!!!insufficient buffer size on endpoint %d", epIdx);
	}
	else 
	{
		for (uint32_t i = 0; i < len; i++)
			endpoint->buffer[i] = MDR_USB->USB_SEP_FIFO[epIdx].RXFD;

		endpoint->buffer += len;
		endpoint->bufferLen -= len;

		if (len < endpoint->desc.maxPacketSize) // end of transfer for sure
		{
			if (endpoint->DataOut)
				endpoint->DataOut();
		}
		else if (len <= endpoint->bufferLen) // still have data to receive
		{
			USBD_SetEndpointReady(epIdx);
		}
		else if (len == endpoint->desc.maxPacketSize)
		{
			if (endpoint->desc.transferType == EP_TYPE_CTRL) // second last packet in transfer, ZLP must follow on control data
			{
				USBD_SetEndpointReady(epIdx); // receive ZLP
			}
			else if (endpoint->bufferLen == 0) // last packet in transfer
			{
				if (endpoint->DataOut)
					endpoint->DataOut();
			}
		}
	}
}
