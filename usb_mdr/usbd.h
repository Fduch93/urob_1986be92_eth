#ifndef __USB_DEVICE__H__
#define __USB_DEVICE__H__

typedef enum {
	USBD_MODE_DISABLED,
	USBD_MODE_CDC,
	USBD_MODE_MSC,
	USBD_MODE_COMPOSITE
} USBD_Mode;

USBD_Mode USBD_GetMode(void);
void USBD_SetMode(USBD_Mode mode);

#endif
