#include "usbd.h"
#include "usbd_hal.h"
#include "usbd_msc_dev.h"
#include "usbd_cdc_dev.h"

USBD_Device_T *dev;
USBD_CDC_Device_T cdc;
USBD_MSC_Device_T msc;
USBD_Device_T *cmpDevs[2];

USBD_Device_T *USBD_GetDevice()
{
	return dev;
}

USBD_MSC_Device_T *MSC_GetDevice()
{
	return &msc;
}

USBD_CDC_Device_T *CDC_GetDevice()
{
	return &cdc;
}

static USBD_Mode currentMode = USBD_MODE_DISABLED;

USBD_Mode USBD_GetMode()
{
	return currentMode;
}

void USBD_SetMode(USBD_Mode mode)
{
	if (mode == currentMode)
		return;

	USBD_HAL_DeInit();

	currentMode = mode;

	switch (mode)
	{
		case USBD_MODE_MSC:
			USBD_MSC_Device(MSC_GetDevice());
			dev = &MSC_GetDevice()->device;
			break;
		case USBD_MODE_CDC:
			USBD_CDC_Device(&cdc);
			dev = &cdc.device;
			break;
//		case USBD_MODE_COMPOSITE:
//			USBD_MSC_Device(MSC_GetDevice());
//			USBD_CDC_Device(&cdc);
//			cmpDevs[0] = &MSC_GetDevice()->device;
//			cmpDevs[1] = &cdc.device;
//			dev = USBD_CompositeDevice(cmpDevs, 2);
//			break;
		case USBD_MODE_DISABLED:
		default:
			return;
	}

	dev->state = USBD_STATE_DEFAULT;

	USBD_HAL_Init();
}
