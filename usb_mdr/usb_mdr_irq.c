#include <MDR32Fx.h>
#include <string.h>
#include "TICK.h"
#include "usbd_hal.h"
#include "usbd_endpoint0.h"
#include "usbd_log.h"

static void USBD_IRQ_HandleReset(void);
extern void USBD_IRQ_HandleTransactionDone(void);

void USB_IRQHandler(void)
{
	if (!(MDR_USB->HSCR & USB_HSCR_HOST_MODE))
	{
		uint32_t irq = MDR_USB->SIS & MDR_USB->SIM;
		while (irq)
		{
			if (irq & USB_SIS_SCTDONE)
			{
				MDR_USB->SIS = USB_SIS_SCTDONE;
				USBD_IRQ_HandleTransactionDone();
			}
			if (irq & USB_SIS_SCRESUME)
			{
				MDR_USB->SIS = USB_SIS_SCRESUME;
				USBD_IRQ_LOG("USBD Resume IRQ");
			}
			if (irq & USB_SIS_SCRESETEV)
			{
				MDR_USB->SIS = USB_SIS_SCRESETEV;
				USBD_IRQ_HandleReset();
			}
			if (irq & USB_SIS_SCSOFREC)
			{
				MDR_USB->SIS = USB_SIS_SCSOFREC;
				USBD_IRQ_LOG("USBD SOF received IRQ");	// spawns many log data
			}
			if (irq & USB_SIS_SCNAKSENT)
			{
				MDR_USB->SIS = USB_SIS_SCNAKSENT;
				USBD_IRQ_LOG("USBD NAK sent IRQ");	// spawns many log data
			}
			// fixme i'm undocumented flag (skipped with mask)
			// MDR_USB->SIS = 0x20;
			irq = MDR_USB->SIS & MDR_USB->SIM;
		}
	}
}

static void USBD_IRQ_HandleReset(void)
{
	USBD_IRQ_LOG("USBD Reset IRQ");

	MDR_USB->SA = 0;			// Set default address 0
	MDR_USB->SC &= ~USB_SCGEN;	// General disable
	
	for (int i = 0; i < 4; i++)
	{
		MDR_USB->USB_SEP[i].CTRL = 0;		// reset endpoint control bits
		MDR_USB->USB_SEP_FIFO[i].RXFC = 1;	// force rx fifo clear
		MDR_USB->USB_SEP_FIFO[i].TXFDC = 1;	// force tx fifo clear;
	}

	USBD_Device_T *device = USBD_GetDevice();
	if (device->Reset)
		device->Reset();

	MDR_USB->HSCR	|= USB_HSCR_EN_TX	// Enable Tx
					|  USB_HSCR_EN_RX;	// Enable Rx

	MDR_USB->SC |= USB_SCGEN;		// General enable

	MDR_USB->SIS = MDR_USB->SIS;
}
