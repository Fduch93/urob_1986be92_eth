#include <MDR32Fx.h>
#include <string.h>
#include "TICK.h"
#include "usbd_hal.h"
#include "usbd_endpoint0.h"
#include "usbd_log.h"

static void USBD_HAL_ResetCore(void);
static void USBD_HAL_Connect(void);
static void USBD_HAL_Disconnect(void);

void USBD_HAL_Init(void)
{
	if (!(MDR_RST_CLK->PER_CLOCK & (1 << 2)))
	{
		MDR_RST_CLK->PER_CLOCK |= 1 << 2;
		DELAY(10);
	}

	USBD_HAL_ResetCore();

	MDR_USB->SC	= !USB_SCGEN		// No general enable yet
				| !USB_SCTXLS_Msk	// No diresct control bits
				| !USB_SCDC			// Disable direct pin control
				| USB_SCFSP			// Full speed polarity
				| USB_SCFSR;		// Full speed mode

	MDR_USB->HSCR	= !USB_HSCR_HOST_MODE
					| !USB_HSCR_RESET_CORE
					| !USB_HSCR_EN_TX
					| !USB_HSCR_EN_RX
					| !USB_HSCR_DP_PULLDOWN
					| !USB_HSCR_DM_PULLDOWN
					| !USB_HSCR_DP_PULLUP
					| !USB_HSCR_DM_PULLUP;

	MDR_USB->SIM	= USB_SIM_SCTDONEIE		// Transaction done interrupt enable
//					| USB_SIM_SCRESUMEIE	// Resume interrupt enable
					| USB_SIM_SCRESETEVIE;	// Bus reset interrupt enable
//					| USB_SIM_SCSOFRECIE	// SOF receive interrupt enable
//					| USB_SIM_SCNAKSENTIE;	// NAK sent interrupt enable


	NVIC_EnableIRQ(USB_IRQn);
	NVIC_SetPriority(USB_IRQn,5);

	USBD_HAL_Connect();
	
	USBD_LOG("USBD initializazed");
}

void USBD_HAL_DeInit(void)
{
	NVIC_DisableIRQ(USB_IRQn);
	USBD_HAL_ResetCore();
	
	USBD_LOG("USBD Deinitializazed");
}

static void USBD_HAL_ResetCore(void)
{
	USBD_LOG("USBD Reset core started...");
	MDR_USB->HSCR |= USB_HSCR_RESET_CORE; //Reset USB controller
	DELAY(5);
	MDR_USB->HSCR &= ~USB_HSCR_RESET_CORE; //USB controller in normal mode
	DELAY(5);
	USBD_LOG("USBD Reset core complete");
}

static void USBD_HAL_Connect(void)
{
	MDR_USB->HSCR |= USB_HSCR_DP_PULLUP;
	DELAY(3);
	
	USBD_LOG("USBD ready for connection");
}

static void USBD_HAL_Disconnect(void)
{
	MDR_USB->HSCR &= ~(USB_HSCR_EN_TX
					| USB_HSCR_EN_RX
					| USB_HSCR_DP_PULLDOWN
					| USB_HSCR_DM_PULLDOWN
					| USB_HSCR_DP_PULLUP
					| USB_HSCR_DM_PULLUP);
	DELAY(3);
	
	USBD_LOG("USBD disconnected");
}

static uint8_t epLastCtrl[4];

void USBD_SetEndpointReady(uint8_t epIdx)
{
	MDR_USB->USB_SEP[epIdx].CTRL |= USB_SEP_CTRL_EPRDY;
	epLastCtrl[epIdx] |= USB_SEP_CTRL_EPRDY;
}

uint8_t USBD_HasEndpointReadyChanged(uint8_t epIdx)
{
	return (epLastCtrl[epIdx] & USB_SEP_CTRL_EPRDY) &&
		!(MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPRDY);
}

void USBD_HAL_OpenEndpoint(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP[epIdx].CTRL	= USB_SEP_CTRL_EPEN
									| !USB_SEP_CTRL_EPRDY
									| !USB_SEP_CTRL_EPDATASEQ	// respond with data0
									| !USB_SEP_CTRL_EPSSTALL	// no STALL response
									| !USB_SEP_CTRL_EPISOEN;	// not ISOC
	MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;		// force rx fifo clear
	MDR_USB->USB_SEP_FIFO[epIdx].TXFDC = 1;		// force tx fifo clear

	USBD_LOG("USBD Opened EP%d", epIdx);
}

void USBD_HAL_CloseEndpoint(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP[epIdx].CTRL = 0;		// reset endpoint control bits
	MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;	// force rx fifo clear
	MDR_USB->USB_SEP_FIFO[epIdx].TXFDC = 1;	// force tx fifo clear;

	USBD_LOG("USBD Closed EP%d", epIdx);

}

void USBD_HAL_FlushEndpoint(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;	// force rx fifo clear
	MDR_USB->USB_SEP_FIFO[epIdx].TXFDC = 1;	// force tx fifo clear;

	USBD_LOG("USBD Flushed EP%d buffers", epIdx);
}
	
void USBD_HAL_EndpointReceive(USBD_Endpoint_T *endpoint, uint8_t *buffer, uint32_t len)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP_FIFO[epIdx].RXFC = 1;	// force rx fifo clear

	endpoint->buffer = buffer;
	endpoint->bufferLen = len;
	
	USBD_SetEndpointReady(epIdx);

	USBD_XFER_LOG("USBD EP%d ready to receive %d bytes at 0x%x", epIdx, len, (uint32_t)buffer);
}

void USBD_HAL_EndpointTransmit(USBD_Endpoint_T *endpoint, uint8_t *buffer, uint32_t len)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP_FIFO[epIdx].TXFDC = 1;	// force tx fifo clear;

	if (len < endpoint->desc.maxPacketSize)
	{
		endpoint->buffer = 0;
		endpoint->bufferLen = 0;

		for (uint32_t i = 0; i < len; i++)
			MDR_USB->USB_SEP_FIFO[epIdx].TXFD = buffer[i];
	}
	else if (len == endpoint->desc.maxPacketSize)
	{
		endpoint->buffer = buffer;
		endpoint->bufferLen = 0;

		for (uint32_t i = 0; i < len; i++)
			MDR_USB->USB_SEP_FIFO[epIdx].TXFD = buffer[i];
	}
	else
	{
		endpoint->buffer = buffer + endpoint->desc.maxPacketSize;
		endpoint->bufferLen = len - endpoint->desc.maxPacketSize;

		for (int i = 0; i < endpoint->desc.maxPacketSize; i++)
			MDR_USB->USB_SEP_FIFO[epIdx].TXFD = buffer[i];
	}

	if (endpoint->desc.transferType == EP_TYPE_CTRL)
	{
		MDR_USB->USB_SEP[epIdx].CTRL |= USB_SEP_CTRL_EPDATASEQ;
		if (len == 0)
		{
			USBD_XFER_LOG("USBD EP%d sending ZLP, DATA%d",
				epIdx, !!(MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPDATASEQ));
		}
		else
		{
			USBD_XFER_LOG("USBD EP%d sending %d control bytes, DATA%d",
				epIdx, len, !!(MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPDATASEQ));
		}
	}
	else
	{
		USBD_XFER_LOG("USBD EP%d sending %d bytes, DATA%d",
			epIdx, len, !!(MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPDATASEQ));
	}

	USBD_SetEndpointReady(epIdx);
}

void USBD_HAL_StallEndpoint(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP[epIdx].CTRL |= USB_SEP_CTRL_EPSSTALL;
	USBD_SetEndpointReady(epIdx);

	USBD_LOG("USBD Stalled EP%d", epIdx);
}

void USBD_HAL_UnstallEndpoint(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	MDR_USB->USB_SEP[epIdx].CTRL &= ~USB_SEP_CTRL_EPSSTALL;

	USBD_LOG("USBD Unstalled EP%d", epIdx);
}

char USBD_HAL_IsEndpointStalled(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	return MDR_USB->USB_SEP[epIdx].CTRL & USB_SEP_CTRL_EPSSTALL;
}

uint32_t USBD_HAL_EndpointRxCount(USBD_Endpoint_T *endpoint)
{
	uint8_t epIdx = endpoint->desc.number;

	// dunno if RXFDC_L is ever non-zero as max packet size is limited to 64
	// RXFDC_H seems to be less significant byte
	// separate RXFDC_L and RXFDC_H are not documented as of 2020
	uint32_t dataCount	= (MDR_USB->USB_SEP_FIFO[epIdx].RXFDC_L << 8)
						+  MDR_USB->USB_SEP_FIFO[epIdx].RXFDC_H;
	return dataCount;
}
