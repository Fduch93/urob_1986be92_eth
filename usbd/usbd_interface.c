#include "usbd_interface.h"
#include <string.h>

void USBD_Interface(
	USBD_Interface_T *interface,
	uint8_t altSetting,
	uint8_t interfaceClass,
	uint8_t interfaceSubclass,
	uint8_t interfaceProtocol,
	char *interfaceStrPtr,

	uint8_t classSpecCnt,
	USB_ClassSpecificDesc_T *classSpecs,

	uint8_t endpointCnt,
	USBD_Endpoint_T *endpoints,

	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response))
{
	USB_Interface_Desc(
		&interface->desc,
		0,
		altSetting,
		endpointCnt,
		interfaceClass,
		interfaceSubclass,
		interfaceProtocol,
		0);


	interface->interfaceStrPtr = interfaceStrPtr;

	interface->classSpecCnt = classSpecCnt;
	interface->classSpecs = classSpecs;

	interface->endpoints = endpoints;

	interface->Setup = Setup;
}

void USBD_RebuildInterface(
	USBD_Interface_T *interface,
	uint16_t *size_to_increase,
	uint8_t *interfaceIdx_start,
	uint8_t *in_endpoint_start,
	uint8_t *out_endpoint_start)
{
	interface->desc.interfaceIdx = *interfaceIdx_start;
	(*interfaceIdx_start)++;

	for (int i = 0; i < interface->classSpecCnt; i++)
	{
		*size_to_increase += interface->classSpecs[i].size;
	}

	for (int i = 0; i < interface->desc.endpointCnt; i++)
	{
		USBD_Endpoint_T *endpoint = &interface->endpoints[i];
		*size_to_increase += endpoint->desc.size;

		if (endpoint->desc.isInEndpoint)
		{
			endpoint->desc.number = *in_endpoint_start;
//			(*in_endpoint_start)++;
		}
		else
		{
			endpoint->desc.number = *out_endpoint_start;
//			(*out_endpoint_start)++;
		}
		(*in_endpoint_start)++;
		(*out_endpoint_start)++;
	}

	while (*in_endpoint_start > *out_endpoint_start) (*out_endpoint_start)++;
	while (*in_endpoint_start < *out_endpoint_start) (*in_endpoint_start)++;
}

void USBD_Write_Interface_Children(
	uint8_t **desc_ptr,
	USBD_Interface_T *interface)
{
	for (int i = 0; i < interface->classSpecCnt; i++)
	{
		USB_ClassSpecificDesc_T *class_spec = &interface->classSpecs[i];

		memcpy(*desc_ptr, class_spec, USB_DESC_SIZE_CLASS_SPECIFIC_HEADER);
		*desc_ptr += 3;

		memcpy(*desc_ptr, class_spec->data, class_spec->size - USB_DESC_SIZE_CLASS_SPECIFIC_HEADER);
		*desc_ptr += class_spec->size - USB_DESC_SIZE_CLASS_SPECIFIC_HEADER;
	}

	for (int i = 0; i < interface->desc.endpointCnt; i++)
	{
		USBD_Endpoint_T *endpoint = &interface->endpoints[i];

		memcpy(*desc_ptr, &endpoint->desc, endpoint->desc.size);
		*desc_ptr += endpoint->desc.size;
	}
}

void USBD_ForInterfaceEndpoints(
	USBD_Interface_T *interface,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint))
{
	for (int i = 0; i < interface->desc.endpointCnt; i++)
		endpointConsumer(&interface->endpoints[i]);
}
