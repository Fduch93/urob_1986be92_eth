#ifndef USB_HAL_H
#define USB_HAL_H

#include "usbd_device.h"

void USBD_HAL_Init(void);
void USBD_HAL_DeInit(void);

void USBD_HAL_OpenEndpoint(USBD_Endpoint_T *endpoint);
void USBD_HAL_CloseEndpoint(USBD_Endpoint_T *endpoint);
void USBD_HAL_FlushEndpoint(USBD_Endpoint_T *endpoint);
void USBD_HAL_EndpointReceive(USBD_Endpoint_T *endpoint, uint8_t *buffer, uint32_t len);
void USBD_HAL_EndpointTransmit(USBD_Endpoint_T *endpoint, uint8_t *buffer, uint32_t len);
void USBD_HAL_StallEndpoint(USBD_Endpoint_T *endpoint);
void USBD_HAL_UnstallEndpoint(USBD_Endpoint_T *endpoint);
char USBD_HAL_IsEndpointStalled(USBD_Endpoint_T *endpoint);
uint32_t USBD_HAL_EndpointRxCount(USBD_Endpoint_T *endpoint);

void USBD_HAL_SetDeviceAddress(uint8_t address);

void USBD_SetEndpointReady(uint8_t epIdx);
uint8_t USBD_HasEndpointReadyChanged(uint8_t epIdx);

#endif
