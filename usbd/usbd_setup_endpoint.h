#ifndef USBD_SETUP_ENDPOINT_H
#define USBD_SETUP_ENDPOINT_H

#include "usbd_device.h"

#define USB_FEATURE_EP_HALT			0
#define USB_FEATURE_REMOTE_WAKEUP	1

void USBD_SetupEndpoint(USBD_Device_T *device);

#endif
