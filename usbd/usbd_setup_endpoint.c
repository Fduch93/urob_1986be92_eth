#include "usbd_setup_endpoint.h"
#include "usbd_endpoint0.h"
#include "usbd_hal.h"

#include "USBD_LOG.h"

void USBD_SetupEndpoint(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	uint8_t ep_addr = req->index & 0xFF;

	USBD_Endpoint_T *endpoint = USBD_GetEndpoint(device, ep_addr);
	static uint8_t isEndpointStalledResponse;

	switch (device->state)
	{
		case USBD_STATE_ADDRESSED:
			if (endpoint)
				USBD_HAL_StallEndpoint(endpoint);
			break;

		case USBD_STATE_CONFIGURED:
			switch (req->request)
			{
				case USB_REQ_GET_STATUS:
				{
					isEndpointStalledResponse = USBD_HAL_IsEndpointStalled(endpoint);
					USBD_Endpoint0SendData(device, &isEndpointStalledResponse, 2);
					break;
				}
				case USB_REQ_SET_FEATURE:
					if (req->value == USB_FEATURE_EP_HALT)
					{
						if (endpoint)
							USBD_HAL_StallEndpoint(endpoint);
					}
					USBD_Endpoint0SendStatus(device);
					break;

				case USB_REQ_CLEAR_FEATURE:
					if (req->value == USB_FEATURE_EP_HALT)
					{
						if (endpoint)
							USBD_HAL_UnstallEndpoint(endpoint);
						USBD_Endpoint0SendStatus(device);
					}
				break;

				default:
					break;
			}
			break;

		default:
			USBD_LOG("USBD endp setup?!");
			USBD_Endpoint0Stall(device);
			break;
	}
}
