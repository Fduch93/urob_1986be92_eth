#include "usbd_device.h"
#include "usbd_endpoint0.h"
#include "usbd_hal.h"
#include "usbd_log.h"

#include "usbd_setup_device.h"
#include "usbd_setup_interface.h"
#include "usbd_setup_endpoint.h"
#include "usbd_setup_class.h"

#define ZERO_LENGTH_PACKET 0, 0

static void USBD_Endpoint0Setup(void);
static void USBD_Endpoint0DataIn(void);
static void USBD_Endpoint0DataOut(void);

void USBD_Endpoint0Init(USBD_Device_T *device)
{
	USBD_Endpoint(
		&device->endpoint0,
		0,
		EP_TYPE_CTRL,
		64,
		NO_POLLING_INTERVAL,
		USBD_Endpoint0Setup,
		USBD_Endpoint0DataIn,
		USBD_Endpoint0DataOut);
}

void USBD_Endpoint0SendStatus(USBD_Device_T *device)
{
	device->endpoint0_state = USBD_EP0_STATUS_IN;
	USBD_HAL_EndpointTransmit(&device->endpoint0, ZERO_LENGTH_PACKET);
}

void USBD_Endpoint0ReceiveStatus(USBD_Device_T *device)
{
	device->endpoint0_state = USBD_EP0_STATUS_OUT;
	USBD_HAL_EndpointReceive(&device->endpoint0, ZERO_LENGTH_PACKET);
}

void USBD_Endpoint0SendData(USBD_Device_T *device, uint8_t *pbuf, uint16_t len)
{
	device->endpoint0_state = USBD_EP0_DATA_IN;
	USBD_HAL_EndpointTransmit(&device->endpoint0, pbuf, len);
}

void USBD_Endpoint0ReceiveData(USBD_Device_T *device, uint8_t *pbuf, uint16_t len)
{
	device->endpoint0_state = USBD_EP0_DATA_OUT;
	USBD_HAL_EndpointReceive(&device->endpoint0, pbuf, len);
}

void USBD_Endpoint0ReceiveSetup(USBD_Device_T *device, uint8_t *pbuf, uint16_t len)
{
	device->endpoint0_state = USBD_EP0_IDLE;
	USBD_HAL_EndpointReceive(&device->endpoint0, pbuf, len);
}

void USBD_Endpoint0Stall(USBD_Device_T *device)
{
	USBD_LOG("USBD control error");
	USBD_HAL_StallEndpoint(&device->endpoint0);
}

static void USBD_Endpoint0Setup(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	device->endpoint0_state = USBD_EP0_SETUP;

	switch (device->setupRequest.type)
	{
		case USB_REQ_TYPE_STANDARD:	
			switch (device->setupRequest.recipient)
			{
				case USB_REQ_RECIPIENT_DEVICE:		USBD_SetupDevice(device);		break;
				case USB_REQ_RECIPIENT_INTERFACE:	USBD_SetupInterface(device);	break;
				case USB_REQ_RECIPIENT_ENDPOINT:	USBD_SetupEndpoint(device);		break;
				default:
					USBD_HAL_StallEndpoint(&device->endpoint0);
					break;
			}
			break;
			
		case USB_REQ_TYPE_CLASS:
			USBD_SetupClass(device);
			break;
		
		case USB_REQ_TYPE_VENDOR:
		default:
			break;
	}
}

static void USBD_Endpoint0DataIn(void)
{
	USBD_Device_T *device = USBD_GetDevice();
	
	if (device->endpoint0_state == USBD_EP0_DATA_IN)
	{
		if (device->state == USBD_STATE_CONFIGURED)
			if (device->configuration->EP0_TxSent)
				device->configuration->EP0_TxSent();
		USBD_Endpoint0ReceiveStatus(device);
	}
	else if (device->endpoint0_state == USBD_EP0_STATUS_IN)
	{
		USBD_Endpoint0ReceiveSetup(device, device->setupRequest.u8, sizeof(USB_SetupReq_T));
	}
}

static void USBD_Endpoint0DataOut(void)
{
	USBD_Device_T *device = USBD_GetDevice();
	if (device->endpoint0_state == USBD_EP0_DATA_OUT)
	{
		if (device->state == USBD_STATE_CONFIGURED)
			if (device->configuration->EP0_RxReady)
				device->configuration->EP0_RxReady();
		USBD_Endpoint0SendStatus(device);
	}
	else if (device->endpoint0_state == USBD_EP0_STATUS_OUT)
	{
		USBD_Endpoint0ReceiveSetup(device, device->setupRequest.u8, sizeof(USB_SetupReq_T));
	}
}
