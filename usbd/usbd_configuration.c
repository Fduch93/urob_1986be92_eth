#include "usbd_configuration.h"
#include "usbd_hal.h"
#include <string.h>

void USBD_Configuration(
	USBD_Configuration_T *configuration,
	uint8_t cfgIdx,
	char *cfgStrPtr,
	uint8_t attributes,
	uint8_t maxPower,

	uint8_t interfaceCnt,
	USBD_Interface_T **interfaces,

	uint8_t itfAssociationCnt,
	USBD_InterfaceAssociation_T **itfAssociations,

	void (*Rebuild)(void),
	void (*Init)(void),
	void (*DeInit)(void),
	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response),
	void (*EP0_TxSent)(void),
	void (*EP0_RxReady)(void),
	void (*SOF)(void))
{
	uint8_t inEndpointCnt = 1;
	uint8_t outEndpointCnt = 1;
	uint8_t totalItfCnt = 0;
	uint16_t cfgSize = USB_DESC_SIZE_CONFIGURATION;

	for (int i = 0; i < interfaceCnt; i++)
	{
		USBD_Interface_T *interface = interfaces[i];
		cfgSize += interface->desc.size;

		USBD_RebuildInterface(
			interface,
			&cfgSize,
			&totalItfCnt,
			&inEndpointCnt,
			&outEndpointCnt);
	}

	for (int i = 0; i < itfAssociationCnt; i++)
	{
		USBD_InterfaceAssociation_T *itfAssociation = itfAssociations[i];
		cfgSize += itfAssociation->desc->size;

		itfAssociation->desc->interfaceIdx = totalItfCnt;

		for (int j = 0; j < itfAssociation->desc->interfaceCnt; j++)
		{
			USBD_Interface_T *interface = itfAssociation->interfaces[j];
			cfgSize += interface->desc.size;

			USBD_RebuildInterface(
				interface,
				&cfgSize,
				&totalItfCnt,
				&inEndpointCnt,
				&outEndpointCnt);
		}
	}

	USB_ConfigurationDesc(
		&configuration->desc,
		cfgSize,
		totalItfCnt,
		cfgIdx,
		0,
		attributes,
		maxPower);

	configuration->cfgStrPtr = cfgStrPtr;

	configuration->interfaceCnt = interfaceCnt;
	configuration->interfaces = interfaces;

	configuration->itfAssociationCnt = itfAssociationCnt;
	configuration->itfAssociations = itfAssociations;

	configuration->Rebuild		= Rebuild;
	configuration->Init			= Init;
	configuration->DeInit		= DeInit;
	configuration->Setup		= Setup;
	configuration->EP0_TxSent	= EP0_TxSent;
	configuration->EP0_RxReady	= EP0_RxReady;
	configuration->SOF			= SOF;
}

void USBD_ForConfigurationInterfaces(
	USBD_Configuration_T *cfg,
	void (*interfaceConsumer)(USBD_Interface_T *interface))
{
	for (int i = 0; i < cfg->interfaceCnt; i++)
		interfaceConsumer(cfg->interfaces[i]);

	for (int i = 0; i < cfg->itfAssociationCnt; i++)
		USBD_ForAssociatedInterfaces(cfg->itfAssociations[i],interfaceConsumer);
}

void USBD_ForConfigurationEndpoints(
	USBD_Configuration_T *cfg,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint))
{
	for (int i = 0; i < cfg->interfaceCnt; i++)
		USBD_ForInterfaceEndpoints(cfg->interfaces[i], endpointConsumer);

	for (int i = 0; i < cfg->itfAssociationCnt; i++)
		USBD_ForAssociatedInterfacesEndpoints(cfg->itfAssociations[i], endpointConsumer);
}

#define USB_CONFIG_DESC_MAX_SIZE 512

 uint8_t USBD_Cfg_Desc[USB_CONFIG_DESC_MAX_SIZE] __attribute__((aligned(4))) = {0};

uint8_t *USBD_Cfg_Descriptor(
	USBD_Configuration_T *cfg)
{
	uint8_t *desc_ptr = USBD_Cfg_Desc;

	memcpy(desc_ptr, &cfg->desc, cfg->desc.size);
	desc_ptr += cfg->desc.size;

	for (int i = 0; i < cfg->interfaceCnt; i++)
	{
		USBD_Interface_T *interface = cfg->interfaces[i];

		memcpy(desc_ptr, &interface->desc, interface->desc.size);
		desc_ptr += interface->desc.size;

		USBD_Write_Interface_Children(&desc_ptr, interface);
	}

	for (int i = 0; i < cfg->itfAssociationCnt; i++)
	{
		USBD_InterfaceAssociation_T *itfAssociation = cfg->itfAssociations[i];

		memcpy(desc_ptr, itfAssociation->desc, itfAssociation->desc->size);
		desc_ptr += itfAssociation->desc->size;

		for (int j = 0; j < itfAssociation->desc->interfaceCnt; j++)
		{
			USBD_Interface_T *interface = itfAssociation->interfaces[j];

			memcpy(desc_ptr, &interface->desc, interface->desc.size);
			desc_ptr += interface->desc.size;

			USBD_Write_Interface_Children(&desc_ptr, interface);
		}
	}

	return USBD_Cfg_Desc;
}
