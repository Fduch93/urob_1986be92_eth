#ifndef USBD_DEVICE_H
#define USBD_DEVICE_H

#include "usb_device_desc.h"
#include "usbd_configuration.h"
#include "usb_setup_request.h"

/*  EP0 State */
#define USBD_EP0_IDLE        0
#define USBD_EP0_SETUP       1
#define USBD_EP0_DATA_IN     2
#define USBD_EP0_DATA_OUT    3
#define USBD_EP0_STATUS_IN   4
#define USBD_EP0_STATUS_OUT  5
#define USBD_EP0_STALL       6

/*  Device Status */
#define USBD_STATE_DEFAULT    1
#define USBD_STATE_ADDRESSED  2
#define USBD_STATE_CONFIGURED 3
#define USBD_STATE_SUSPENDED  4

#define USBD_NO_DEVICE_CONNECTED_CALLBACK		0
#define USBD_NO_DEVICE_DISCONNECTED_CALLBACK	0
#define USBD_NO_DEVICE_RESET_CALLBACK			0
#define USBD_NO_DEVICE_SOF_CALLBACK				0
#define USBD_NO_DEVICE_SUSPENDED_CALLBACK		0
#define USBD_NO_DEVICE_RESUMED_CALLBACK			0
#define USBD_NO_DEVICE_CALLBACKS 0,0,0,0,0,0

typedef struct
{
	USB_DeviceDesc_T desc;

	uint8_t address;

	uint32_t state;
	uint32_t prev_state;

	USBD_Endpoint_T endpoint0;
	uint16_t endpoint0_state;

	USB_SetupReq_T setupRequest;
	uint8_t setupData[64-sizeof(USB_SetupReq_T)];

	uint16_t langID;
	char *manufacturerStrPtr;
	char *productStrPtr;
	char *serialStrPtr;

	USBD_Configuration_T *configuration;
	char *strings[16];

	void (*Connected)(void);
	void (*Disconnected)(void);
	void (*Reset)(void);
	void (*SOF)(void);
	void (*Suspended)(void);
	void (*Resumed)(void);
} USBD_Device_T;

void USBD_Device(
	USBD_Device_T *device,
	uint16_t usbVer,
	uint8_t devClass,
	uint8_t devSubclass,
	uint8_t devProtocol,
	uint8_t maxPacketSize,
	uint16_t vid,
	uint16_t pid,
	uint16_t devVer,
	uint16_t langID,
	char *manufacturerStrPtr,
	char *productStrPtr,
	char *serialStrPtr,
	USBD_Configuration_T *configuration,
	void (*Connected)(void),
	void (*Disconnected)(void),
	void (*Reset)(void),
	void (*SOF)(void),
	void (*Suspended)(void),
	void (*Resumed)(void));

USBD_Endpoint_T *USBD_GetEndpoint(USBD_Device_T *device, uint8_t endpoint_address);
USBD_Interface_T *USBD_GetInterface(USBD_Device_T *device, uint8_t interface_index);

extern USBD_Device_T *USBD_GetDevice(void);

#endif
