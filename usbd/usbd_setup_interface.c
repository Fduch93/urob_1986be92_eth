#include "usbd_setup_interface.h"
#include "usbd_endpoint0.h"

#include "USBD_LOG.h"

void USBD_SetupInterface(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	uint8_t itf_idx = req->index & 0xFF;
	switch (device->state)
	{
		case USBD_STATE_CONFIGURED:
			if (itf_idx <= device->configuration->desc.interfaceCnt)
			{
				uint8_t zero_itf_status = 0;
				uint8_t no_alt_itf = 0;
				switch (req->request)
				{
					case USB_REQ_GET_STATUS:
						// reserved by usb spec
						USBD_Endpoint0SendData(device, &zero_itf_status, 2);
						break;

					case USB_REQ_CLEAR_FEATURE:
					case USB_REQ_SET_FEATURE:
						// no interface features in usb spec
						break;

					case USB_REQ_GET_INTERFACE:
						USBD_Endpoint0SendData(device, &no_alt_itf, 1);
						break;

					case USB_REQ_SET_INTERFACE:
						// TODO: implement alternative interface setting
						break;
				}
				break;
			}
			else
			{
				USBD_LOG("USBD wrong itf idx");
				USBD_Endpoint0Stall(device);
			}
			break;

		case USBD_STATE_DEFAULT:
		case USBD_STATE_ADDRESSED:
		case USBD_STATE_SUSPENDED:
		default:
			USBD_LOG("USBD itf setup?!");
			USBD_Endpoint0Stall(device);
			break;
	}
}
