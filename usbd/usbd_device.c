#include "usbd_device.h"
#include "usbd_hal.h"
#include "usbd_endpoint0.h"

static uint16_t USBD_AddString(char **strings, uint16_t *strCnt, char *string);
static void USBD_Rebuild_Strings(
	USBD_Device_T *device,
	char **strings,
	uint16_t strings_max_cnt);
	
static void USBD_Connected(void);
static void USBD_Disconnected(void);
static void USBD_Reset(void);
static void USBD_SOF(void);
static void USBD_Suspended(void);
static void USBD_Resumed(void);

void USBD_Device(
	USBD_Device_T *device,
	uint16_t usbVer,
	uint8_t devClass,
	uint8_t devSubclass,
	uint8_t devProtocol,
	uint8_t maxPacketSize,
	uint16_t vid,
	uint16_t pid,
	uint16_t devVer,
	uint16_t langID,
	char *manufacturerStrPtr,
	char *productStrPtr,
	char *serialStrPtr,
	USBD_Configuration_T *configuration,
	void (*Connected)(void),
	void (*Disconnected)(void),
	void (*Reset)(void),
	void (*SOF)(void),
	void (*Suspended)(void),
	void (*Resumed)(void))
{
	USB_DeviceDesc(
		&device->desc,
		usbVer,
		devClass,
		devSubclass,
		devProtocol,
		maxPacketSize,
		vid,
		pid,
		devVer,
		0, 0, 0,	// string indexes
		1);			// cfg count

	device->address = 0;

	USBD_Endpoint0Init(device);

	device->endpoint0_state = 0;

	device->langID = langID;
	device->manufacturerStrPtr = manufacturerStrPtr;
	device->productStrPtr = productStrPtr;
	device->serialStrPtr = serialStrPtr;

	device->configuration = configuration;
	if (configuration->Rebuild)
		configuration->Rebuild();

	USBD_Rebuild_Strings(device, device->strings, 16);

	device->Connected		= Connected ? Connected : USBD_Connected;
	device->Disconnected	= Disconnected ? Disconnected : USBD_Disconnected;
	device->Reset			= Reset ? Reset : USBD_Reset;
	device->SOF				= SOF ? SOF : USBD_SOF;
	device->Suspended		= Suspended ? Suspended : USBD_Suspended;
	device->Resumed			= Resumed ? Resumed : USBD_Resumed;
}

USBD_Endpoint_T *found_endpoint = 0;
uint8_t search_address = 0;

static void endpoint_match_address(USBD_Endpoint_T *endpoint)
{
	if (endpoint->desc.address == search_address)
		found_endpoint = endpoint;
}

USBD_Endpoint_T *USBD_GetEndpoint(USBD_Device_T *device, uint8_t endpoint_address)
{
	if ((endpoint_address & 0x0F) == 0x00)
		return &device->endpoint0;
//	else if (endpoint_address == 0x80)
//		return &device->in_endpoint0;

	found_endpoint = 0;
	search_address = endpoint_address;

	USBD_ForConfigurationEndpoints(
		device->configuration,
		endpoint_match_address);

	return found_endpoint;
}

USBD_Interface_T *found_interface = 0;
uint8_t search_index = 0;

static void interface_match_index(USBD_Interface_T *interface)
{
	if (interface->desc.interfaceIdx == search_index)
		found_interface = interface;
}

USBD_Interface_T *USBD_GetInterface(USBD_Device_T *device, uint8_t interface_index)
{
	found_interface = 0;
	search_index = interface_index;

	USBD_ForConfigurationInterfaces(
		device->configuration,
		interface_match_index);

	return found_interface;
}

static uint16_t USBD_AddString(char **strings, uint16_t *strCnt, char *string)
{
	if (string != 0)
	{
		strings[*strCnt] = string;
		*strCnt += 1;
	}
	return *strCnt;
}

static void USBD_Rebuild_Strings(
	USBD_Device_T *device,
	char **strings,
	uint16_t strings_max_cnt)
{
	uint16_t strCnt = 0;
	USBD_AddString(strings, &strCnt, (char *)&device->langID);

	device->desc.manufacturerStrIdx = strCnt;
		USBD_AddString(strings, &strCnt, device->manufacturerStrPtr);

	device->desc.productStrIdx = strCnt;
		USBD_AddString(strings, &strCnt, device->productStrPtr);

	device->desc.serialStrIdx = strCnt;
		USBD_AddString(strings, &strCnt, device->serialStrPtr);

	USBD_Configuration_T *configuration = device->configuration;

	configuration->desc.cfgStrIdx = strCnt;
		USBD_AddString(strings, &strCnt, configuration->cfgStrPtr);

	for (int i = 0; i < configuration->interfaceCnt; i++)
	{
		USBD_Interface_T *interface = configuration->interfaces[i];

		interface->desc.interfaceStrIdx = strCnt;
			USBD_AddString(strings, &strCnt, interface->interfaceStrPtr);
	}

	for (int i = 0; i < configuration->itfAssociationCnt; i++)
	{
		USBD_InterfaceAssociation_T *itfAssociation = configuration->itfAssociations[i];

		itfAssociation->desc->functionStrIdx = strCnt;
			USBD_AddString(strings, &strCnt, itfAssociation->functionStrPtr);

		for (int j = 0; j < itfAssociation->desc->interfaceCnt; j++)
		{
			USBD_Interface_T *interface = itfAssociation->interfaces[j];

			interface->desc.interfaceStrIdx = strCnt;
				USBD_AddString(strings, &strCnt, interface->interfaceStrPtr);
		}
	}
}

static void USBD_Connected(void)
{
	USBD_Device_T *device = USBD_GetDevice();
	
	if (device->Connected)
		device->Connected();
}

static void USBD_Disconnected(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	device->state = USBD_STATE_DEFAULT;
	if (device->configuration->DeInit)
		device->configuration->DeInit();
}

static void USBD_Reset(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	device->address = 0;
	device->state = USBD_STATE_DEFAULT;

	if (device->configuration->DeInit)
		device->configuration->DeInit();

	USBD_HAL_OpenEndpoint(&device->endpoint0);
	USBD_Endpoint0ReceiveSetup(device, device->setupRequest.u8, sizeof(USB_SetupReq_T));
}

static void USBD_SOF(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	if (device->state == USBD_STATE_CONFIGURED)
		if (device->configuration->SOF)
			device->configuration->SOF();
}

static void USBD_Suspended(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	device->prev_state = device->state;
	device->state = USBD_STATE_SUSPENDED;
}

static void USBD_Resumed(void)
{
	USBD_Device_T *device = USBD_GetDevice();

	device->state = device->prev_state;
}
