#ifndef USBD_CONFIGURATION_H
#define USBD_CONFIGURATION_H

#include <stdint.h>
#include "usb_configuration_desc.h"
#include "usbd_interface_association.h"
#include "usbd_interface.h"
#include "usb_setup_request.h"

#define USBD_NO_CONFIGURATION_REBUILD_CALLBACK	0
#define USBD_NO_CONFIGURATION_INIT_CALLBACK		0
#define USBD_NO_CONFIGURATION_DEINIT_CALLBACK	0
#define USBD_NO_CONFIGURATION_SETUP_CALLBACK	0
#define USBD_NO_CONFIGURATION_EP0TX_CALLBACK	0
#define USBD_NO_CONFIGURATION_EP0RX_CALLBACK	0
#define USBD_NO_CONFIGURATION_SOF_CALLBACK		0
#define USBD_NO_CONFIGURATION_CALLBACKS 0,0,0,0,0

typedef struct
{
	USB_ConfigurationDesc_T desc;

	char *cfgStrPtr;

	uint8_t interfaceCnt;
	USBD_Interface_T **interfaces;

	uint8_t itfAssociationCnt;
	USBD_InterfaceAssociation_T **itfAssociations;

	void (*Rebuild)(void);
	void (*Init)(void);
	void (*DeInit)(void);
	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response);
	/* Control Endpoints*/
	void (*EP0_TxSent)(void);
	void (*EP0_RxReady)(void);
	/* Class Specific Endpoints*/
	void (*SOF)(void);
} USBD_Configuration_T;

void USBD_Configuration(
	USBD_Configuration_T *congiguration,
	uint8_t cfgIdx,
	char *cfgStrPtr,
	uint8_t attributes,
	uint8_t maxPower,

	uint8_t interfaceCnt,
	USBD_Interface_T **interfaces,

	uint8_t itfAssociationCnt,
	USBD_InterfaceAssociation_T **itfAssociations,

	void (*Rebuild)(void),
	void (*Init)(void),
	void (*DeInit)(void),
	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response),
	void (*EP0_TxSent)(void),
	void (*EP0_RxReady)(void),
	void (*SOF)(void));

uint8_t *USBD_Cfg_Descriptor(
	USBD_Configuration_T *cfg);

void USBD_ForConfigurationInterfaces(
	USBD_Configuration_T *cfg,
	void (*interfaceConsumer)(USBD_Interface_T *interface));

void USBD_ForConfigurationEndpoints(
	USBD_Configuration_T *cfg,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint));

#endif
