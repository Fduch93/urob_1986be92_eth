#ifndef USBD_INTERFACE_H
#define USBD_INTERFACE_H

#include <stdint.h>
#include "usb_interface_desc.h"
#include "usbd_endpoint.h"
#include "usb_class_specific_desc.h"

typedef struct
{
	USB_Interface_Desc_T desc;

	char *interfaceStrPtr;

	USBD_Endpoint_T *endpoints;

	uint8_t classSpecCnt;
	USB_ClassSpecificDesc_T *classSpecs;

	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response);

} USBD_Interface_T;

void USBD_Interface(
	USBD_Interface_T *interface,
	uint8_t altSetting,
	uint8_t interfaceClass,
	uint8_t interfaceSubclass,
	uint8_t interfaceProtocol,
	char *interfaceStrPtr,

	uint8_t classSpecCnt,
	USB_ClassSpecificDesc_T *classSpecs,

	uint8_t endpointCnt,
	USBD_Endpoint_T *endpoints,

	void (*Setup)(USB_SetupReq_T *req, USB_SetupReqResponse_T *response));

void USBD_RebuildInterface(
	USBD_Interface_T *interface,
	uint16_t *size_to_increase,
	uint8_t *interfaceIdx_start,
	uint8_t *in_endpoint_start,
	uint8_t *out_endpoint_start);

void USBD_Write_Interface_Children(
	uint8_t **desc_ptr,
	USBD_Interface_T *interface);

void USBD_ForInterfaceEndpoints(
	USBD_Interface_T *interface,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint));

#endif
