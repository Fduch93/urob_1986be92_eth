#include "usbd_setup_device.h"
#include "usbd_endpoint0.h"
#include "usbd_hal.h"
#include "usb_qualifier_desc.h"

#include "USBD_LOG.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static void USBD_GetDescriptor	(USBD_Device_T *device);
static void USBD_SetAddress		(USBD_Device_T *device);
static void USBD_SetConfig		(USBD_Device_T *device);
static void USBD_GetConfig		(USBD_Device_T *device);
static char USBD_SetClassConfig	(USBD_Device_T *device, uint8_t cfgidx);
static char USBD_ClrClassConfig	(USBD_Device_T *device);
static void USBD_GetStatus		(USBD_Device_T *device);
static void USBD_SetFeature		(USBD_Device_T *device);
static void USBD_ClrFeature		(USBD_Device_T *device);
static void USBD_GetString(char *desc, uint8_t *unicode);

void USBD_SetupDevice(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	switch (req->request)
	{
		case USB_REQ_GET_STATUS:		USBD_GetStatus(device);		break;
		case USB_REQ_CLEAR_FEATURE:		USBD_ClrFeature(device);	break;
		case USB_REQ_SET_FEATURE:		USBD_SetFeature(device);	break;
		case USB_REQ_SET_ADDRESS:		USBD_SetAddress(device);	break;
		case USB_REQ_GET_DESCRIPTOR:	USBD_GetDescriptor(device);	break;
		case USB_REQ_GET_CONFIGURATION:	USBD_GetConfig(device);		break;
		case USB_REQ_SET_CONFIGURATION:	USBD_SetConfig(device);		break;
		default:
			USBD_LOG("USBD wrong req type");
			USBD_Endpoint0Stall(device);
			break;
	}
}

/* Internal string descriptor. */
#define USBD_MAX_STR_DESC_SIZ		512
uint8_t USBD_StrDesc[USBD_MAX_STR_DESC_SIZ] __attribute__((aligned(4)));

static void USBD_GetDescriptor(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	uint8_t descType = req->value >> 8;
	uint8_t desc_index = req->value & 0xFF;

	switch (descType)
	{
		case USB_DESC_TYPE_DEVICE:
			USBD_ENUM_LOG("USBD Sending device descriptor");
			USBD_Endpoint0SendData(
				device,
				(uint8_t *)&device->desc,
				MIN(device->desc.size, req->length));
			return;

		case USB_DESC_TYPE_CONFIGURATION:
			USBD_ENUM_LOG("USBD Sending configuration descriptor");
			USBD_Endpoint0SendData(
				device,
				USBD_Cfg_Descriptor(device->configuration),
				MIN(device->configuration->desc.cfgSize, req->length));
			return;

		case USB_DESC_TYPE_STRING:
			if (desc_index == 0)
			{
				USBD_StrDesc[0] = USB_LEN_LANGID_STR_DESC;
				USBD_StrDesc[1] = USB_DESC_TYPE_STRING;
				USBD_StrDesc[2] = device->langID & 0xFF;
				USBD_StrDesc[3] = device->langID >> 8;

				USBD_ENUM_LOG("USBD Sending LangID descriptor");
				USBD_Endpoint0SendData(
					device,
					USBD_StrDesc,
					MIN(USB_LEN_LANGID_STR_DESC, req->length));
				return;
			}
			else if ((desc_index < 16) && (device->strings[desc_index] != 0))
			{
				USBD_GetString(device->strings[desc_index], USBD_StrDesc);

				USBD_ENUM_LOG("USBD Sending string descriptor %d", desc_index);
				USBD_Endpoint0SendData(
					device,
					USBD_StrDesc,
					MIN(USBD_StrDesc[0], req->length));
				return;
			}
			else
			{
				USBD_LOG("USBD Wrong str idx %d!!!", desc_index);
				USBD_Endpoint0Stall(device);
				return;
			}

		case USB_DESC_TYPE_QUALIFIER: // No qualifier while not High Speed
//			USBD_Endpoint0SendData(
//				device,
//				USBD_QualifierDesc(device),
//				MIN(USB_DESC_SIZE_QUALIFIER, req->length));
//			return;

		default:
			USBD_LOG("USBD Wrong desc type %d!!!", descType);
			USBD_Endpoint0Stall(device);
			return;
	}
}

static void USBD_SetAddress(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	if ((req->index == 0) && (req->length == 0))
	{
		uint8_t dev_addr = (uint8_t)(req->value) & 0x7F;

		if (device->state == USBD_STATE_CONFIGURED)
		{
			USBD_LOG("(setaddr when cfg?!)");
			USBD_Endpoint0Stall(device);
		}
		else
		{
			USBD_ENUM_LOG("USBD Set address %d", dev_addr);
			device->address = dev_addr;
			//USBD_HAL_SetDeviceAddress(dev_addr);
			USBD_Endpoint0SendStatus(device);

			device->state = dev_addr
				? USBD_STATE_ADDRESSED
				: USBD_STATE_DEFAULT;
		}
	}
	else
	{
		USBD_LOG("(wrong setaddr req)");
		USBD_Endpoint0Stall(device);
	}
}

static void USBD_SetConfig(USBD_Device_T *device)
{
	static uint8_t cfgidx;

	USB_SetupReq_T *req = &device->setupRequest;

	cfgidx = (uint8_t)(req->value);

#define USBD_MAX_NUM_CONFIGURATION	1
	if (cfgidx > USBD_MAX_NUM_CONFIGURATION)
	{
		USBD_LOG("USBD wrong cfg idx %d", cfgidx);
		USBD_Endpoint0Stall(device);
	}
	else
	{
		switch (device->state)
		{
			case USBD_STATE_ADDRESSED:
				if (cfgidx)
				{
					device->state = USBD_STATE_CONFIGURED;
					if (USBD_SetClassConfig(device, cfgidx) == 0)
					{
						USBD_LOG("USBD cfg %d init err", cfgidx);
						USBD_Endpoint0Stall(device);
						return;
					}
				}
				USBD_ENUM_LOG("USBD Set configuration %d", cfgidx);
				USBD_Endpoint0SendStatus(device);
				break;

			case USBD_STATE_CONFIGURED:
				if (cfgidx == 0)
				{
					device->state = USBD_STATE_ADDRESSED;
					USBD_ClrClassConfig(device);
					
					USBD_ENUM_LOG("USBD Reset configuration");
					USBD_Endpoint0SendStatus(device);
				}
				else if (cfgidx != 1)
				{
					/* Clear old configuration */
					USBD_ClrClassConfig(device);

					/* set new configuration */
//					pdev->dev_config = cfgidx;
					if (USBD_SetClassConfig(device, cfgidx) == 0)
					{
						USBD_LOG("USBD change to cfg %d err", cfgidx);
						USBD_Endpoint0Stall(device);
						return;
					}
					USBD_ENUM_LOG("USBD Changed configuration to %d", cfgidx);
					USBD_Endpoint0SendStatus(device);
				}
				else
				{
					USBD_Endpoint0SendStatus(device);
				}
				break;

			default:
				USBD_LOG("USBD set cfg?!");
				USBD_Endpoint0Stall(device);
				break;
		}
	}
}

static void USBD_GetConfig(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;

	if (req->length != 1)
	{
		USBD_LOG("USBD getcfg len %d?!", req->length);
		USBD_Endpoint0Stall(device);
	}
	else
	{
		static uint8_t config_dummy;
		switch (device->state)
		{
			case USBD_STATE_ADDRESSED:
//				pdev->dev_default_config = 0;
				config_dummy = 0;
				USBD_Endpoint0SendData(device, &config_dummy, 1);//(uint8_t *)&pdev->dev_default_config, 1);
				break;

			case USBD_STATE_CONFIGURED:
				config_dummy = 1;
				USBD_Endpoint0SendData(device, &config_dummy, 1);//(uint8_t *)&pdev->dev_config, 1);
				break;

			default:
				USBD_LOG("USBD setcfg noaddr?!");
				USBD_Endpoint0Stall(device);
				break;
		}
	}
}

static char USBD_SetClassConfig(USBD_Device_T *device, uint8_t cfgidx)
{
	/* Set configuration  and Start the Class*/
	USBD_ForConfigurationEndpoints(device->configuration, USBD_HAL_OpenEndpoint);
	if (device->configuration->Init)
		device->configuration->Init();
	return 1;
}

static char USBD_ClrClassConfig(USBD_Device_T *device)
{
	/* Clear configuration  and De-initialize the Class process*/
	USBD_ForConfigurationEndpoints(device->configuration, USBD_HAL_CloseEndpoint);
	if (device->configuration->DeInit)
		device->configuration->DeInit();
	return 1;
}

static uint8_t rwu_dummy;

static void USBD_GetStatus(USBD_Device_T *device)
{
	static uint16_t status_dummy;
	switch (device->state)
	{
		case USBD_STATE_ADDRESSED:
		case USBD_STATE_CONFIGURED:

//		#if ( USBD_SELF_POWERED == 1)
//			pdev->dev_config_status = USB_CONFIG_SELF_POWERED;
//		#else
//			pdev->dev_config_status = 0;
//		#endif
//
//			if (pdev->dev_remote_wakeup)
//				 pdev->dev_config_status |= USB_CONFIG_REMOTE_WAKEUP;
			status_dummy = USB_CONFIG_SELF_POWERED |
				rwu_dummy ? USB_CONFIG_REMOTE_WAKEUP : 0;
			USBD_LOG("USBD sent status %x", status_dummy);
			USBD_Endpoint0SendData(device, (uint8_t *)&status_dummy, 2);//(uint8_t *)& pdev->dev_config_status, 2);
			break;

		default:
			USBD_LOG("USBD wrong dev state");
			USBD_Endpoint0Stall(device);
			break;
	}
}

static void USBD_SetFeature(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;

	if (req->value == USB_FEATURE_REMOTE_WAKEUP)
	{
//		pdev->dev_remote_wakeup = 1;
//		rwu_dummy = 1;
//		if (device->configuration->Setup)
//			device->configuration->Setup(device);
		USBD_Endpoint0SendStatus(device);
	}
}

static void USBD_ClrFeature(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;

	switch (device->state)
	{
		case USBD_STATE_ADDRESSED:
		case USBD_STATE_CONFIGURED:
			if (req->value == USB_FEATURE_REMOTE_WAKEUP)
			{
//				pdev->dev_remote_wakeup = 0;
				rwu_dummy = 0;
//				if (device->configuration->Setup)
//					device->configuration->Setup(device);
				USBD_Endpoint0SendStatus(device);
			}
			break;

		default:
			USBD_LOG("USBD clr feature?!");
			USBD_Endpoint0Stall(device);
			break;
	}
}

#include <string.h>
static void USBD_GetString(char *desc, uint8_t *unicode)
{
	uint8_t idx = 0;
	uint16_t len;

	if (desc)
	{
		len = strlen(desc) * 2 + 2;
		unicode[idx++] = len;
		unicode[idx++] = USB_DESC_TYPE_STRING;

		while (*desc)
		{
			unicode[idx++] = *desc++;
			unicode[idx++] =  0x00;
		}
	}
}
