#ifndef USBD_INTERFACE_ASSOCIATION_H
#define USBD_INTERFACE_ASSOCIATION_H

#include <stdint.h>
#include "usb_interface_association_desc.h"
#include "usbd_interface.h"

typedef struct
{
	USB_InterfaceAssociationDesc_T *desc;

	char *functionStrPtr;

	USBD_Interface_T **interfaces;
} USBD_InterfaceAssociation_T;

void USBD_InterfaceAssociation(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	USB_InterfaceAssociationDesc_T *desc,
	uint8_t functionClass,
	uint8_t functionSubclass,
	uint8_t functionProtocol,
	char *functionStrPtr,

	uint8_t interfaceCnt,
	USBD_Interface_T **interfaces);

void USBD_ForAssociatedInterfaces(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	void (*interfaceConsumer)(USBD_Interface_T *interface));

void USBD_ForAssociatedInterfacesEndpoints(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint));

#endif
