#ifndef USBD_SETUP_CLASS_H
#define USBD_SETUP_CLASS_H

#include "usbd_device.h"

void USBD_SetupClass(USBD_Device_T *device);
	
#endif
