#ifndef USBD_SETUP_INTERFACE_H
#define USBD_SETUP_INTERFACE_H

#include "usbd_device.h"

#define USB_FEATURE_REMOTE_WAKEUP	1

void USBD_SetupInterface(USBD_Device_T *device);
	
#endif
