#ifndef USBD_IOREQ_H
#define USBD_IOREQ_H

#include "usbd_device.h"

void USBD_Endpoint0Init(USBD_Device_T *device);

void USBD_Endpoint0SendStatus(USBD_Device_T *device);
void USBD_Endpoint0ReceiveStatus(USBD_Device_T *device);
void USBD_Endpoint0SendData(USBD_Device_T *device, uint8_t *pbuf, uint16_t len);
void USBD_Endpoint0ReceiveData(USBD_Device_T *device, uint8_t *pbuf, uint16_t len);
void USBD_Endpoint0ReceiveSetup(USBD_Device_T *device, uint8_t *pbuf, uint16_t len);
void USBD_Endpoint0Stall(USBD_Device_T *device);

#endif
