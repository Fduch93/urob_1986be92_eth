#include "usbd_device.h"
#include "usbd_endpoint0.h"
#include "usbd_hal.h"
#include "usbd_log.h"

void USBD_SetupClass(USBD_Device_T *device)
{
	USB_SetupReq_T *req = &device->setupRequest;
	
	USB_SetupReqResponse_T resp = {
		.data = 0,
		.len = 0,
		.operation = USBD_REQ_OP_NONE
	};
	
	switch (req->type)
	{
		case USB_REQ_RECIPIENT_DEVICE:
			if (device->configuration->Setup)
				device->configuration->Setup(req, &resp);
			break;

		case USB_REQ_RECIPIENT_INTERFACE:
			{
				uint8_t itf_idx = req->index & 0xFF;
				USBD_Interface_T *interface = USBD_GetInterface(device, itf_idx);
				if (interface->Setup)
				{
					interface->Setup(req, &resp);
					if (req->length == 0) // TODO: without this yet unspecified to my mind operation vcp enumeration hangs a lot
					{
						resp.operation = USBD_REQ_OP_SEND_STATUS;
					}
				}
			}
			break;

		case USB_REQ_RECIPIENT_ENDPOINT:
			{
				uint8_t ep_addr = req->index & 0xFF;
				USBD_Endpoint_T *endpoint = USBD_GetEndpoint(device, ep_addr);
				if (endpoint->Setup)
				{
					endpoint->Setup();
					if (req->length == 0) // TODO: same as interface class-specific setup request
					{
						USBD_Endpoint0SendStatus(device);
					}
				}
			}
			break;

		default:
			USBD_HAL_StallEndpoint(&device->endpoint0);
			break;
	}
	switch (resp.operation)
	{
		case USBD_REQ_OP_ERROR:
			USBD_LOG("USBD reset ctrlErr");
			USBD_Endpoint0Stall(device);
			break;
		case USBD_REQ_OP_SEND_STATUS:
			USBD_Endpoint0SendStatus(device);
			break;
		case USBD_REQ_OP_RECEIVE_STATUS:
			USBD_Endpoint0ReceiveStatus(device);
			break;
		case USBD_REQ_OP_SEND_DATA:
			USBD_Endpoint0SendData(device, resp.data, resp.len);
			break;
		case USBD_REQ_OP_RECEIVE_DATA:
			USBD_Endpoint0ReceiveData(device, resp.data, resp.len);
			break;
		case USBD_REQ_OP_NONE:
		default:
			break;
	}
}
