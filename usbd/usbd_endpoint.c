#include "usbd_endpoint.h"

void USBD_Endpoint(
	USBD_Endpoint_T *endpoint,
	uint8_t isInEndpoint,
	uint8_t transferType,
	uint16_t maxPacketSize,
	uint8_t pollingInterval,

	void (*Setup)(void),
	void (*DataIn)(void),
	void (*DataOut)(void))
{
	USB_EndpointDesc(
		&endpoint->desc,
		isInEndpoint
			? ENDPOINT_IN
			: ENDPOINT_OUT,
		transferType,
		maxPacketSize,
		pollingInterval);

	endpoint->buffer = 0;
	endpoint->bufferLen = 0;

	endpoint->Setup = Setup;
	endpoint->DataIn = DataIn;
	endpoint->DataOut = DataOut;
}
