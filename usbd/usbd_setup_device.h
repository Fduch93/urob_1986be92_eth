#ifndef USBD_SETUP_DEVICE_H
#define USBD_SETUP_DEVICE_H

#include "usbd_device.h"

#define USB_CONFIG_REMOTE_WAKEUP	2
#define USB_CONFIG_SELF_POWERED		1

#define USB_FEATURE_REMOTE_WAKEUP	1

void USBD_SetupDevice(USBD_Device_T *device);

#endif
