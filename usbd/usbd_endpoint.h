#ifndef USBD_ENDPOINT_H
#define USBD_ENDPOINT_H

#include <stdint.h>
#include "usb_setup_request.h"
#include "usb_endpoint_desc.h"

#define USBD_NO_ENDPOINT_SETUP 0
#define USBD_NO_ENDPOINT_DATA_IN 0
#define USBD_NO_ENDPOINT_DATA_OUT 0
#define USBD_NO_ENDPOINT_CALLBACKS 0,0,0

typedef struct
{
	USB_EndpointDesc_T desc;

	uint8_t *buffer;
	uint32_t bufferLen;

	void (*Setup)(void);
	void (*DataIn)(void);
	void (*DataOut)(void);
} USBD_Endpoint_T
__attribute__((aligned(4)));

void USBD_Endpoint(
	USBD_Endpoint_T *endpoint,
	uint8_t isInEndpoint,
	uint8_t transferType,
	uint16_t maxPacketSize,
	uint8_t pollingInterval,

	void (*Setup)(void),
	void (*DataIn)(void),
	void (*DataOut)(void));

#endif
