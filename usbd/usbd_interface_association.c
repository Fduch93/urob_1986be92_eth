#include "usbd_interface_association.h"

void USBD_InterfaceAssociation(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	USB_InterfaceAssociationDesc_T *desc,
	uint8_t functionClass,
	uint8_t functionSubclass,
	uint8_t functionProtocol,
	char *functionStrPtr,

	uint8_t interfaceCnt,
	USBD_Interface_T **interfaces)
{
	desc->size = USB_DESC_SIZE_INTERFACE_ASSOCIATION;
	desc->type = USB_DESC_TYPE_INTERFACE_ASSOCIATION;
	desc->functionClass = functionClass;
	desc->functionSubclass = functionSubclass;
	desc->functionProtocol = functionProtocol;
	desc->functionStrIdx = 0;
	desc->interfaceCnt = interfaceCnt;

	interfaceAssociation->desc = desc;

	interfaceAssociation->functionStrPtr = functionStrPtr;

	interfaceAssociation->interfaces = interfaces;
}

void USBD_ForAssociatedInterfaces(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	void (*interfaceConsumer)(USBD_Interface_T *interface))
{
	for (int i = 0; i < interfaceAssociation->desc->interfaceCnt; i++)
		interfaceConsumer(interfaceAssociation->interfaces[i]);
}

void USBD_ForAssociatedInterfacesEndpoints(
	USBD_InterfaceAssociation_T *interfaceAssociation,
	void (*endpointConsumer)(USBD_Endpoint_T *endpoint))
{
	for (int i = 0; i < interfaceAssociation->desc->interfaceCnt; i++)
		USBD_ForInterfaceEndpoints(interfaceAssociation->interfaces[i], endpointConsumer);
}
