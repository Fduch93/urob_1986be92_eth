#ifndef USBD_CDC_DEV_H
#define USBD_CDC_DEV_H

#include "usbd_device.h"
#include "usbd_cdc_cfg.h"

typedef struct
{
	USBD_Device_T device;
	USBD_CDC_Configuration_T cdcConfiguration;
} USBD_CDC_Device_T;

void USBD_CDC_Device(USBD_CDC_Device_T *cdcDevice);

extern USBD_CDC_Device_T *CDC_GetDevice(void);

#endif
