#ifndef USBD_CDC_H
#define USBD_CDC_H

#include <stdint.h>

#define CDC_DATA_PACKET_SIZE	64

/*---------------------------------------------------------------------*/
/*  CDC definitions                                                    */
/*---------------------------------------------------------------------*/
#define CDC_SEND_ENCAPSULATED_COMMAND	0x00
#define CDC_GET_ENCAPSULATED_RESPONSE	0x01
#define CDC_SET_COMM_FEATURE			0x02
#define CDC_GET_COMM_FEATURE			0x03
#define CDC_CLEAR_COMM_FEATURE			0x04
#define CDC_SET_LINE_CODING				0x20
#define CDC_GET_LINE_CODING				0x21
#define CDC_SET_CONTROL_LINE_STATE		0x22
#define CDC_SEND_BREAK					0x23

/* Define size for the receive and transmit buffer over CDC */
/* It's up to user to redefine and/or remove those define */
#define CDC_SERIAL_BUFFER_SIZE	1024
#define APP_RX_DATA_SIZE		CDC_SERIAL_BUFFER_SIZE
#define APP_TX_DATA_SIZE		CDC_SERIAL_BUFFER_SIZE

#endif
