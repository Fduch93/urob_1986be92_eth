#include "usbd_cdc_dev.h"
#include "usbd_cdc.h"

#define USBD_VID_CDC						0x0483
#define USBD_PID_CDC						0x5740
#define USBD_LANGID_CDC						1033
#define USBD_MANUFACTURER_STRING_CDC		"STMicroelectronics"
#define USBD_PRODUCT_STRING_CDC				"STM32 Virtual ComPort"
#define USBD_SERIALNUMBER_STRING_CDC		"00000000001A"

#define USB_MAX_PACKET_SIZE 64

void USBD_CDC_Device(USBD_CDC_Device_T *cdcDevice)
{
	USBD_CDC_Configuration(&cdcDevice->cdcConfiguration);

	USBD_Device(
		&cdcDevice->device,
		USB_VER_1_1,
		DEV_CLASS_BY_INTERFACE,
		DEV_SUBCLASS_BY_INTERFACE,
		DEV_PROTOCOL_BY_INTERFACE,
		USB_MAX_PACKET_SIZE,
		USBD_VID_CDC,
		USBD_PID_CDC,
		DEV_VER_2_0,
		USB_LANG_EN,
		USBD_MANUFACTURER_STRING_CDC,
		USBD_PRODUCT_STRING_CDC,
		USBD_SERIALNUMBER_STRING_CDC,
		&cdcDevice->cdcConfiguration.configuration,
		USBD_NO_DEVICE_CALLBACKS);

	/* Init Xfer states */
	cdcDevice->cdcConfiguration.cdcDataInterface.TxState = 0;
	cdcDevice->cdcConfiguration.cdcDataInterface.RxState = 0;
}

USBD_CDC_Configuration_T *CDC_GetConfiguration()
{
	USBD_CDC_Device_T *cdcDevice = CDC_GetDevice();
	return &cdcDevice->cdcConfiguration;
}

#include "usbd_hal.h"

void CDC_Transmit(uint8_t *buf, uint32_t len)
{
	USBD_CDC_Device_T *cdcDevice = CDC_GetDevice();
	USBD_Endpoint_T *cdcInEP = &cdcDevice->cdcConfiguration.cdcDataInterface.cdcInEP;
	USBD_HAL_EndpointTransmit(cdcInEP, buf, len);
}

void CDC_Receive(uint8_t *buf, uint32_t len)
{
	USBD_CDC_Device_T *cdcDevice = CDC_GetDevice();
	USBD_Endpoint_T *cdcOutEP = &cdcDevice->cdcConfiguration.cdcDataInterface.cdcOutEP;
	USBD_HAL_EndpointReceive(cdcOutEP, buf, len);
}

uint32_t CDC_Rx_Count()
{
	USBD_CDC_Device_T *cdcDevice = CDC_GetDevice();
	USBD_Endpoint_T *cdcOutEP = &cdcDevice->cdcConfiguration.cdcDataInterface.cdcOutEP;
	return cdcOutEP->bufferLen;
}
