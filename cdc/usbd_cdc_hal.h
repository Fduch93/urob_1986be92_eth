#ifndef USBD_CDC_HAL
#define USBD_CDC_HAL

#include <stdint.h>

void CDC_Open_In_Endpoint(void);
void CDC_Close_In_Endpoint(void);
void CDC_Flush_In_Endpoint(void);
void CDC_Open_Out_Endpoint(void);
void CDC_Close_Out_Endpoint(void);
void CDC_Flush_Out_Endpoint(void);
void CDC_Transmit(uint8_t *buf, uint32_t len);
void CDC_Receive(uint8_t *buf, uint32_t len);
uint32_t CDC_Rx_Count(void);

#endif
