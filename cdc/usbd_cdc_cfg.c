#include "usbd_cdc.h"
#include "usbd_cdc_cfg.h"

#define USBD_CONFIGURATION_STRING_CDC	"CDC Config"

static void USBD_CDC_Cfg_Rebuild(void);
static void USBD_CDC_Init		(void);
static void USBD_CDC_EP0_RxReady(void);

void USBD_CDC_Configuration(USBD_CDC_Configuration_T *cdcConfiguration)
{
	USBD_CDC_CommandInterface(&cdcConfiguration->cdcCmdInterface);
	USBD_CDC_DataInterface(&cdcConfiguration->cdcDataInterface);

	cdcConfiguration->cmdInterface = &cdcConfiguration->cdcCmdInterface.interface;
	cdcConfiguration->dataInterface = &cdcConfiguration->cdcDataInterface.interface;

	USBD_Configuration(
		&cdcConfiguration->configuration,
		1,
		USBD_CONFIGURATION_STRING_CDC,
		0xC0,
		0x32,
		2,
		cdcConfiguration->cdcInterfaces,
		0, 0,

		USBD_CDC_Cfg_Rebuild,
		USBD_CDC_Init,
		USBD_NO_CONFIGURATION_DEINIT_CALLBACK,
		USBD_NO_CONFIGURATION_SETUP_CALLBACK,
		USBD_NO_CONFIGURATION_EP0TX_CALLBACK,
		USBD_CDC_EP0_RxReady,
		USBD_NO_CONFIGURATION_SOF_CALLBACK);
}

static void USBD_CDC_Cfg_Rebuild(void)
{
	USBD_CDC_Configuration_T *cdcConfiguration = CDC_GetConfiguration();

	cdcConfiguration->cdcCmdInterface.cdcCallMgmtData.call_mgmt_capabilities = 0;
	cdcConfiguration->cdcCmdInterface.cdcCallMgmtData.cdcDataItfIdx = cdcConfiguration->dataInterface->desc.interfaceIdx;

	cdcConfiguration->cdcCmdInterface.cdcUnionData.cdcCmdItfIdx = cdcConfiguration->cmdInterface->desc.interfaceIdx;
	cdcConfiguration->cdcCmdInterface.cdcUnionData.cdcDataItfIdx = cdcConfiguration->dataInterface->desc.interfaceIdx;
}

#include "usbd_cdc_hal.h"

static void USBD_CDC_Init(void)
{
	USBD_CDC_DataInterface_T *cdcDataInterface = &CDC_GetConfiguration()->cdcDataInterface;
	
	/* Prepare Out endpoint to receive next packet */
	CDC_Receive(
		cdcDataInterface->RxBuffer,
		CDC_DATA_PACKET_SIZE);
}

static void USBD_CDC_EP0_RxReady()
{
	USBD_CDC_CommandInterface_T *cdcCmdInterface = &CDC_GetConfiguration()->cdcCmdInterface;
	
	if (cdcCmdInterface->CmdOpCode != 0xFF)
	{
		VCP_Control(cdcCmdInterface->CmdOpCode, (uint8_t *)cdcCmdInterface->data, cdcCmdInterface->CmdLength);
		cdcCmdInterface->CmdOpCode = 0xFF;
	}
}
