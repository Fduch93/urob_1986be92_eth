#ifndef USBD_CDC_CFG_H
#define USBD_CDC_CFG_H

#include "usbd_configuration.h"
#include "usbd_cdc_cmd_itf.h"
#include "usbd_cdc_data_itf.h"

typedef struct
{
	USBD_Configuration_T configuration;
	union {
		USBD_Interface_T *cdcInterfaces[2];
		struct {
			USBD_Interface_T *cmdInterface;
			USBD_Interface_T *dataInterface;
		};
	};
	USBD_CDC_CommandInterface_T cdcCmdInterface;
	USBD_CDC_DataInterface_T cdcDataInterface;
} USBD_CDC_Configuration_T;

void USBD_CDC_Configuration(USBD_CDC_Configuration_T *cdcConfiguration);

extern USBD_CDC_Configuration_T *CDC_GetConfiguration(void);

#endif
