#ifndef USBD_CDC_CMD_ITF_H
#define USBD_CDC_CMD_ITF_H

#include "usbd_interface.h"

#define CDC_CMD_PACKET_SIZE				8

typedef struct
{
	USBD_Interface_T interface;

	union {
		USB_ClassSpecificDesc_T cdc_cmd_cs[4];
		struct {
			USB_ClassSpecificDesc_T cdcHeader;
			USB_ClassSpecificDesc_T cdcCallManagement;
			USB_ClassSpecificDesc_T cdcAbstractCtlMgmt;
			USB_ClassSpecificDesc_T cdcUnion;
		};
	};

	uint16_t cdcSpecVer;
	uint8_t cdcAbstractCtlMgmtCapabilities;

	struct {
		uint8_t call_mgmt_capabilities;
		uint8_t cdcDataItfIdx;
	} cdcCallMgmtData;

	struct {
		uint8_t cdcCmdItfIdx;
		uint8_t cdcDataItfIdx;
	} cdcUnionData;

	USBD_Endpoint_T cdcCmdEP;
	
	uint32_t data[CDC_CMD_PACKET_SIZE/4];	/* Force 32bits alignment */
	uint8_t  CmdOpCode;
	uint8_t  CmdLength;
} USBD_CDC_CommandInterface_T;

void USBD_CDC_CommandInterface(USBD_CDC_CommandInterface_T *cdcCmdInterface);

void VCP_Control(uint8_t cmd, uint8_t* pbuf, uint16_t length);

#endif
