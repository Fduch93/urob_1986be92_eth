#include "usbd_cdc_data_itf.h"
#include "usbd_cdc.h"
#include "usbd_cdc_cfg.h"
#include "usbd_cdc_hal.h"

#define USBD_DATA_INTERFACE_STRING_CDC "CDC Data Interface"

static void USBD_CDC_DataIn(void);
static void USBD_CDC_DataOut(void);

void USBD_CDC_DataInterface(USBD_CDC_DataInterface_T *cdcDataInterface)
{
	USBD_Endpoint(
		&cdcDataInterface->cdcInEP,
		ENDPOINT_IN,
		EP_TYPE_BULK,
		CDC_DATA_PACKET_SIZE,
		NO_POLLING_INTERVAL,
		USBD_NO_ENDPOINT_SETUP,
		USBD_CDC_DataIn,
		USBD_NO_ENDPOINT_DATA_OUT);

	USBD_Endpoint(
		&cdcDataInterface->cdcOutEP,
		ENDPOINT_OUT,
		EP_TYPE_BULK,
		CDC_DATA_PACKET_SIZE,
		NO_POLLING_INTERVAL,
		USBD_NO_ENDPOINT_SETUP,
		USBD_NO_ENDPOINT_DATA_IN,
		USBD_CDC_DataOut);

	USBD_Interface(
		&cdcDataInterface->interface,
		USB_INTERFACE_NO_ALTERNATIVE,
		0x0A, 0, 0,
		USBD_DATA_INTERFACE_STRING_CDC,
		0, 0,
		2,
		cdcDataInterface->cdcDataEndpoints,
		0);
}

static void USBD_CDC_DataIn()
{
	USBD_CDC_Configuration_T *cdcConfiguration = CDC_GetConfiguration();
	cdcConfiguration->cdcDataInterface.TxState = 0;
	CDC_DataSentHandler();
}

static void USBD_CDC_DataOut()
{
	USBD_CDC_DataInterface_T *cdcDataInterface = &CDC_GetConfiguration()->cdcDataInterface;
	
	cdcDataInterface->RxLength = CDC_DATA_PACKET_SIZE - CDC_GetConfiguration()->cdcDataInterface.cdcOutEP.bufferLen;// CDC_Rx_Count();

	if (cdcDataInterface->RxLength)
	{
		CDC_DataReceivedHandler(cdcDataInterface->RxBuffer, cdcDataInterface->RxLength);

		CDC_Receive(
			cdcDataInterface->RxBuffer,
			CDC_DATA_PACKET_SIZE);
	}
}

#include "usbd_cdc_cfg.h"
#include <string.h>
char VCP_Transmit(const uint8_t* buf, uint16_t len)
{
	USBD_CDC_DataInterface_T *cdcDataInterface = &CDC_GetConfiguration()->cdcDataInterface;
	
	if (cdcDataInterface->TxState == 0 &&
		len <= sizeof(cdcDataInterface->TxBuffer))
	{
		memcpy(cdcDataInterface->TxBuffer, buf, len);
		cdcDataInterface->TxLength = len;

		/* Tx Transfer in progress */
		cdcDataInterface->TxState = 1;

		/* Transmit next packet */
		CDC_Transmit(
			cdcDataInterface->TxBuffer,
			cdcDataInterface->TxLength);

		return 1;
	}
	else
	{
		return 0;
	}
}
