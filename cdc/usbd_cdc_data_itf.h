#ifndef USBD_CDC_DATA_ITF_H
#define USBD_CDC_DATA_ITF_H

#include "usbd_interface.h"

#define CDC_SERIAL_BUFFER_SIZE	1024
#define APP_RX_DATA_SIZE		CDC_SERIAL_BUFFER_SIZE
#define APP_TX_DATA_SIZE		CDC_SERIAL_BUFFER_SIZE

typedef struct
{
	USBD_Interface_T interface;
	union {
		USBD_Endpoint_T cdcDataEndpoints[2];
		struct {
			USBD_Endpoint_T cdcInEP;
			USBD_Endpoint_T cdcOutEP;
		};
	};
	
	uint8_t  RxBuffer[APP_RX_DATA_SIZE];
	uint8_t  TxBuffer[APP_RX_DATA_SIZE];
	uint32_t RxLength;
	uint32_t TxLength;

	volatile uint32_t TxState;
	volatile uint32_t RxState;
} USBD_CDC_DataInterface_T;

void USBD_CDC_DataInterface(USBD_CDC_DataInterface_T *cdcDataInterface);

char VCP_Transmit(const uint8_t *Buf, uint16_t Len);

extern void CDC_DataReceivedHandler(uint8_t * buf, uint32_t len);
extern void CDC_DataSentHandler(void);

#endif
