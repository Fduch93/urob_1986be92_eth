#include "usbd_cdc.h"
#include "usbd_cdc_cmd_itf.h"
#include "usbd_cdc_hal.h"

#define USBD_CMD_INTERFACE_STRING_CDC	"CDC Command Interface"

#define USB_DESC_SUBTYPE_CDC_HEADER				0
#define USB_DESC_SUBTYPE_CDC_CALL_MANAGEMENT	1
#define USB_DESC_SUBTYPE_CDC_ABSTRACT_CTL_MGMT	2
#define USB_DESC_SUBTYPE_CDC_UNION				6

static void CDC_CmdInterfaceSetup(USB_SetupReq_T *req, USB_SetupReqResponse_T *response);

void USBD_CDC_CommandInterface(USBD_CDC_CommandInterface_T *cdcCmdInterface)
{
	cdcCmdInterface->cdcSpecVer = 0x0110;
	cdcCmdInterface->cdcAbstractCtlMgmtCapabilities = 0x02;
	cdcCmdInterface->cdcCallMgmtData.call_mgmt_capabilities = 0;
	cdcCmdInterface->cdcCallMgmtData.cdcDataItfIdx = 0;
	cdcCmdInterface->cdcUnionData.cdcCmdItfIdx = 0;
	cdcCmdInterface->cdcUnionData.cdcDataItfIdx = 0;

	USB_ClassSpecificDesc(
		&cdcCmdInterface->cdcHeader,
		USB_DESC_SIZE_CLASS_SPECIFIC_HEADER + sizeof(cdcCmdInterface->cdcSpecVer),
		USB_DESC_SUBTYPE_CDC_HEADER,
		sizeof(cdcCmdInterface->cdcSpecVer),
		&cdcCmdInterface->cdcSpecVer);

	USB_ClassSpecificDesc(
		&cdcCmdInterface->cdcCallManagement,
		USB_DESC_SIZE_CLASS_SPECIFIC_HEADER + sizeof(cdcCmdInterface->cdcCallMgmtData),
		USB_DESC_SUBTYPE_CDC_CALL_MANAGEMENT,
		sizeof(cdcCmdInterface->cdcCallMgmtData),
		&cdcCmdInterface->cdcCallMgmtData);

	USB_ClassSpecificDesc(
		&cdcCmdInterface->cdcAbstractCtlMgmt,
		USB_DESC_SIZE_CLASS_SPECIFIC_HEADER + sizeof(cdcCmdInterface->cdcAbstractCtlMgmtCapabilities),
		USB_DESC_SUBTYPE_CDC_ABSTRACT_CTL_MGMT,
		sizeof(cdcCmdInterface->cdcAbstractCtlMgmtCapabilities),
		&cdcCmdInterface->cdcAbstractCtlMgmtCapabilities);

	USB_ClassSpecificDesc(
		&cdcCmdInterface->cdcUnion,
		USB_DESC_SIZE_CLASS_SPECIFIC_HEADER + sizeof(cdcCmdInterface->cdcUnionData),
		USB_DESC_SUBTYPE_CDC_UNION,
		sizeof(cdcCmdInterface->cdcUnionData),
		&cdcCmdInterface->cdcUnionData);

	USBD_Endpoint(
		&cdcCmdInterface->cdcCmdEP,
		ENDPOINT_IN,
		EP_TYPE_INTR,
		CDC_CMD_PACKET_SIZE,
		0x10,
		USBD_NO_ENDPOINT_CALLBACKS);

	USBD_Interface(
		&cdcCmdInterface->interface,
		USB_INTERFACE_NO_ALTERNATIVE,
		2, 2, 1,
		USBD_CMD_INTERFACE_STRING_CDC,
		4,
		cdcCmdInterface->cdc_cmd_cs,
		1,
		&cdcCmdInterface->cdcCmdEP,
		CDC_CmdInterfaceSetup);
}

#include "usbd_cdc_cfg.h"

static void CDC_CmdInterfaceSetup(USB_SetupReq_T *req, USB_SetupReqResponse_T *response)
{
	USBD_CDC_CommandInterface_T *cdcCmdInterface = &CDC_GetConfiguration()->cdcCmdInterface;
	
	if (req->length)
	{
		if (req->direction)
		{
			VCP_Control(req->request, (uint8_t *)cdcCmdInterface->data, req->length);
			response->operation = USBD_REQ_OP_SEND_DATA;
			response->data = (uint8_t *)cdcCmdInterface->data;
			response->len = req->length;
		}
		else
		{
			cdcCmdInterface->CmdOpCode = req->request;
			cdcCmdInterface->CmdLength = req->length;

			response->operation = USBD_REQ_OP_RECEIVE_DATA;
			response->data = (uint8_t *)cdcCmdInterface->data;
			response->len = req->length;
		}
	}
	else
	{
		VCP_Control(req->request, (uint8_t*)req, 0);
	}
}

typedef struct
{
	uint32_t baudRate;
	uint8_t charFormat;
	uint8_t parityType;
	uint8_t dataBits;
} CDC_LineCoding_T;

static CDC_LineCoding_T lineCoding = {115200, 0, 0, 8};

#include "usbd_cdc_cfg.h"
#include <string.h>
#include "UART.h"
void VCP_Control(uint8_t cmd, uint8_t *pbuf, uint16_t length)
{
	static uint8_t dtr_pin = 0; //DTR pin is disabled
	
	USBD_CDC_CommandInterface_T *cdcCmdInterface = &CDC_GetConfiguration()->cdcCmdInterface;
	
	switch (cmd)
	{
		case CDC_SET_LINE_CODING:
			memcpy(&lineCoding, pbuf, sizeof(CDC_LineCoding_T));
			UART_SetBaudRate(lineCoding.baudRate);
			break;

		case CDC_GET_LINE_CODING:
			memcpy(cdcCmdInterface->data, &lineCoding, sizeof(CDC_LineCoding_T));
			break;

		case CDC_SET_CONTROL_LINE_STATE:
			dtr_pin++; //DTR pin is enabled
			//TODO: basically this is not very correct. It needs to analyze data 0-bit to get DTR state
			break;

		case CDC_SEND_ENCAPSULATED_COMMAND:
		case CDC_GET_ENCAPSULATED_RESPONSE:
		case CDC_SET_COMM_FEATURE:
		case CDC_GET_COMM_FEATURE:
		case CDC_CLEAR_COMM_FEATURE:
		case CDC_SEND_BREAK:
		default:
			break;
	}
}
