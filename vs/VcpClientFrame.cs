﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vcp_client
{
    public partial class VcpClientFrame : UserControl
    {

        public VcpClientFrame()
        {
            InitializeComponent();
        }

        private void VcpClientFrame_Load(object sender, EventArgs e)
        {
            combSerial_DropDown(null, null);

            serial = new SerialPort
            {
                NewLine = "\n",
                ReadTimeout = 5000,
                ReadBufferSize = 256 * 1024,
                BaudRate = 115200,
                //DataBits = 8,
                //DiscardNull = false,
                //Handshake = Handshake.None,
                //Parity = Parity.None,
                //StopBits = StopBits.One
            };
            serial.DataReceived += serialDataReceived;
            serial.ErrorReceived += serialError;
        }

        private void combSerial_DropDown(object sender, EventArgs e)
        {
            List<COMPortInfo> serials = COMPortInfo.GetCOMPortsInfo();

            combSerial.Items.Clear();
            combSerial.Items.AddRange(serials
                .ConvertAll(ser => string.Format("{0} – {1}", ser.Name, ser.Description))
                .ToArray());
        }

        public void SetBaud(int baud)
        {
            TryClosePort();

            try
            {
                serial.BaudRate = baud;
                serial.Open();
                logColoredAligned($"{serial.PortName} connected", richRxTx, Color.LimeGreen, HorizontalAlignment.Center);
            }
            catch (Exception ex)
            {
                logColoredAligned($"{serial.PortName}: {ex.Message}", richRxTx, Color.DarkMagenta, HorizontalAlignment.Center);
            }
            
        }

        private void combSerial_SelectedIndexChanged(object sender, EventArgs e)
        {
            TryClosePort();

            try
            {
                serial.PortName = combSerial.Text.Split(' ')[0];
                serial.Open();
                logColoredAligned($"{serial.PortName} connected", richRxTx, Color.LimeGreen, HorizontalAlignment.Center);
            }
            catch (Exception ex)
            {
                logColoredAligned($"{serial.PortName}: {ex.Message}", richRxTx, Color.DarkMagenta, HorizontalAlignment.Center);
            }
        }

        private void TryClosePort()
        {
            try
            {
                if (serial.IsOpen)
                {
                    serial.Close();
                    logColoredAligned($"{serial.PortName} disconnected", richRxTx, Color.DarkGreen, HorizontalAlignment.Center);
                }
            }
            catch (Exception ex)
            {
                logColoredAligned($"{serial.PortName}: {ex.Message}", richRxTx, Color.DarkMagenta, HorizontalAlignment.Center);
            }
        }

        private void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] buffer = new byte[serial.BytesToRead];
            serial.Read(buffer, 0, buffer.Length);

            //Encoding.ASCII.GetString(buffer)
            //if (buffer.All(b => 0x20 >= b && b <= 0x7E))
            richRxTx.BeginInvoke((Action)(() =>
                logColoredAligned(
                    $">> {Encoding.ASCII.GetString(buffer)}",
                    richRxTx, Color.Green, HorizontalAlignment.Left)));
        }

        private void serialError(object sender, SerialErrorReceivedEventArgs e)
        {
            logColoredAligned("!!!ERROR!!!", richRxTx, Color.Red, HorizontalAlignment.Center);
        }

        private void logColoredAligned(string output,
                                       RichTextBox richRxTx,
                                       Color color,
                                       HorizontalAlignment alignment)
        {
            int selStart = richRxTx.Text.Length;
            richRxTx.AppendText($"{output.Trim()}{Environment.NewLine}");
            richRxTx.SelectionStart = selStart;
            richRxTx.SelectionLength = richRxTx.Text.Length - selStart - 1;
            richRxTx.SelectionColor = color;
            richRxTx.SelectionAlignment = alignment;
            richRxTx.SelectionStart = richRxTx.Text.Length;
            richRxTx.ScrollToCaret();
        }

        private void txtTx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && serial.IsOpen)
            {
                serial.WriteLine(txtTx.Text);
                
                logColoredAligned($"{txtTx.Text} <<", richRxTx, Color.Blue, HorizontalAlignment.Right);

                //txtTx.Text = string.Empty;
            }
        }
    }
}
