﻿
namespace vcp_client
{
    partial class VcpClientFrame
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richRxTx = new System.Windows.Forms.RichTextBox();
            this.combSerial = new System.Windows.Forms.ComboBox();
            this.serial = new System.IO.Ports.SerialPort(this.components);
            this.txtTx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // richRxTx
            // 
            this.richRxTx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richRxTx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richRxTx.Location = new System.Drawing.Point(0, 21);
            this.richRxTx.Name = "richRxTx";
            this.richRxTx.ReadOnly = true;
            this.richRxTx.Size = new System.Drawing.Size(337, 459);
            this.richRxTx.TabIndex = 3;
            this.richRxTx.Text = "";
            // 
            // combSerial
            // 
            this.combSerial.Dock = System.Windows.Forms.DockStyle.Top;
            this.combSerial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combSerial.FormattingEnabled = true;
            this.combSerial.Location = new System.Drawing.Point(0, 0);
            this.combSerial.Name = "combSerial";
            this.combSerial.Size = new System.Drawing.Size(337, 21);
            this.combSerial.TabIndex = 4;
            this.combSerial.DropDown += new System.EventHandler(this.combSerial_DropDown);
            this.combSerial.SelectedIndexChanged += new System.EventHandler(this.combSerial_SelectedIndexChanged);
            // 
            // txtTx
            // 
            this.txtTx.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtTx.Location = new System.Drawing.Point(0, 480);
            this.txtTx.Name = "txtTx";
            this.txtTx.Size = new System.Drawing.Size(337, 20);
            this.txtTx.TabIndex = 7;
            this.txtTx.Text = "*idn?";
            this.txtTx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTx_KeyDown);
            // 
            // VcpClientFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richRxTx);
            this.Controls.Add(this.combSerial);
            this.Controls.Add(this.txtTx);
            this.Name = "VcpClientFrame";
            this.Size = new System.Drawing.Size(337, 500);
            this.Load += new System.EventHandler(this.VcpClientFrame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox richRxTx;
        private System.Windows.Forms.ComboBox combSerial;
        private System.IO.Ports.SerialPort serial;
        private System.Windows.Forms.TextBox txtTx;
    }
}
