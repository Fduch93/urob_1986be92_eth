﻿
namespace vcp_client
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStrip strip;
            System.Windows.Forms.ToolStripLabel lblBaud;
            this.txtBaud = new System.Windows.Forms.ToolStripTextBox();
            this.split = new System.Windows.Forms.SplitContainer();
            this.vcpClientFrame1 = new vcp_client.VcpClientFrame();
            this.vcpClientFrame2 = new vcp_client.VcpClientFrame();
            strip = new System.Windows.Forms.ToolStrip();
            lblBaud = new System.Windows.Forms.ToolStripLabel();
            strip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.split)).BeginInit();
            this.split.Panel1.SuspendLayout();
            this.split.Panel2.SuspendLayout();
            this.split.SuspendLayout();
            this.SuspendLayout();
            // 
            // strip
            // 
            strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            lblBaud,
            this.txtBaud});
            strip.Location = new System.Drawing.Point(0, 0);
            strip.Name = "strip";
            strip.Size = new System.Drawing.Size(845, 25);
            strip.TabIndex = 1;
            strip.Text = "toolStrip1";
            // 
            // lblBaud
            // 
            lblBaud.Name = "lblBaud";
            lblBaud.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            lblBaud.Size = new System.Drawing.Size(37, 22);
            lblBaud.Text = "Baud:";
            // 
            // txtBaud
            // 
            this.txtBaud.Name = "txtBaud";
            this.txtBaud.Size = new System.Drawing.Size(50, 25);
            this.txtBaud.Text = "115200";
            this.txtBaud.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBaud.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBaud_KeyDown);
            // 
            // split
            // 
            this.split.Dock = System.Windows.Forms.DockStyle.Fill;
            this.split.IsSplitterFixed = true;
            this.split.Location = new System.Drawing.Point(0, 25);
            this.split.Name = "split";
            // 
            // split.Panel1
            // 
            this.split.Panel1.Controls.Add(this.vcpClientFrame1);
            // 
            // split.Panel2
            // 
            this.split.Panel2.Controls.Add(this.vcpClientFrame2);
            this.split.Size = new System.Drawing.Size(845, 577);
            this.split.SplitterDistance = 420;
            this.split.TabIndex = 1;
            // 
            // vcpClientFrame1
            // 
            this.vcpClientFrame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vcpClientFrame1.Location = new System.Drawing.Point(0, 0);
            this.vcpClientFrame1.Name = "vcpClientFrame1";
            this.vcpClientFrame1.Size = new System.Drawing.Size(420, 577);
            this.vcpClientFrame1.TabIndex = 0;
            // 
            // vcpClientFrame2
            // 
            this.vcpClientFrame2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vcpClientFrame2.Location = new System.Drawing.Point(0, 0);
            this.vcpClientFrame2.Name = "vcpClientFrame2";
            this.vcpClientFrame2.Size = new System.Drawing.Size(421, 577);
            this.vcpClientFrame2.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 602);
            this.Controls.Add(this.split);
            this.Controls.Add(strip);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            strip.ResumeLayout(false);
            strip.PerformLayout();
            this.split.Panel1.ResumeLayout(false);
            this.split.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.split)).EndInit();
            this.split.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VcpClientFrame vcpClientFrame1;
        private System.Windows.Forms.SplitContainer split;
        private VcpClientFrame vcpClientFrame2;
        private System.Windows.Forms.ToolStripTextBox txtBaud;
    }
}

