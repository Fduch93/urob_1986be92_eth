﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vcp_client
{
    class Utils
    {
        public static void AdjustFont(TextBox txtBox)
        {
            txtBox.SuspendLayout();

            txtBox.Font = new Font(txtBox.Font.FontFamily, 40f);

            int width = TextRenderer.MeasureText(txtBox.Text, txtBox.Font).Width;

            while (width > txtBox.Width - 8)
            {
                txtBox.Font = new Font(
                     txtBox.Font.FontFamily,
                     txtBox.Font.Size * 0.9f);

                width = TextRenderer.MeasureText(txtBox.Text, txtBox.Font).Width;
            }

            txtBox.ResumeLayout();
        }
    }
}
