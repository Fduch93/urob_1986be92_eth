﻿using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace vcp
{
    class Vcp
    {
        public SerialPort serialPort;
        private Timer tCheck;

        private Action<byte[]> dataHandler;

        private Action<string> statusHandler;
        private Action<string> inputLog;
        private Action<string> outputLog;

        private string idn;

        public Vcp(string idn,
                   Action<byte[]> dataHandler = null,
                   Action<string> statusHandler = null,
                   Action<string> inputLog = null,
                   Action<string> outputLog = null,
                   bool enable = false)
        {
            serialPort = new SerialPort
            {
                NewLine = "\n",
                ReadTimeout = 10000,
                ReadBufferSize = 256 * 1024
            };
            serialPort.DataReceived += vcpDataReceived;
            serialPort.ErrorReceived += vcpError;

            this.dataHandler = dataHandler;
            this.statusHandler = statusHandler;
            this.inputLog = inputLog;
            this.outputLog = outputLog;
            this.idn = idn;

            tCheck = new Timer();
            tCheck.Tick += tCheck_Tick;
            tCheck.Interval = 100;
            tCheck.Enabled = enable;
        }

        public void Enable()
        {
            tCheck.Enabled = true;
        }

        public void Disable()
        {
            vcpClose();
            tCheck.Enabled = false;
        }

        public void command(string request)
        {
            if (serialPort.IsOpen)
            {
                try
                {
                    serialPort.DataReceived -= vcpDataReceived;
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                    serialPort.WriteLine(request);
                    outputLog.Invoke(request);
                    serialPort.DataReceived += vcpDataReceived;
                }
                catch { }
            }
        }

        public string request(string request)
        {
            while (!serialPort.IsOpen)
            {
                try
                {
                    System.Threading.Thread.Sleep(1000);
                    serialPort.Open();
                }
                catch (Exception e)
                {
                    if (MessageBox.Show(e.Message, "FUCK!", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                        Application.Exit();
                }
            }   
            if (serialPort.IsOpen)
            {
                try
                {
                    serialPort.DataReceived -= vcpDataReceived;
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                    serialPort.WriteLine(request);
                    outputLog?.Invoke(request);

                    string response = serialPort.ReadLine() ?? "[NO RESPONSE]";

                    inputLog?.Invoke(
                        response.Length < 256
                            ? response
                            : $"Received {response.Length} bytes");

                    serialPort.DataReceived += vcpDataReceived;
                    return response;
                }
                catch (TimeoutException)
                {
                    statusHandler?.Invoke("[NO RESPONSE]");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            return null;
        }

        private void tCheck_Tick(object sender, EventArgs e)
        {
            if (!serialPort.IsOpen)
            {
                foreach (string portName in SerialPort.GetPortNames())
                {
                    serialPort.PortName = portName;
                    try
                    {
                        serialPort.Open();
                        if (idn != null && idn.Equals(request("*IDN?")))
                        {
                            statusHandler?.Invoke($"{portName}:OK");
                            break;
                        }
                        vcpClose();
                    }
                    catch
                    {
                        vcpClose(); 
                    }
                }
            }
            if (!Array.Exists(SerialPort.GetPortNames(), portName => portName.Equals(serialPort.PortName)))
            {
                statusHandler?.Invoke($"{serialPort.PortName}: Not connected");
                try
                {
                    serialPort.Close();
                }
                catch
                {
                    statusHandler?.Invoke($"{serialPort.PortName}: Close error");
                }
            }
        }

        private void vcpDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] buffer = new byte[serialPort.BytesToRead];
            serialPort.Read(buffer, 0, buffer.Length);

            dataHandler?.Invoke(buffer);
        }

        private void vcpError(object sender, SerialErrorReceivedEventArgs e)
        {
            statusHandler?.Invoke($"{serialPort.PortName}:Error!");
            vcpClose();
        }

        private void vcpClose()
        {
            try
            {
                serialPort.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
