﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vcp;

namespace vcp_client
{
    class VcpWrapper
    {
        private Vcp vcp;

        public VcpWrapper(RichTextBox richRxTx, ToolStripStatusLabel statusLabel)
        {
            vcp = new Vcp(
                "STM32F4D CSRA64xxx flashfucker",
                null,
                status => statusLabel.Text = status,
                input => logInput(input, richRxTx),
                output => logOutput(output, richRxTx),
                true);
        }

        private void logInput(string input, RichTextBox richRxTx)
        {
            int selStart = richRxTx.Text.Length;
            richRxTx.AppendText($">> {input}{Environment.NewLine}");
            richRxTx.SelectionStart = selStart;
            richRxTx.SelectionLength = richRxTx.Text.Length - selStart - 1;
            richRxTx.SelectionColor = Color.Green;
            richRxTx.SelectionAlignment = HorizontalAlignment.Left;
            richRxTx.SelectionStart = richRxTx.Text.Length;
            richRxTx.ScrollToCaret();
        }

        private void logOutput(string output, RichTextBox richRxTx)
        {
            int selStart = richRxTx.Text.Length;
            richRxTx.AppendText($"{output} <<{Environment.NewLine}");
            richRxTx.SelectionStart = selStart;
            richRxTx.SelectionLength = richRxTx.Text.Length - selStart - 1;
            richRxTx.SelectionColor = Color.Blue;
            richRxTx.SelectionAlignment = HorizontalAlignment.Right;
            richRxTx.SelectionStart = richRxTx.Text.Length;
            richRxTx.ScrollToCaret();
        }

        public void SetFL() => vcp.request("*FL");
        public void SetBT() => vcp.request("*BT");
        public void Stop() => vcp.request("*STOP");
        public void TestWrite() => vcp.request("*TESTWRITE");
        public string ReadPage(int page) => vcp.request($"*READPAGE:{page}");
        public void Write(int addr, int write) => vcp.request($"*WRITE:{addr}:{write}");
        public void SetI2S() => vcp.request("*I2SOUT");
        public void SetDAC() => vcp.request("*DACOUT");
        public string CfgOut(int outCfg) => vcp.request($"*OutCfg:{outCfg}");
    }
}
