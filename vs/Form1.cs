﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace vcp_client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                GetChildAtPoint(Point.Empty).SendToBack();
            }
        }

        private void txtBaud_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (int.TryParse(txtBaud.Text, out int baud))
                    if (9600 <= baud && baud <= 921600)
                    {
                        vcpClientFrame1.SetBaud(baud);
                        vcpClientFrame2.SetBaud(baud);
                    }
            }
        }
    }
}
