#include "FAT_hal.h"

#include "FAT_volume.h"
#include "FAT_entry.h"
#include "FAT_fat.h"

#include <string.h>

void FAT_ReadFirstVolume(FAT_Volume_T *volume)
{
	FAT_ReadSector(0);

	FAT_Drive_T drive = {0};

	FAT_BuildDrive(&drive, (FAT_MasterBootRecord_T *)FAT_GetSectorBuffer());

	for (int i = 0; i < 4; i++)
	{
		if (drive.partitions[i].partitionType == 0x0C)
		{
			FAT_ReadSector(drive.partitions[i].firstSector);
			FAT_BuildVolume(volume, (FAT_BootSector_T *)FAT_GetSectorBuffer(), &drive.partitions[i]);
			return;
		}
	}
}

uint32_t FAT_GetMaxClusterFromChain(uint32_t firstCluster, FAT_Volume_T *volume)
{
	uint16_t clustersPerFatSector = volume->bytesPerSector / 4;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	uint32_t fatSectorRelative = 0xFFFFFFFF;
	uint32_t maxCluster = firstCluster;
	uint32_t nextCluster = firstCluster;

	while (1)
	{
		if (nextCluster / clustersPerFatSector != fatSectorRelative)
		{
			fatSectorRelative = nextCluster / clustersPerFatSector;
			FAT_ReadSector(volume->fat1FirstSector + fatSectorRelative);
		}

		uint32_t currentCluster = nextCluster;
		nextCluster = fatSectorBuffer[nextCluster % clustersPerFatSector];

		maxCluster = currentCluster > maxCluster ? currentCluster : maxCluster;

		if (nextCluster <= 2 || nextCluster >= clustersPerFatSector * volume->sectorsPerFat)
			return maxCluster;
	}
}

uint32_t FAT_GetMaxClusterFromDir(FAT_Directory_T *dir, FAT_Volume_T *volume)
{
	uint32_t maxCluster = FAT_GetMaxClusterFromChain(dir->firstCluster, volume);

	for (uint16_t i = 0; i < dir->fileCount; i++)
	{
		uint32_t maxFileCluster = FAT_GetMaxClusterFromChain(dir->files[i].firstCluster, volume);
		maxCluster = maxFileCluster > maxCluster ? maxFileCluster : maxCluster;
	}
	for (uint16_t i = 0; i < dir->subdirCount; i++)
	{
		FAT_Directory_T *subdir = &dir->subdirs[i];

		FAT_ReadDirectory(subdir, volume,0,0,0,0);

		FAT_Directory_T dirs[subdir->subdirCount];
		FAT_File_T files[subdir->fileCount];

		FAT_ReadDirectory(
			subdir, volume,
			dirs, subdir->subdirCount,
			files,subdir->fileCount);

		uint32_t maxDirCluster = FAT_GetMaxClusterFromDir(subdir, volume);
		maxCluster = maxDirCluster > maxCluster ? maxDirCluster : maxCluster;
	}

	return maxCluster;
}

uint32_t FAT_GetFreeClustersStart(FAT_Volume_T *volume)
{
	FAT_Directory_T root = {0};

	FAT_ReadRootDirectory(&root, volume, 0,0,0,0);

	FAT_Directory_T rootDirs[root.subdirCount];
	FAT_File_T rootFiles[root.fileCount];

	FAT_ReadRootDirectory(
		&root, volume,
		rootDirs, root.subdirCount,
		rootFiles, root.fileCount);

	uint32_t maxCluster = FAT_GetMaxClusterFromChain(volume->rootCluster, volume);

	for (uint16_t i = 0; i < root.fileCount; i++)
	{
		uint32_t maxFileCluster = FAT_GetMaxClusterFromChain(rootFiles[i].firstCluster, volume);
		maxCluster = maxFileCluster > maxCluster ? maxFileCluster : maxCluster;
	}
	for (uint16_t i = 0; i < root.subdirCount; i++)
	{
		FAT_Directory_T *subdir = &root.subdirs[i];

		FAT_ReadDirectory(subdir, volume,0,0,0,0);

		FAT_Directory_T dirs[subdir->subdirCount];
		FAT_File_T files[subdir->fileCount];

		FAT_ReadDirectory(
			subdir, volume,
			dirs, subdir->subdirCount,
			files,subdir->fileCount);

		uint32_t maxDirCluster = FAT_GetMaxClusterFromDir(subdir, volume);
		maxCluster = maxDirCluster > maxCluster ? maxDirCluster : maxCluster;
	}

	return maxCluster + 1;
}

static char FAT_IsFreeEntry(FAT_Entry_T *entry)
{
	uint8_t firstChar = entry->shortName[0];
	if (firstChar == 0x00 ||
		firstChar == 0x05 ||
		firstChar == 0xE5 ||
		!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&'()-@^_`{}~_.", firstChar))
		return 1;
	else
		return 0;
}

void FAT_CreateDirectory(FAT_Directory_T *dir, char *dirName, FAT_Directory_T *parentDir, FAT_Volume_T *volume)
{
	uint32_t dirCluster = FAT_AllocateSingleCluster(volume);

	if (dirCluster == 0)
		while (1);

	char dirShortName[12] = "        .   ";
	uint32_t dirNameLen = strlen(dirName);
	dirNameLen = dirNameLen > 8 ? 8 : dirNameLen;
	memcpy(dirShortName, dirName, dirNameLen);

	uint32_t parentDirSector = FAT_ClusterToSector(parentDir->firstCluster, volume);

	FAT_ReadSector(parentDirSector);

	FAT_Entry_T *parentDirEntries = (FAT_Entry_T *)FAT_GetSectorBuffer();
	uint16_t entriesPerSector = volume->bytesPerSector / sizeof(FAT_Entry_T);

	FAT_Entry_T *dirEntry = 0;

	for (uint16_t i = 0; i < entriesPerSector; i++)
	{
		if (FAT_IsFreeEntry(&parentDirEntries[i]))
		{
			if (dirEntry == 0)
				dirEntry = &parentDirEntries[i];
		}
		else
		{
			if (strncmp(dirShortName, (char *)parentDirEntries[i].shortName, 12) == 0)
			{
				dirEntry = 0;
				break;
			}
		}
	}

	if (dirEntry == 0)
		while (1);

	dir->dirSize = 0;
	dir->firstCluster = dirCluster;
	memcpy(dir->dirName, dirShortName, 12);
	dir->files = 0;
	dir->fileCount = 0;
	dir->subdirs = 0;
	dir->subdirCount = 0;

	FAT_CreateDirectoryEntry(dir, dirEntry);

	FAT_WriteSector(parentDirSector);

	/////////

	memset(FAT_GetSectorBuffer(), 0, volume->bytesPerSector);

	FAT_Entry_T *dirEntries = (FAT_Entry_T *)FAT_GetSectorBuffer();

	FAT_Directory_T dotDir = {0};

	dotDir.dirSize = 0;
	dotDir.firstCluster = dirCluster;
	memcpy(dotDir.dirName, ".       .   ", 12);

	FAT_CreateDirectoryEntry(&dotDir, &dirEntries[0]);

	dotDir.dirSize = 0;
	dotDir.firstCluster = parentDir->firstCluster;
	memcpy(dotDir.dirName, "..      .   ", 12);

	FAT_CreateDirectoryEntry(&dotDir, &dirEntries[1]);

	uint32_t dirSector = FAT_ClusterToSector(dirCluster, volume);
	FAT_WriteSector(dirSector);

	memset(FAT_GetSectorBuffer(), 0, volume->bytesPerSector);
	for (uint32_t rest = 1; rest < volume->sectorsPerCluster; rest++)
		FAT_WriteSector(dirSector+rest);
}


void FAT_CreateFile(FAT_File_T *file, char *fileName, uint32_t firstCluster, FAT_Directory_T *parentDir, FAT_Volume_T *volume)
{
	char fileShortName[12] = "        .   ";
	const char *dot = strchr(fileName, '.');
	if (dot)
	{
		if (dot > fileName)
		{
			uint32_t fileNameLen = dot - fileName;
			fileNameLen = fileNameLen > 8 ? 8 : fileNameLen;
			memcpy(fileShortName, fileName, fileNameLen);

			uint32_t fileExtLen = strlen(dot + 1);
			fileExtLen = fileExtLen > 3 ? 3 : fileExtLen;
			memcpy(&fileShortName[9], dot + 1, fileExtLen);
		}
		else
		{
			while (1);
		}
	}
	else
	{
		uint32_t fileNameLen = strlen(fileName);
		fileNameLen = fileNameLen > 8 ? 8 : fileNameLen;
		memcpy(fileShortName, fileName, fileNameLen);
	}

	uint32_t parentDirSector = FAT_ClusterToSector(parentDir->firstCluster, volume);

	FAT_ReadSector(parentDirSector);

	FAT_Entry_T *parentDirEntries = (FAT_Entry_T *)FAT_GetSectorBuffer();
	uint16_t entriesPerSector = volume->bytesPerSector / sizeof(FAT_Entry_T);

	FAT_Entry_T *fileEntry = 0;

	for (uint16_t i = 0; i < entriesPerSector; i++)
	{
		if (FAT_IsFreeEntry(&parentDirEntries[i]))
		{
			if (fileEntry == 0)
				fileEntry = &parentDirEntries[i];
		}
		else
		{
			if (strncmp(fileShortName, (char *)parentDirEntries[i].shortName, 12) == 0)
			{
				fileEntry = 0;
				break;
			}
		}
	}

	if (fileEntry == 0)
		while (1);

	if (firstCluster == 0)
		while (1);

	file->fileSize = 0;
	file->firstCluster = firstCluster;
	memcpy(file->fileName, fileShortName, 12);

	FAT_CreateFileEntry(file, fileEntry);

	FAT_WriteSector(parentDirSector);
}

void FAT_SetFileSize(uint32_t fileFirstCluster, uint32_t fileSize, FAT_Directory_T *parentDir, FAT_Volume_T *volume)
{
	if (fileFirstCluster <= 2)
		while (1);

	uint32_t parentDirSector = FAT_ClusterToSector(parentDir->firstCluster, volume);

	FAT_ReadSector(parentDirSector);

	FAT_Entry_T *parentDirEntries = (FAT_Entry_T *)FAT_GetSectorBuffer();
	uint16_t entriesPerSector = volume->bytesPerSector / sizeof(FAT_Entry_T);

	FAT_File_T file = {0};
	FAT_Entry_T *fileEntry = 0;

	for (uint16_t i = 0; i < entriesPerSector; i++)
	{
		FAT_ReadFileEntry(&file, &parentDirEntries[i]);
		if (file.firstCluster == fileFirstCluster)
		{
			fileEntry = &parentDirEntries[i];
			break;
		}
	}

	if (fileEntry == 0)
		while (1);

	file.fileSize = fileSize;

	FAT_CreateFileEntry(&file, fileEntry);

	FAT_WriteSector(parentDirSector);
}

uint32_t FAT_GetSectorAlignedFreeClusterStart(FAT_Volume_T *volume)
{
	uint32_t startCluster = FAT_GetFreeClustersStart(volume);
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;

	if (startCluster % clustersPerFatSector == 0)
	{
		return startCluster;
	}
	else
	{
		uint32_t startClusterFatSectorRelative = startCluster / clustersPerFatSector;
		startClusterFatSectorRelative++;
		startCluster = startClusterFatSectorRelative * clustersPerFatSector; // startCluster shoud be first in fat sector
		return startCluster;
	}
}

