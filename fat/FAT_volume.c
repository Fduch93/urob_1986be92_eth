#include "FAT_volume.h"
#include <string.h>

char FAT_BuildVolume(
	FAT_Volume_T *vloume,
	FAT_BootSector_T *bootSector,
	FAT_Partition_T *partition)
{
	if (bootSector->fatCount == 0 || bootSector->fatCount > 2)
		return 0;

	vloume->sectorCount = partition->sectorCount;
	memcpy(&vloume->bytesPerSector, bootSector->bytesPerSector, 2);
	vloume->sectorsPerCluster = bootSector->sectorsPerCluster;

	vloume->fatCount = bootSector->fatCount;
	memcpy(&vloume->sectorsPerFat, bootSector->sectorsPerFat, 4);

	uint16_t sectorsReserved = 0;
	memcpy(&sectorsReserved, bootSector->sectorsReserved, 2);

	vloume->fat1FirstSector = partition->firstSector + sectorsReserved;

	if (vloume->fatCount == 2)
		vloume->fat2FirstSector = vloume->fat1FirstSector + vloume->sectorsPerFat;

	vloume->firstClusterSector = vloume->fat1FirstSector + vloume->fatCount * vloume->sectorsPerFat;
	memcpy(&vloume->rootCluster, bootSector->rootCluster, 4);

	uint16_t infoSectorOffset = 0;
	memcpy(&infoSectorOffset, bootSector->infoSectorOffset, 2);
	vloume->infoSector = partition->firstSector + infoSectorOffset;

	return 1;
}
