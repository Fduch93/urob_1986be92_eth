#ifndef FAT_UTILS_H
#define FAT_UTILS_H

#include "FAT_volume.h"
#include "FAT_entry.h"

void FAT_ReadFirstVolume(FAT_Volume_T *volume);

uint32_t FAT_GetMaxClusterFromChain(uint32_t firstCluster, FAT_Volume_T *volume);
uint32_t FAT_GetMaxClusterFromDir(FAT_Directory_T *dir, FAT_Volume_T *volume);
uint32_t FAT_GetFreeClustersStart(FAT_Volume_T *volume);

void FAT_CreateDirectory(FAT_Directory_T *dir, char *dirName, FAT_Directory_T *parentDir, FAT_Volume_T *volume);
void FAT_CreateFile(FAT_File_T *file, char *fileName, uint32_t firstCluster, FAT_Directory_T *parentDir, FAT_Volume_T *volume);
void FAT_SetFileSize(uint32_t fileFirstCluster, uint32_t fileSize, FAT_Directory_T *parentDir, FAT_Volume_T *volume);
uint32_t FAT_GetSectorAlignedFreeClusterStart(FAT_Volume_T *volume);
void FAT_CreateWavFiles(FAT_Directory_T *wavDir, FAT_File_T *files, uint8_t fileCount, FAT_Volume_T *volume);
void FAT_Format(void);

#endif
