#ifndef FAT_VOLUME_H
#define FAT_VOLUME_H

#include "FAT_drive.h"

typedef struct
{
	uint8_t bootJump[3];
	uint8_t oemName[8];
	
	// DOS 2.0 BIOS Parameter Block
	uint8_t bytesPerSector[2]; // in powers of 2
	uint8_t sectorsPerCluster;
	uint8_t sectorsReserved[2]; // before FAT itself
	uint8_t fatCount;
	uint8_t fatRootCount[2]; // 0 for fat32
	uint8_t sectorsCount[2]; // 0 for fat32
	uint8_t mediaDescriptor;
	uint8_t sectorsPerFatDOS2[2]; // 0 for fat32
	
	// DOS 3.31 BIOS Parameter Block
	uint8_t sectorsPerTrack[2];
	uint8_t heads[2];
	uint8_t sectorsHidden[4];
	uint8_t sectorsTotalDOS3[4];
	
	// FAT32 Extended BIOS Parameter Block
	uint8_t sectorsPerFat[4];
	uint8_t descrMirror[2];
	uint8_t version[2];
	uint8_t rootCluster[4];
	uint8_t infoSectorOffset[2];
	uint8_t bootBackupSectorOffset[2];
	uint8_t reserved34h[12];
	uint8_t physicalDriveNumber;
	union {
		uint8_t reserved41h;
		struct {
			uint8_t dityFlag:1;
			uint8_t errorFlag:1;
			uint8_t :6;
		};
	};
	uint8_t extebdedBootSignature;
	uint8_t volumeID[4];
	uint8_t volumeLabel[11];
	uint8_t fileSystemType[8];
	
	uint8_t bootCode[420];
	
	uint8_t bootSectorSignature[2]; // 0x55AA
	
} FAT_BootSector_T;

typedef struct
{	
	uint32_t sectorCount;
	uint16_t bytesPerSector;
	uint8_t sectorsPerCluster;
	
	uint8_t fatCount;
	uint32_t sectorsPerFat;
	
	uint32_t fat1FirstSector;
	uint32_t fat2FirstSector;
	
	uint32_t firstClusterSector;
	uint32_t rootCluster;
	
	uint32_t infoSector;
} FAT_Volume_T;

char FAT_BuildVolume(
	FAT_Volume_T *vloume,
	FAT_BootSector_T *bootSector, 
	FAT_Partition_T *partition);

#endif
