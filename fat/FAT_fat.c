#include "FAT_hal.h"

#include "FAT_volume.h"

uint32_t FAT_ClusterToSector(uint32_t cluster, FAT_Volume_T *volume)
{
	return volume->firstClusterSector + (cluster - 2) * volume->sectorsPerCluster;
}

uint32_t FAT_GetFreeCluster(FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	for (uint32_t fatSectorRelative = 0; fatSectorRelative < volume->sectorsPerFat; fatSectorRelative++)
	{
		uint32_t fatSector = volume->fat1FirstSector + fatSectorRelative;

		FAT_ReadSector(fatSector);

		for (uint32_t clusterRelative = 0; clusterRelative < clustersPerFatSector; clusterRelative++)
		{
			if (fatSectorBuffer[clusterRelative] == 0)
			{
				uint32_t cluster = fatSectorRelative * clustersPerFatSector + clusterRelative;
				return cluster;
			}
		}
	}

	return 0;
}

uint32_t FAT_AllocateSingleCluster(FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;

	uint32_t cluster = FAT_GetFreeCluster(volume);

	if (cluster == 0)
		return 0;

	uint32_t clusterFatSectorRelative = cluster / clustersPerFatSector;
	uint32_t clusterFatSector = clusterFatSectorRelative + volume->fat1FirstSector;
	uint32_t clusterRelative = cluster % clustersPerFatSector;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	FAT_ReadSector(clusterFatSector);
	fatSectorBuffer[clusterRelative] = 0x0FFFFFFF;
	FAT_WriteSector(clusterFatSector);

	return cluster;
}

uint32_t FAT_AllocateChainCluster(uint32_t cluster, FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;

	uint32_t chainCluster = FAT_GetFreeCluster(volume);
	if (chainCluster == 0)
		return 0;

	uint32_t clusterFatSectorRelative = cluster / clustersPerFatSector;
	uint32_t clusterFatSector = clusterFatSectorRelative + volume->fat1FirstSector;
	uint32_t clusterRelative = cluster % clustersPerFatSector;

	uint32_t chainClusterFatSectorRelative = chainCluster / clustersPerFatSector;
	uint32_t chainClusterFatSector = chainClusterFatSectorRelative + volume->fat1FirstSector;
	uint32_t chainClusterRelative = chainCluster % clustersPerFatSector;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	FAT_ReadSector(clusterFatSector);
	fatSectorBuffer[clusterRelative] = chainCluster;
	if (clusterFatSector != chainClusterFatSector)
	{
		FAT_WriteSector(clusterFatSector);
		FAT_ReadSector(chainClusterFatSector);
	}
	fatSectorBuffer[chainClusterRelative] = 0x0FFFFFFF;
	FAT_WriteSector(chainClusterFatSector);

	return chainCluster;
}

static uint32_t lastAllocatedFatSectorRelative = 0;

void FAT_BeginInterleavedAllocation(
	uint32_t startCluster,
	uint8_t fatSectorBatch,
	uint8_t fileCount,
	FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;
	if (startCluster % clustersPerFatSector != 0)
		while (1);

	uint32_t startFatSectorRelative = startCluster / clustersPerFatSector;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	for (uint32_t fatSectorRelative = startFatSectorRelative; fatSectorRelative < startFatSectorRelative + fatSectorBatch; fatSectorRelative++)
	{
		uint32_t fatSector = fatSectorRelative + volume->fat1FirstSector;
		FAT_ReadSector(fatSector);

		for (uint32_t clusterRelative = 0; clusterRelative < clustersPerFatSector; clusterRelative++)
		{
			uint32_t cluster = fatSectorRelative * clustersPerFatSector + clusterRelative;
			uint32_t chainCluster = cluster + fileCount;

			fatSectorBuffer[clusterRelative] = chainCluster;
		}

		FAT_WriteSector(fatSector);

		lastAllocatedFatSectorRelative = fatSectorRelative;
	}
}

void FAT_ContinueInterleavedAllocation(
	uint8_t fatSectorBatch,
	uint8_t fileCount,
	FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;
	uint32_t startFatSectorRelative = lastAllocatedFatSectorRelative + 1;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	for (uint32_t fatSectorRelative = startFatSectorRelative; fatSectorRelative < startFatSectorRelative + fatSectorBatch; fatSectorRelative++)
	{
		uint32_t fatSector = fatSectorRelative + volume->fat1FirstSector;
		FAT_ReadSector(fatSector);

		for (uint32_t clusterRelative = 0; clusterRelative < clustersPerFatSector; clusterRelative++)
		{
			uint32_t cluster = fatSectorRelative * clustersPerFatSector + clusterRelative;
			uint32_t chainCluster = cluster + fileCount;

			fatSectorBuffer[clusterRelative] = chainCluster;
		}

		FAT_WriteSector(fatSector);

		lastAllocatedFatSectorRelative = fatSectorRelative;
	}
}

void FAT_FinishInterleavedAllocation(
	uint32_t startCluster,
	uint32_t clustersPerFile,
	uint8_t fileCount,
	FAT_Volume_T *volume)
{
	uint32_t clustersPerFatSector = volume->bytesPerSector / 4;

	uint32_t firstTerminalCluster = startCluster + fileCount * (clustersPerFile - 1);

	uint32_t firstTerminalFatSectorRelative = firstTerminalCluster / clustersPerFatSector;
	uint32_t terminalFatSector = firstTerminalFatSectorRelative + volume->fat1FirstSector;

	uint32_t *fatSectorBuffer = FAT_GetSectorBuffer32();

	FAT_ReadSector(terminalFatSector);

	for (uint8_t i = 0; i < fileCount; i++)
	{
		uint32_t terminalCluster = firstTerminalCluster + i;
		uint32_t terminalClusterRelative = terminalCluster % clustersPerFatSector;

		fatSectorBuffer[terminalClusterRelative] = 0x0FFFFFFF;

		if (terminalClusterRelative == clustersPerFatSector - 1)
		{
			FAT_WriteSector(terminalFatSector);
			terminalFatSector++;
			FAT_ReadSector(terminalFatSector);
		}
	}

	FAT_WriteSector(terminalFatSector);

	uint32_t firstLooseCluster = startCluster + fileCount * clustersPerFile;

	uint32_t firstLooseFatSectorRelative = firstLooseCluster / clustersPerFatSector;

	for (uint32_t looseFatSectorRelative = firstLooseFatSectorRelative; looseFatSectorRelative <= lastAllocatedFatSectorRelative; looseFatSectorRelative++)
	{
		uint32_t fatSector = looseFatSectorRelative + volume->fat1FirstSector;
		FAT_ReadSector(fatSector);

		for (uint32_t clusterRelative = 0; clusterRelative < clustersPerFatSector; clusterRelative++)
		{
			uint32_t cluster = looseFatSectorRelative * clustersPerFatSector + clusterRelative;

			if (cluster >= firstLooseCluster)
				fatSectorBuffer[clusterRelative] = 0;
		}

		FAT_WriteSector(fatSector);
	}
}
