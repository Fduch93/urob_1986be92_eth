#include "FAT_drive.h"
#include <string.h>

char FAT_BuildDrive(FAT_Drive_T *drive, FAT_MasterBootRecord_T *mbr)
{
	uint16_t mbrSignature = 0;
	memcpy(&mbrSignature, mbr->mbrSignature, 2);

	if (mbrSignature != 0xAA55)
		return 0;

	for (int i = 0; i < 4; i++)
	{
		drive->partitions[i].isActive = (mbr->partitions[i].isActive == 0x80);
		drive->partitions[i].partitionType = mbr->partitions[i].partitionType;
		memcpy(&drive->partitions[i].firstSector, mbr->partitions[i].firstSector, 4);
		memcpy(&drive->partitions[i].sectorCount, mbr->partitions[i].sectorCount, 4);
	}

	return 1;
}
