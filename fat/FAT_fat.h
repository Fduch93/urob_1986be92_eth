#ifndef FAT_FAT_H
#define FAT_FAT_H

#include "FAT_volume.h"

uint32_t FAT_ClusterToSector(uint32_t cluster, FAT_Volume_T *volume);
uint32_t FAT_GetFreeCluster(FAT_Volume_T *volume);
uint32_t FAT_AllocateSingleCluster(FAT_Volume_T *volume);
uint32_t FAT_AllocateChainCluster(uint32_t cluster, FAT_Volume_T *volume);

void FAT_BeginInterleavedAllocation(
	uint32_t startCluster,
	uint8_t fatSectorBatch,
	uint8_t fileCount,
	FAT_Volume_T *volume);

void FAT_ContinueInterleavedAllocation(
	uint8_t fatSectorBatch,
	uint8_t fileCount,
	FAT_Volume_T *volume);

void FAT_FinishInterleavedAllocation(
	uint32_t startCluster,
	uint32_t clustersPerFile,
	uint8_t fileCount,
	FAT_Volume_T *volume);

#endif
