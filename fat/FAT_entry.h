#ifndef FAT_ENTRY_H
#define FAT_ENTRY_H

#include "FAT_volume.h"
#include <stdint.h>

typedef struct
{
	uint8_t shortName[8];
	uint8_t shortExtension[3];
	uint8_t attributes;
	uint8_t userAttributes;
	uint8_t createTime[5];
	uint8_t lastAccessDate[2];
	uint8_t firstClusterHi[2];
	uint8_t lastModifiedTime[2];
	uint8_t lastModifiedDate[2];
	uint8_t firstClusterLo[2];
	uint8_t fileSize[4];
} FAT_Entry_T;

typedef struct
{
	char fileName[12];
	uint32_t firstCluster;
	uint32_t fileSize;
} FAT_File_T;

typedef struct FAT_Directory_T
{
	char dirName[12];
	uint32_t firstCluster;
	uint32_t dirSize;
	uint16_t subdirCount;
	uint16_t fileCount;
	struct FAT_Directory_T *subdirs;
	FAT_File_T *files;
} FAT_Directory_T;

char FAT_CreateFileEntry(FAT_File_T *file, FAT_Entry_T *fileEntry);
char FAT_CreateDirectoryEntry(FAT_Directory_T *dir, FAT_Entry_T *dirEntry);

char FAT_ReadFileEntry(FAT_File_T *file, FAT_Entry_T *fileEntry);
char FAT_ReadDirectoryEntry(FAT_Directory_T *dir, FAT_Entry_T *dirEntry);

char FAT_ReadRootDirectory(
	FAT_Directory_T *dir,
	FAT_Volume_T *volume,
	FAT_Directory_T *subdirs,
	uint32_t subdirCount,
	FAT_File_T *files,
	uint32_t fileCount);

char FAT_ReadDirectory(
	FAT_Directory_T *dir,
	FAT_Volume_T *volume,
	FAT_Directory_T *subdirs,
	uint32_t subdirCount,
	FAT_File_T *files,
	uint32_t fileCount);

#endif
