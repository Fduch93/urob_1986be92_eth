#include "FAT_entry.h"
#include "FAT_fat.h"
#include "FAT_hal.h"

#include <string.h>

static char FAT_isValidShortName(uint8_t shortName[8])
{
	if (shortName[0] == 0x00)
		return 0;

	if (shortName[0] == 0x05)
		return 0;

	if (shortName[0] == 0xE5)
		return 0;

	if (!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~_", shortName[0]))
		return 0;

	return 1;
}

static char FAT_FillName(char name[12], FAT_Entry_T *entry)
{
	if (entry == 0)
		return 0;

	if (!FAT_isValidShortName(entry->shortName))
		return 0;

	name[8] = '.';
	memcpy(&name[0], entry->shortName, 8);
	memcpy(&name[9], entry->shortExtension, 3);

	return 1;
}

static uint32_t FAT_GetFirstCluster(FAT_Entry_T *entry)
{
	if (entry == 0)
		return 0;

	uint16_t firstClusterHi;
	uint16_t firstClusterLo;
	memcpy(&firstClusterHi, entry->firstClusterHi, 2);
	memcpy(&firstClusterLo, entry->firstClusterLo, 2);

	return (firstClusterHi << 16) + firstClusterLo;
}

char FAT_CreateFileEntry(FAT_File_T *file, FAT_Entry_T *fileEntry)
{
	if (file == 0 || fileEntry == 0)
		return 0;

	memset(fileEntry, 0, sizeof(FAT_Entry_T));

	for (int i = 0; i < 8; i++)
		if (!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~_ ", file->fileName[i]))
			return 0;
	for (int i = 9; i < 12; i++)
		if (!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~_ ", file->fileName[i]))
			return 0;

	memcpy(fileEntry->shortName, &file->fileName[0], 8);
	memcpy(fileEntry->shortExtension, &file->fileName[9], 3);
	fileEntry->attributes = 0;

	uint16_t firstClusterHi = file->firstCluster >> 16;
	uint16_t firstClusterLo = file->firstCluster & 0xFFFF;

	memcpy(fileEntry->firstClusterHi, &firstClusterHi, 2);
	memcpy(fileEntry->firstClusterLo, &firstClusterLo, 2);

	memcpy(fileEntry->fileSize, &file->fileSize, 4);

	return 1;
}

char FAT_CreateDirectoryEntry(FAT_Directory_T *dir, FAT_Entry_T *dirEntry)
{
	if (dir == 0 || dirEntry == 0)
		return 0;

	memset(dirEntry, 0, sizeof(FAT_Entry_T));

	for (int i = 0; i < 8; i++)
		if (!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~_ .", dir->dirName[i]))
			return 0;
	for (int i = 9; i < 12; i++)
		if (!strchr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~_ .", dir->dirName[i]))
			return 0;

	memcpy(dirEntry->shortName, &dir->dirName[0], 8);
	memcpy(dirEntry->shortExtension, &dir->dirName[9], 3);
	dirEntry->attributes = 0x10;

	uint16_t firstClusterHi = dir->firstCluster >> 16;
	uint16_t firstClusterLo = dir->firstCluster & 0xFFFF;

	memcpy(dirEntry->firstClusterHi, &firstClusterHi, 2);
	memcpy(dirEntry->firstClusterLo, &firstClusterLo, 2);

	memcpy(dirEntry->fileSize, &dir->dirSize, 4);

	return 1;
}

char FAT_ReadFileEntry(FAT_File_T *file, FAT_Entry_T *fileEntry)
{
	if (file == 0 || fileEntry == 0)
		return 0;

	if (fileEntry->attributes & (0x10 | 0x08))
		return 0;

	if (!FAT_FillName(file->fileName, fileEntry))
		return 0;

	file->firstCluster = FAT_GetFirstCluster(fileEntry);
	memcpy(&file->fileSize, fileEntry->fileSize, 4);

	return 1;
}

char FAT_ReadDirectoryEntry(FAT_Directory_T *dir, FAT_Entry_T *dirEntry)
{
	if (dir == 0 || dirEntry == 0)
		return 0;

	if (!(dirEntry->attributes & 0x10))
		return 0;

	if (!FAT_FillName(dir->dirName, dirEntry))
		return 0;

	dir->firstCluster = FAT_GetFirstCluster(dirEntry);
	memcpy(&dir->dirSize, dirEntry->fileSize, 4);

	dir->subdirCount = 0;
	dir->fileCount = 0;

	dir->subdirs = 0;
	dir->files = 0;

	return 1;
}

char FAT_ReadRootDirectory(
	FAT_Directory_T *dir,
	FAT_Volume_T *volume,
	FAT_Directory_T *subdirs,
	uint32_t subdirCount,
	FAT_File_T *files,
	uint32_t fileCount)
{
	if (dir == 0 || volume == 0)
		return 0;

	if (volume->rootCluster < 2 || 
		volume->rootCluster >= volume->sectorsPerFat * volume->bytesPerSector / 4)
		return 0;

	FAT_ReadSector(FAT_ClusterToSector(volume->rootCluster, volume));

	FAT_Entry_T *dirEntry = (FAT_Entry_T *)FAT_GetSectorBuffer();

	if (!(dirEntry->attributes & (0x08)))
		return 0;

	if (!FAT_FillName(dir->dirName, dirEntry))
		return 0;

	dir->firstCluster = volume->rootCluster;
	memcpy(&dir->dirSize, dirEntry->fileSize, 4);

	dir->subdirCount = 0;
	dir->fileCount = 0;

	dir->subdirs = (subdirs && subdirCount) ? subdirs : 0;
	dir->files = (files && fileCount) ? files : 0;

	for (int i = 1; i < 16; i++)
	{
		if (FAT_isValidShortName(dirEntry[i].shortName))
		{
			if (dirEntry[i].attributes & 0x10)
			{
				if (subdirCount)
				{
					FAT_ReadDirectoryEntry(&subdirs[dir->subdirCount], &dirEntry[i]);
					subdirCount--;
				}
				dir->subdirCount++;
			}
			else if (dirEntry[i].attributes != 0x0F)
			{
				if (fileCount)
				{
					FAT_ReadFileEntry(&files[dir->fileCount], &dirEntry[i]);
					fileCount--;
				}
				dir->fileCount++;
			}
		}
	}

	return 1;
}

char FAT_ReadDirectory(
	FAT_Directory_T *dir,
	FAT_Volume_T *volume,
	FAT_Directory_T *subdirs,
	uint32_t subdirCount,
	FAT_File_T *files,
	uint32_t fileCount)
{
	if (dir == 0 || volume == 0)
		return 0;

	if (dir->firstCluster <= 2 || dir->firstCluster >= volume->sectorsPerFat / volume->sectorsPerCluster)
		return 0;

	dir->subdirCount = 0;
	dir->fileCount = 0;

	dir->subdirs = (subdirs && subdirCount) ? subdirs : 0;
	dir->files = (files && fileCount) ? files : 0;

	FAT_Entry_T *dirEntry = (FAT_Entry_T *)FAT_GetSectorBuffer();

	uint32_t startSector = FAT_ClusterToSector(dir->firstCluster, volume);// volume->firstClusterSector + (dir->firstCluster - 2) * volume->sectorsPerCluster;

	for (uint32_t sectorOffset = 0; sectorOffset < volume->sectorsPerCluster; sectorOffset++)
	{
		FAT_ReadSector(startSector + sectorOffset);

		for (int i = 0; i < 16; i++)
		{
			if (FAT_isValidShortName(dirEntry[i].shortName))
			{
				if (dirEntry[i].attributes & 0x10)
				{
					if (subdirCount)
					{
						FAT_ReadDirectoryEntry(&subdirs[dir->subdirCount], &dirEntry[i]);
						subdirCount--;
					}
					dir->subdirCount++;
				}
				else if (dirEntry[i].attributes != 0x0F)
				{
					if (fileCount)
					{
						FAT_ReadFileEntry(&files[dir->fileCount], &dirEntry[i]);
						fileCount--;
					}
					dir->fileCount++;
				}
			}
		}
	}

	return 1;
}
