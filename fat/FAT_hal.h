#ifndef FAT_HAL_H
#define FAT_HAL_H

#include <stdint.h>

uint8_t *FAT_GetSectorBuffer(void);
uint32_t *FAT_GetSectorBuffer32(void);
char FAT_ReadSector(uint32_t sector);
char FAT_WriteSector(uint32_t sector);
char FAT_ReadSectorMulti(uint8_t *buffer, uint32_t sector, uint32_t sectorCnt);
char FAT_WriteSectorMulti(uint8_t *buffer, uint32_t sector, uint32_t sectorCnt);

#endif
