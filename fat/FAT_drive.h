#ifndef FAT_DRIVE_H
#define FAT_DRIVE_H

#include <stdint.h>

typedef struct
{
	uint8_t isActive;
	uint8_t startHead;
	uint8_t startSector;
	uint8_t startCylinder;
	uint8_t partitionType;
	uint8_t stopHead;
	uint8_t stopSector;
	uint8_t stopCylinder;
	uint8_t firstSector[4];
	uint8_t sectorCount[4];
} FAT_PartitionEntry_T;

typedef struct
{
	uint8_t bootCode[446];
	FAT_PartitionEntry_T partitions[4];
	uint8_t mbrSignature[2];
	
} FAT_MasterBootRecord_T;

typedef struct
{
	char isActive;
	uint8_t partitionType;
	uint32_t firstSector;
	uint32_t sectorCount;
} FAT_Partition_T;

typedef struct
{
	FAT_Partition_T partitions[4];	
} FAT_Drive_T;

char FAT_BuildDrive(FAT_Drive_T *drive, FAT_MasterBootRecord_T *mbr);

#endif
