#ifndef LOG_H
#define LOG_H

#include <stdint.h>

#define LOG_SIZE 500

uint32_t LOG_GetTotalCount(void);
uint16_t LOG_Get(uint32_t i, char *out, uint16_t outMaxLen);

void LOG_Log(uint32_t source, int varCnt, ...);

#define LOG_VA_ARGS_COUNT(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,N,...) N
#define LOG_VAR_COUNT(...) LOG_VA_ARGS_COUNT(__VA_ARGS__,10,9,8,7,6,5,4,3,2,1,0)
#define LOG(...) LOG_Log(LOG_SOURCE_COMMON, LOG_VAR_COUNT(__VA_ARGS__), __VA_ARGS__)

#define LOG_FROM_SOURCE(src, ...) LOG_Log(src, LOG_VAR_COUNT(__VA_ARGS__), __VA_ARGS__)

#define LOG_QWE(a,b) a ## b
#define LOG_UNIQUE(a,b) LOG_QWE(a,b)
#define LOG_ONCE(...) static char LOG_UNIQUE(var,__LINE__) = 1; if (LOG_UNIQUE(var,__LINE__)) { LOG(__VA_ARGS__); LOG_UNIQUE(var,__LINE__) = 0; }

#define LOG_SOURCE_ALL			0xFFFFFFFFU
#define LOG_SOURCE_COMMON		(1U << 31)
#define LOG_SOURCE_USBD			(1U << 0)
#define LOG_SOURCE_USBD_IRQ		(1U << 1)
#define LOG_SOURCE_USBD_XFER	(1U << 2)
#define LOG_SOURCE_USBD_ENUM	(1U << 3)

#define LOG_SOURCE_INITIALIZER (\
	LOG_SOURCE_COMMON		|\
	LOG_SOURCE_USBD			|\
	LOG_SOURCE_USBD_IRQ		|\
	LOG_SOURCE_USBD_ENUM	|\
0)

#endif
