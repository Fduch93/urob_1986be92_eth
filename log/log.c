#include "log.h"
#include "TICK.h"
#include "num2str.h"
#include <string.h>
#include <stdarg.h>

#define LOG_SIZE 500
#define TIME TICK_GetHCLK()

static uint32_t *GetEntry(uint16_t idx);
static uint16_t LOG_MakeString(uint32_t *entry, char *out, uint16_t outMaxLen);

static uint32_t log[LOG_SIZE] = {0};
static uint16_t logPos = 0;
static uint32_t logTotalCount = 0;
uint32_t LOG_GetTotalCount(void) { return logTotalCount; }

static uint32_t sourceEnabled = LOG_SOURCE_INITIALIZER;

void LOG_EnableSource(uint32_t source)  { sourceEnabled |=  source; }
void LOG_DisableSource(uint32_t source) { sourceEnabled &= ~source; }

void LOG_Log(uint32_t source, int varCnt, ...)
{
	if (!(source & sourceEnabled))
		return;
	
	va_list args;
	va_start(args, varCnt);

	for (int i = varCnt-1; i >= 0; i--)
		{
			int pos = logPos + i;

			if (pos >= LOG_SIZE)
				pos -= LOG_SIZE;

			log[pos] = (uint32_t)va_arg(args, int);
		}

	logPos += varCnt;
	if (logPos >= LOG_SIZE)
		logPos -= LOG_SIZE;

	log[logPos++] = (uint32_t)(TIME << 16 | (varCnt + 1));
	if (logPos >= LOG_SIZE)
		logPos -= LOG_SIZE;
	
	logTotalCount++;

	va_end(args);
}

uint16_t LOG_Get(uint32_t i, char *out, uint16_t outMaxLen)
{
	uint32_t *entry = GetEntry(i-1);
	
	if (out)
	{
		if (entry)
		{
			return LOG_MakeString(entry, out, outMaxLen);
		}
		else
		{
			*out = 0;
			strcat(out, "LOST");
			return 4;
		}
	}
	else
	{
		if (entry)
		{
			char qwe[256];
			return LOG_MakeString(entry, qwe, sizeof(qwe));
		}
		else
		{
			return 4;
		}
	}
}

static uint32_t *GetEntry(uint16_t idx)
{
	if (logPos == 0 && log[LOG_SIZE-1] == 0)
		return 0; // log seems empty
	if (idx > logTotalCount)
		return 0; // let us call this an underrun exception
	
	int start = logPos ? logPos - 1 : LOG_SIZE - 1;
		
	if (idx == 0)
	{
		return &log[start];
	}
	else
	{
		int current = start;
		char overFlag = 0;
		for (int i = 0; i < idx; i++)
		{
			current -= log[current] & 0xFF;
			
			if (current < 0)
			{
				current += LOG_SIZE;
				overFlag = 1;
			}
			
			if ((!overFlag && current >= start) ||
				(overFlag && current <= start))
				return 0; // that one is already overwritten
		}
		return  &log[current];
	}
}

static uint16_t LOG_MakeString(uint32_t *entry, char *out, uint16_t outMaxLen)
{
	#define DECREASE_ENTRY \
		if (entry > log) entry--;\
		else entry = &log[LOG_SIZE-1];\
	
	if (! entry)
		return 0;
	
	uint8_t entrySz = *entry & 0xFF;
	uint16_t time = *entry >> 16;
	
	DECREASE_ENTRY;
	
	char *pattern = (char *)*entry;
	
	*out = 0;
	
	if (!pattern)
		return 0;

	num2str(out, time, 10);
	strcat(out, ": ");

	const char *wildchar = strchr(pattern, '%');
	if (wildchar == 0)
	{
		strcat(out, pattern);
	}
	else
	{
		strncat(out, pattern, wildchar - pattern);
		for (int i = 0; i < entrySz - 2; i++)
		{
			DECREASE_ENTRY;
			uint32_t var = *entry;
			
			if (wildchar)
			{
				switch (*(wildchar+1))
				{
					case 's':
					{
						strcat(out, (char *)var);
						break;
					}
					case 'x':
					case 'd':
					{
						uint32_t cntBase = *(wildchar+1) == 'x' ? 16 : 10;

						num2str(out, var, cntBase);
						break;
					}
					case 'a':
					{
						uint8_t *a = (uint8_t *)&var;

						num2str(out, a[0], 10);
						strcat(out,".");
						num2str(out, a[1], 10);
						strcat(out,".");
						num2str(out, a[2], 10);
						strcat(out,".");
						num2str(out, a[3], 10);
						break;
					}

					default:
					{
						strcat(out, wildchar);
						break;
					}
				}

				char *next = strchr(wildchar+2, '%');
				if (next)
					strncat(out, wildchar+2, next - (wildchar+2));
				else
				{
					strcat(out, wildchar + 2);
					return strlen(out);
				}

				wildchar = next;
			}
		}
	}

	return strlen(out);
}
