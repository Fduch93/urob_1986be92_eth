#include "udp.h"
#include "checksum.h"
#include "beconv.h"
#include "TICK.h"
#include <string.h>

static void UDP_Build(uint8_t *pHeader, uint16_t fromPort, uint16_t toPort, uint8_t *data, uint16_t len);

extern uint16_t DHCP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)			__attribute__((weak, alias("DHCP_FilterDefault")));
extern uint16_t NBNS_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)			__attribute__((weak, alias("NBNS_FilterDefault")));
extern uint16_t LLMNR_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)			__attribute__((weak, alias("LLMNR_FilterDefault")));

static uint16_t DHCP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)	{ return 0; }
static uint16_t NBNS_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)	{ return 0; }
static uint16_t LLMNR_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)	{ return 0; }

extern uint16_t DHCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])			__attribute__((weak, alias("DHCP_ProcessDefault")));
extern uint16_t NBNS_Process(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])			__attribute__((weak, alias("NBNS_ProcessDefault")));
extern uint16_t LLMNR_Process(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])			__attribute__((weak, alias("LLMNR_ProcessDefault")));

static uint16_t DHCP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])		{ return 0; }
static uint16_t NBNS_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])		{ return 0; }
static uint16_t LLMNR_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])	{ return 0; }

uint16_t UDP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast)
{
	*isBroadcast = 0;

	UDP_Header_T header = {0};
	memcpy(&header, data, sizeof(UDP_Header_T));

	if (len == ntohs(header.len))
	{
		uint8_t *payload = &data[sizeof(UDP_Header_T)];
		uint16_t payloadLen = len - sizeof(UDP_Header_T);

		uint16_t udpResponseLen = 0;

		switch (header.toPort)
		{
			case UDP_DHCP_CLIENT_PORT:
				*isBroadcast = 1;
				udpResponseLen = DHCP_Filter(payload, payloadLen, responseMaxLen - sizeof(UDP_Header_T));
				break;
			case UDP_NBNS_PORT:
				udpResponseLen = NBNS_Filter(payload, payloadLen, responseMaxLen - sizeof(UDP_Header_T));
				break;
			case UDP_LLMNR_PORT:
				udpResponseLen = LLMNR_Filter(payload, payloadLen, responseMaxLen - sizeof(UDP_Header_T));
				break;
			default:
				break;
		}

		if (udpResponseLen > 0)
		{
			UDP_Build(
				data,
				header.toPort, header.fromPort,
				payload, udpResponseLen);

			return sizeof(UDP_Header_T) + udpResponseLen;
		}
	}

	return 0;
}

uint16_t UDP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])
{
	uint8_t *payload = sendData + sizeof(UDP_Header_T);
	uint16_t payloadMaxLen = sendMaxLen - sizeof(UDP_Header_T);

	uint16_t fromPort = 0;
	uint16_t toPort = 0;

	uint16_t payloadLen = 0;

	if (!payloadLen)
		payloadLen = DHCP_Process(payload, payloadMaxLen, &fromPort, &toPort, toIp, toMac);
	if (!payloadLen)
		payloadLen = NBNS_Process(payload, payloadMaxLen, &fromPort, &toPort, toIp, toMac);
	if (!payloadLen)
		payloadLen = LLMNR_Process(payload, payloadMaxLen, &fromPort, &toPort, toIp, toMac);

	if (payloadLen)
	{
		UDP_Build(sendData, fromPort, toPort, sendData, payloadLen);
		return payloadLen + sizeof(UDP_Header_T);
	}

	return 0;
}

static void UDP_Build(uint8_t *pHeader, uint16_t fromPort, uint16_t toPort, uint8_t *data, uint16_t len)
{
	UDP_Header_T header = {0};

	header.fromPort = fromPort;
	header.toPort = toPort;
	header.len = htons(len + sizeof(UDP_Header_T));
	header.cksum = 0; // may be zero if unused
// TODO: implement proper udp checksum calculation
//#define IP_PROTOCOL_UDP		17
//	header.cksum = checksum(*packetLen + IP_PROTOCOL_UDP, (uint8_t*)packet, *packetLen);

	memcpy(pHeader, &header, sizeof(UDP_Header_T));
}
