#include "http.h"
#include "MDR32Fx.h"
#include "TICK.h"
#include <string.h>
#include "whebbpakk.h"
#include "log.h"
#include "tcp.h"

const char *header200 = "\
HTTP/1.1 200 OK\r\n\
Server: MDR32\r\n\
Content-Type: ";

static const char *header304 = "\
HTTP/1.1 304 Not Modified\r\n\
Server: MDR32\r\n\r\n";

static const char *status200 = "HTTP/1.1 200 OK";
static const char *status404 = "HTTP/1.1 404 Not Found";

extern uint16_t HTTP_Build(
	uint8_t *response,
	const char *status,
	const char *contentType,
	const char *body);

static uint16_t GetLog(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen);

uint16_t HTTP_HandlePost(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen)
{
	if (HTTP_CompareUrl(request->url, request->urlLen, "/led"))
	{
		LOG("HTTP POST TOGGLE SET %d", !(MDR_PORTE->RXTX & 0x04));
		MDR_PORTE->RXTX ^= 0x04;

		return HTTP_Build(response, status200, 0, 0);
	}
	else if (HTTP_CompareUrl(request->url, request->urlLen, "/log") && request->bodyLen > 0)
	{
		return GetLog(request, response, responseMaxLen);
	}
	else
	{
		const WP_Resource_T *res = WP_GetResource("/notfound.html", strlen("/notfound.html"));

		return HTTP_Build(
			response,
			status404,
			res->mime,
			res->rsrc);
	}
}

static uint16_t GetLog(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen)
{
	char *resp = (char *)response;
	*resp = 0;

	uint32_t logTotalRead = 0;
	uint32_t mul = 1;
	for (int i = request->bodyLen-1; i >= 0; i--)
	{
		logTotalRead += (request->body[i] - '0') * mul;
		mul *= 10;
	}

	uint32_t logTotalCount = LOG_GetTotalCount();

	if (logTotalRead + LOG_SIZE < logTotalCount)
		logTotalRead = logTotalCount - LOG_SIZE;


	if (logTotalRead < logTotalCount)
	{
		strcat(resp, header200);
		strcat(resp, "text/plain");
		uint16_t bodyLen = LOG_Get(logTotalCount - logTotalRead, 0, 0);

		strcat(resp, "\r\nContent-Length: ");
		num2dec(resp, bodyLen);
		strcat(resp, "\r\n\r\n");
		LOG_Get(logTotalCount - logTotalRead, &resp[strlen(resp)], responseMaxLen - strlen(resp));
	}
	else
	{
		char *resp = (char *)response;
		*resp = 0;
		strcat(resp, header304);
	}

	return strlen(resp);
}
