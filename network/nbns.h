#ifndef NBNS_H
#define NBNS_H

#include <stdint.h>

#define NBNS_NAME_LEN_MAX 64

typedef enum
{
	MSG_QUERY = 0,
	MSG_REPLY = 1
} NBNS_MessageType_E;

typedef enum
{
	OP_QUERY = 0,
	OP_IQUERY = 1,
	OP_STATUS = 2,
	OP_NOTIFY = 4,
	OP_UPDATE = 5
} NBNS_OpCode_E;

typedef enum
{
	RESP_NO_ERROR = 0,
	RESP_FORMAT_ERROR = 1,
	RESP_SERVER_FAIL = 2,
	RESP_NON_EXIXTENT_DOMAIN = 3
} NBNS_ResponseCode_E;

typedef union
{
	uint16_t u16[6];
	struct
	{
		uint16_t transactionId;
		union {
			uint16_t flags;
			struct {
				uint16_t respCode:4;
				uint16_t noAuthAcceptableOrBroadcast:1;
				uint16_t :2;
				uint16_t recursionAvailable:1;
				uint16_t recursionDesired:1;
				uint16_t trunc:1;
				uint16_t authAnswer:1;
				uint16_t opCode:4;
				uint16_t msgType:1;
			};
		};
		uint16_t questionCnt;
		uint16_t answerCnt;
		uint16_t authorityCnt;
		uint16_t additionalCnt;
	};
} NBNS_Header_T;

typedef union
{
	uint16_t u16[2];
	struct {
		uint16_t queryType;
		struct {
			uint16_t queryClass:15;
			uint16_t uniResp:1;
		};
	};
} NBNS_QueryTail_T;

typedef struct
{
	NBNS_Header_T header;
	uint16_t name[NBNS_NAME_LEN_MAX];
	uint8_t nameLen;
	NBNS_QueryTail_T tail;
} NBNS_Query_T;

typedef union
{
	uint16_t u16;
	struct {
		uint16_t :13;
		uint16_t ownerNodeType:2;
		uint16_t group:1;
	};
} NB_Flags_T;

typedef union
{
	uint16_t u16[6+2];
	struct {
		uint16_t rrType;
		struct {
			uint16_t rrClass:15;
			uint16_t cacheFlush:1;
		};
		uint32_t ttl;
		uint16_t rrDataLen;
		NB_Flags_T nbFlags;
		uint32_t ipAddr;
	};
} NBNS_RsrcRecordTail_T;

typedef struct
{
	uint16_t name[NBNS_NAME_LEN_MAX];
	uint8_t nameLen;
	NBNS_RsrcRecordTail_T tail;
} NBNS_RsrcRecord_T;

typedef struct
{
	NBNS_Query_T query;
	NBNS_RsrcRecord_T rsrcRecord;
} NBNS_Response_T;

uint16_t NBNS_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);

extern uint32_t NBNS_GetIpExt(void);

#endif
