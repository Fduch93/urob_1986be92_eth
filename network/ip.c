#include "ip.h"
#include "checksum.h"
#include "beconv.h"
#include "TICK.h"
#include <string.h>

#define IP_PACKET_TTL 64

static uint32_t ipAddr = 0;
static uint32_t ipMask = 0;
static uint32_t ipGateway = 0;

uint32_t IP_GetAddr()					{ return ipAddr; }
uint32_t IP_GetMask()					{ return ipMask; }
uint32_t IP_GetGateway()				{ return ipGateway; }
void IP_SetAddr(uint32_t ip)			{ ipAddr = ip; }
void IP_SetMask(uint32_t mask)			{ ipMask = mask; }
void IP_SetGateway(uint32_t gateway)	{ ipGateway = gateway; }

static void IP_Build(uint8_t *pHeader, uint8_t protocol, uint32_t toAddr, uint16_t dataLen);

extern uint16_t ICMP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)						__attribute__((weak, alias("ICMP_FilterDefault")));
extern uint16_t UDP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast)	__attribute__((weak, alias("UDP_FilterDefault")));
extern uint16_t TCP_Filter(uint32_t fromIp, uint8_t *data, uint16_t len, uint16_t responseMaxLen)		__attribute__((weak, alias("TCP_FilterDefault")));

extern uint16_t ICMP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	__attribute__((weak, alias("ICMP_ProcessDefault")));
extern uint16_t UDP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	__attribute__((weak, alias("UDP_ProcessDefault")));
extern uint16_t TCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	__attribute__((weak, alias("TCP_ProcessDefault")));

static uint16_t ICMP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)						{ return 0; }
static uint16_t UDP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast)	{ return 0; }
static uint16_t TCP_FilterDefault(uint32_t fromIp, uint8_t *data, uint16_t len, uint16_t responseMaxLen)		{ return 0; }

static uint16_t ICMP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	{ return 0; }
static uint16_t UDP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	{ return 0; }
static uint16_t TCP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	{ return 0; }

uint16_t IP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast)
{
	*isBroadcast = 0;

	if (len <= sizeof(IP_Header_T))
		return 0;

	IP_Header_T header = {0};
	memcpy(&header, data, sizeof(IP_Header_T));

	// TODO fix >=
	if (len >= ntohs(header.totalLen))
	{
		uint16_t hdrChecksumRead = header.cksum;
		header.cksum = 0;
		uint16_t hdrChecksumCalc = checksum(0, (uint8_t *)&header, sizeof(IP_Header_T));

		int isToBroadcast = header.toAddr == (ipAddr | ~ipMask);
		int isMulticast = (header.toAddr & 0x000000E0) == 0x000000E0;

		if ((header.verHeadLen == 0x45) &&
			(hdrChecksumCalc == hdrChecksumRead) &&
			((header.toAddr == ipAddr) || isToBroadcast || isMulticast))
		{
			uint8_t *payload = &data[sizeof(IP_Header_T)];
			uint16_t payloadLen = htons(header.totalLen) - sizeof(IP_Header_T);

			uint16_t ipResponseLen = 0;

			switch (header.protocol)
			{
				case IP_PROTOCOL_ICMP:
					ipResponseLen = ICMP_Filter(payload, payloadLen, responseMaxLen - sizeof(IP_Header_T));
					break;
				case IP_PROTOCOL_UDP:
					ipResponseLen = UDP_Filter(payload, payloadLen, responseMaxLen - sizeof(IP_Header_T), isBroadcast);
					break;
				case IP_PROTOCOL_TCP:
					ipResponseLen = TCP_Filter(header.fromAddr, payload, payloadLen, responseMaxLen - sizeof(IP_Header_T));
					break;
				default:
					break;
			}

			if (ipResponseLen > 0)
			{
				IP_Build(
					data,
					header.protocol,
					*isBroadcast
						? (ipAddr | ~ipMask)
						: header.fromAddr,
					ipResponseLen);

				return sizeof(IP_Header_T) + ipResponseLen;
			}
		}
	}

	return 0;
}

uint16_t IP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6])
{
	uint8_t *payload = sendData + sizeof(IP_Header_T);
	uint16_t payloadMaxLen = sendMaxLen - sizeof(IP_Header_T);

	uint32_t toIp = 0;

	uint16_t payloadLen = 0;
	uint8_t protocol = 0;

	if (!payloadLen)
	{
		payloadLen = ICMP_Process(payload, payloadMaxLen, &toIp, toMac);
		protocol = IP_PROTOCOL_ICMP;
	}
	if (!payloadLen)
	{
		payloadLen = UDP_Process(payload, payloadMaxLen, &toIp, toMac);
		protocol = IP_PROTOCOL_UDP;
	}
	if (!payloadLen)
	{
		payloadLen = TCP_Process(payload, payloadMaxLen, &toIp, toMac);
		protocol = IP_PROTOCOL_TCP;
	}

	if (payloadLen)
	{
		IP_Build(sendData, protocol, toIp, payloadLen);
		return payloadLen + sizeof(IP_Header_T);
	}

	return 0;
}

static void IP_Build(uint8_t *pHeader, uint8_t protocol, uint32_t toAddr, uint16_t dataLen)
{
	IP_Header_T header = {0};

	header.protocol = protocol;
	header.toAddr = toAddr;
	header.verHeadLen = 0x45;
	header.tos = 0;
	header.totalLen = htons(sizeof(IP_Header_T) + dataLen);
	header.fragmentId = 0;
	header.flagsFramgentOffset = 0;
	header.ttl = IP_PACKET_TTL;
	header.fromAddr = ipAddr;
	header.cksum = 0;
	header.cksum = checksum(0, (uint8_t *)&header, sizeof(IP_Header_T));

	memcpy(pHeader, &header, sizeof(IP_Header_T));
}
