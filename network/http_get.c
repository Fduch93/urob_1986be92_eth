#include "http.h"
#include "TICK.h"
#include "whebbpakk.h"
#include "log.h"
#include <string.h>

static const char *status200 = "HTTP/1.1 200 OK";
static const char *status404 = "HTTP/1.1 404 Not Found";

static uint16_t GetSec(uint8_t *response, uint16_t responseMaxLen);
static uint16_t GetLog(uint8_t *response, uint16_t responseMaxLen);
static uint16_t GetWebSocket(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen);

extern uint16_t HTTP_Build(
	uint8_t *response,
	const char *status,
	const char *contentType,
	const char *body);

uint16_t HTTP_HandleGet(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen)
{
	const WP_Resource_T *res = WP_GetResource(request->url, request->urlLen);
	if (res)
	{
		LOG("HTTP GET %s", (uint32_t)res->path);
		return HTTP_Build(
			response,
			status200,
			res->mime,
			res->rsrc);
	}
	else if (HTTP_CompareUrl(request->url, request->urlLen, "/sec"))
	{
		return GetSec(response, responseMaxLen);
	}
	else if (HTTP_CompareUrl(request->url, request->urlLen, "/log"))
	{
		return GetLog(response, responseMaxLen);
	}
	else if (HTTP_CompareUrl(request->url, request->urlLen, "/ws"))
	{
		return GetWebSocket(request, response, responseMaxLen);
	}
	else
	{
		res = WP_GetResource("/notfound.html", strlen("/notfound.html"));

		return HTTP_Build(
			response,
			status404,
			res->mime,
			res->rsrc);
	}
}

static uint16_t GetSec(uint8_t *response, uint16_t responseMaxLen)
{
	char sec[11] = {0};
	num2dec(sec, SECONDS);

	return HTTP_Build(
		response,
		status200,
		"text/plain",
		sec);
}

static uint16_t GetLog(uint8_t *response, uint16_t responseMaxLen)
{
	char str[11] = {0};
	num2dec(str, LOG_GetTotalCount());

	return HTTP_Build(
		response,
		status200,
		"text/plain",
		str);
}

extern void WEBSOCK_MakeResponseKey(char *key, uint8_t keyLen, char base64[29]);
static uint16_t GetWebSocket(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen)
{
	char *WSHDRKEY = "Sec-WebSocket-Key: ";

	static const char *header101 = "\
HTTP/1.1 101 Switching Protocols\r\n\
Upgrade: websocket\r\n\
Connection: keep-alive, Upgrade\r\n\
Sec-WebSocket-Accept: ";

	char *key = strstr(request->headers, WSHDRKEY) + strlen(WSHDRKEY);
	uint8_t keyLen = strstr(key, "\r\n") - key;

	char base64[29];
	WEBSOCK_MakeResponseKey(key, keyLen, base64);

	char *resp = (char *)response;
	*resp = 0;
	strcat(resp, header101);
	strcat(resp, base64);
	strcat(resp, "\r\n\r\n");
	return strlen(resp);
}
