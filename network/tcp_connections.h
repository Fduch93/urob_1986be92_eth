#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include <stdint.h>

#define TCP_MAX_CONNECTIONS	10
#define TCP_CONN_TIMEOUT	5
#define TCP_BUFFER_SIZE		1024

typedef enum {
	TCP_CLOSED,
	TCP_SYN_SENT,
	TCP_SYN_RECEIVED,
	TCP_ESTABLISHED,
	TCP_FIN_WAIT,
	TCP_FIN_ACK_WAIT
} TCP_StatusCode_T;

typedef enum {
	TCP_BUF_EMPTY,
	TCP_BUF_RECIEVE,
	TCP_BUF_SEND
} TCP_BufferStatus_T;

typedef enum
{
	TCP_PROTOCOL_UNKNOWN,
	TCP_PROTOCOL_HTTP,
	TCP_PROTOCOL_WEBSOCK
} TCP_Protocol_T;

typedef struct {
	TCP_StatusCode_T status;
	uint32_t eventTime;
	uint32_t seqNum;
	uint32_t ackNum;
	uint32_t remoteAddr;
	uint16_t remotePort;
	uint16_t localPort;
	uint8_t *buf;
	uint16_t bufLen;
	TCP_BufferStatus_T bufStatus;
	TCP_Protocol_T protocol;
} TCP_Connection_T;

TCP_Connection_T *TCP_GetConnection(int index);
TCP_Connection_T *TCP_NewConnection(uint32_t fromAddr, uint16_t remotePort, uint16_t localPort, uint32_t peerSeqNum);
TCP_Connection_T *TCP_FindConnection(uint32_t fromAddr, uint16_t remotePort, uint16_t localPort);

#endif
