#ifndef LLMNR_H
#define LLMNR_H

#include "nbns.h"

uint16_t LLMNR_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);

#endif
