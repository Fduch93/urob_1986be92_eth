#include "nbns.h"
#include "TICK.h"
#include "beconv.h"
#include <string.h>

static char NBNS_IsMyHostName(uint8_t *name, const char *myName);
static void NBNS_ParseQuery(NBNS_Query_T *query, const uint8_t *data, uint16_t len);
static uint16_t NBNS_BuildResponse(NBNS_Response_T *resp, uint8_t *buf, uint16_t responseMaxLen);

uint16_t NBNS_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	NBNS_Query_T query;
	NBNS_ParseQuery(&query, data, len);

	if (NBNS_IsMyHostName((uint8_t *)query.name, "MDR32") ||
		NBNS_IsMyHostName((uint8_t *)query.name, "MDR32.RU"))
	{
		NBNS_Response_T resp = {0};
		memcpy(&resp.query, &query, sizeof(NBNS_Query_T));

		resp.query.header.msgType = MSG_REPLY;
		resp.query.header.opCode = OP_QUERY;
		resp.query.header.authAnswer = 1;
		resp.query.header.trunc = 0;
		resp.query.header.recursionDesired = 1;//0;
		resp.query.header.recursionAvailable = 0;
		resp.query.header.noAuthAcceptableOrBroadcast = 0;//1;
		resp.query.header.respCode = 0;
		resp.query.header.questionCnt = 0;
		resp.query.header.answerCnt = 1;

		memcpy(resp.rsrcRecord.name, query.name, query.nameLen+1);
		resp.rsrcRecord.nameLen = query.nameLen;

		resp.rsrcRecord.tail.rrType = query.tail.queryType;
		resp.rsrcRecord.tail.rrClass = query.tail.queryClass;
		resp.rsrcRecord.tail.cacheFlush = 0;
		resp.rsrcRecord.tail.ttl = 0xFF000000;
		resp.rsrcRecord.tail.rrDataLen = 6;
		resp.rsrcRecord.tail.nbFlags.group = 0;
		resp.rsrcRecord.tail.nbFlags.ownerNodeType = 0;

		uint32_t ipAddr = NBNS_GetIpExt();
		resp.rsrcRecord.tail.ipAddr = ipAddr;

		return NBNS_BuildResponse(&resp, data, responseMaxLen);
	}

	return 0;
}

static void NBNS_ParseQuery(NBNS_Query_T *query, const uint8_t *data, uint16_t len)
{
	memcpy(query->header.u16, data, sizeof(NBNS_Header_T));
	for (uint32_t i = 0; i < sizeof(NBNS_Header_T)/2; i++)
		query->header.u16[i] = ntohs(query->header.u16[i]);
	data += sizeof(NBNS_Header_T);

	query->nameLen = *data;
	data++;

	memcpy(query->name, data, query->nameLen+1);
	data += query->nameLen+1;

	memcpy(query->tail.u16, data, sizeof(NBNS_QueryTail_T));
	for (uint32_t i = 0; i < sizeof(NBNS_QueryTail_T)/2; i++)
		query->tail.u16[i] = ntohs(query->tail.u16[i]);
}

static uint16_t NBNS_BuildResponse(NBNS_Response_T *resp, uint8_t *buf, uint16_t responseMaxLen)
{
	uint8_t *pBuf = buf;

	for (uint32_t i = 0; i < sizeof(NBNS_Header_T)/2; i++)
		resp->query.header.u16[i] = ntohs(resp->query.header.u16[i]);
	memcpy(pBuf, resp->query.header.u16, sizeof(NBNS_Header_T));
	for (uint32_t i = 0; i < sizeof(NBNS_Header_T)/2; i++)
		resp->query.header.u16[i] = ntohs(resp->query.header.u16[i]);
	pBuf += sizeof(NBNS_Header_T);

	*pBuf = resp->rsrcRecord.nameLen;
	pBuf++;

	memcpy(pBuf, resp->rsrcRecord.name, resp->rsrcRecord.nameLen+1);
	pBuf += resp->rsrcRecord.nameLen+1;

	for (uint32_t i = 0; i < sizeof(NBNS_RsrcRecordTail_T)/2-2; i++)
		resp->rsrcRecord.tail.u16[i] = ntohs(resp->rsrcRecord.tail.u16[i]);
	memcpy(pBuf, resp->rsrcRecord.tail.u16, sizeof(NBNS_RsrcRecordTail_T));
	for (uint32_t i = 0; i < sizeof(NBNS_RsrcRecordTail_T)/2-2; i++)
		resp->rsrcRecord.tail.u16[i] = ntohs(resp->rsrcRecord.tail.u16[i]);
	pBuf += sizeof(NBNS_RsrcRecordTail_T);

	return pBuf - buf;
}

static char NBNS_IsMyHostName(uint8_t *name, const char *myName)
{
	uint8_t nameLen = strlen((char *)name);

	for (int i = 0; i < nameLen/2; i++)
	{
		char c =
			((name[2*i]-'A') << 4) +
			((name[2*i+1]-'A') & 0x0F);
		if (c == ' ')
			return 1;
		if (c != myName[i])
			return 0;
	}
	return 1;
}
