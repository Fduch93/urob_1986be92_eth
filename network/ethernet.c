#include "ethernet.h"
#include <string.h>

#define ARP_HW_TYPE_ETH		htons(0x0001)
#define ARP_PROTO_TYPE_IP	htons(0x0800)

#define ARP_TYPE_REQUEST	htons(1)
#define ARP_TYPE_RESPONSE	htons(2)

typedef union
{
	uint8_t u8[28];
	struct {
		uint16_t hwType;
		uint16_t protoType;
		uint8_t hwAddrLen;
		uint8_t protoAddrLen;
		uint16_t type;
		uint8_t macAddrFrom[6];
		uint8_t ipAddrFrom[4];
		uint8_t macAddrTo[6];
		uint8_t ipAddrTo[4];
	};
} ETH_FakeArpResponse_T;

typedef struct {
	uint8_t unused[12];
	uint8_t fromAddr[4];
} ETH_IpHeaderDummy_T;

static const uint8_t broadcastMac[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

static void ETH_Build(
	uint8_t *headerPtr,
	const uint16_t type,
	const uint8_t toMac[6]);

static void ETH_MakeFakeArpResponse(
	ETH_FakeArpResponse_T *arpResponse,
	ETH_Header_T *header,
	uint8_t *ipPayload);

extern uint16_t ARP_Filter(uint8_t *data, uint16_t len, uint16_t responseLen)								__attribute__((weak, alias("ARP_FilterDefault")));
extern uint16_t IP_Filter(uint8_t *data, uint16_t len, uint16_t responseLen, uint8_t *isBroadcast)			__attribute__((weak, alias("IP_FilterDefault")));

extern uint16_t ARP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6])						__attribute__((weak, alias("ARP_ProcessDefault")));
extern uint16_t IP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6])						__attribute__((weak, alias("IP_ProcessDefault")));

static uint16_t ARP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseLen)						{ return 0; }
static uint16_t IP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseLen, uint8_t *isBroadcast)	{ return 0; }

static uint16_t ARP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6])				{ return 0; }
static uint16_t IP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6])					{ return 0; }

uint16_t ETH_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	if (data && len > sizeof(ETH_Header_T))
	{
		ETH_Header_T header = {0};
		memcpy(&header, data, sizeof(ETH_Header_T));

		uint8_t *payload = &data[sizeof(ETH_Header_T)];
		uint16_t payloadLen = len - sizeof(ETH_Header_T);

		uint16_t responseLen = 0;
		uint8_t isBroadcast = 0;

		switch (header.type)
		{
			case ETH_TYPE_ARP:
			{
				responseLen = ARP_Filter(payload, payloadLen, responseMaxLen - sizeof(ETH_Header_T));
				break;
			}
			case ETH_TYPE_IP:
			{
				ETH_FakeArpResponse_T arpResponse;
				ETH_MakeFakeArpResponse(&arpResponse, &header, payload);
				ARP_Filter(arpResponse.u8, sizeof(ETH_FakeArpResponse_T), 0);

				responseLen = IP_Filter(payload, payloadLen, responseMaxLen - sizeof(ETH_Header_T), &isBroadcast);
				break;
			}
			default:
				break;
		}

		if (responseLen > 0)
		{
			ETH_Build(
				data,
				header.type,
				isBroadcast ? broadcastMac : header.fromAddr);
			return sizeof(ETH_Header_T) + responseLen;
		}
	}

	return 0;
}

uint16_t ETH_Process(uint8_t *sendData, uint16_t sendMaxLen)
{
	uint8_t *payload = sendData + sizeof(ETH_Header_T);
	uint16_t payloadMaxLen = sendMaxLen - sizeof(ETH_Header_T);

	uint8_t toMac[6] = {0,0,0,0,0,0};

	uint16_t payloadLen = 0;
	uint16_t type = 0;

	if (!payloadLen)
	{
		payloadLen = ARP_Process(payload, payloadMaxLen, toMac);
		type = ETH_TYPE_ARP;
	}
	if (!payloadLen)
	{
		payloadLen = IP_Process(payload, payloadMaxLen, toMac);
		type = ETH_TYPE_IP;
	}

	if (payloadLen)
	{
		ETH_Build(sendData, type, toMac);
		return payloadLen + sizeof(ETH_Header_T);
	}

	return 0;
}

static void ETH_Build(
	uint8_t *headerPtr,
	const uint16_t type,
	const uint8_t toMac[6])
{
	ETH_Header_T header = {0};

	header.type = type;
	memcpy(header.fromAddr, ETH_GetMacExt(), 6);
	memcpy(header.toAddr, toMac, 6);

	memcpy(headerPtr, &header, sizeof(ETH_Header_T));
}

static void ETH_MakeFakeArpResponse(
	ETH_FakeArpResponse_T *arpResponse,
	ETH_Header_T *header,
	uint8_t *ipPayload)
{
	ETH_IpHeaderDummy_T *ipHeader =  (ETH_IpHeaderDummy_T *)ipPayload;

	arpResponse->hwType = ARP_HW_TYPE_ETH;
	arpResponse->protoType = ARP_PROTO_TYPE_IP;
	arpResponse->hwAddrLen = 6;
	arpResponse->protoAddrLen = 4;
	arpResponse->type = ARP_TYPE_RESPONSE;
	memset(arpResponse->ipAddrTo, 0xFF, 4);
	memcpy(arpResponse->macAddrTo, ETH_GetMacExt(), 6);
	memcpy(arpResponse->ipAddrFrom, ipHeader->fromAddr, 4);
	memcpy(arpResponse->macAddrFrom, header->fromAddr, 6);
}
