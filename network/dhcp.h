#ifndef DHCP_H
#define DHCP_H

#include <stdint.h>

#define DHCP_SERVER_PORT		htons(67)
#define DHCP_CLIENT_PORT		htons(68)

#define DHCP_OP_REQUEST			1
#define DHCP_OP_REPLY			2

#define DHCP_HW_ADDR_TYPE		DHCP_HW_ADDR_TYPE_ETH
#define DHCP_HW_ADDR_LEN		DHCP_HW_ADDR_LEN_ETH
#define DHCP_HW_ADDR_TYPE_ETH	1
#define DHCP_HW_ADDR_LEN_ETH	6

#define DHCP_FLAG_BROADCAST		htons(0x8000)

#define DHCP_MAGIC_COOKIE		htonl(0x63825363)

#define DHCP_CODE_PAD			0
#define DHCP_CODE_END			255
#define DHCP_CODE_SUBNETMASK	1
#define DHCP_CODE_GATEWAY		3
#define DHCP_CODE_HOSTNAME		12
#define DHCP_CODE_DOMAINNAME	15
#define DHCP_CODE_REQUESTEDADDR	50
#define DHCP_CODE_LEASETIME		51
#define DHCP_CODE_MESSAGETYPE	53
#define DHCP_CODE_DHCPSERVER	54
#define DHCP_CODE_RENEWTIME		58
#define DHCP_CODE_REBINDTIME	59

typedef struct {
	uint8_t code;
	uint8_t len;
} DHCP_OptionHeader_T;

typedef struct {
	DHCP_OptionHeader_T header;
	uint8_t data[];
} DHCP_Option_T;

typedef struct {
	uint8_t operation;
	uint8_t hwAddrType;
	uint8_t hwAddrLen;
	uint8_t unused1;
	uint32_t transactionId;
	uint16_t secondCount;
	uint16_t flags;
	uint32_t clientAddr;
	uint32_t offeredAddr;
	uint32_t serverAddr;
	uint32_t unused2;
	uint8_t hwAddr[16];
	uint8_t unused3[192];
	uint32_t magicCookie;
} DHCP_Header_T;

typedef struct {
	DHCP_Header_T header;
	DHCP_Option_T options[];
} DHCP_Message_T;

#define DHCP_MESSAGE_DISCOVER	1
#define DHCP_MESSAGE_OFFER		2
#define DHCP_MESSAGE_REQUEST	3
#define DHCP_MESSAGE_DECLINE	4
#define DHCP_MESSAGE_ACK		5
#define DHCP_MESSAGE_NAK		6
#define DHCP_MESSAGE_RELEASE	7
#define DHCP_MESSAGE_INFORM		8

typedef enum {
	DHCP_INIT,
	DHCP_ASSIGNED,
	DHCP_WAITING_OFFER,
	DHCP_WAITING_ACK
} DHCP_StatusCode_T;

uint16_t DHCP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);
uint16_t DHCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6]);

extern const uint8_t *DHCP_GetMacExt(void);
extern uint32_t DHCP_GetIpExt(void);
extern void DHCP_SetIpMaskGateway(uint32_t ipAddr, uint32_t mask, uint32_t gateway);

#endif
