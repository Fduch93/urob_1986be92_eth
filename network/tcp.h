#ifndef TCP_H
#define TCP_H

#include <stdint.h>

#define TCP_SYN_MSS			1460

#define TCP_FLAG_URG		0x20
#define TCP_FLAG_ACK		0x10
#define TCP_FLAG_PSH		0x08
#define TCP_FLAG_RST		0x04
#define TCP_FLAG_SYN		0x02
#define TCP_FLAG_FIN		0x01

typedef struct {
	uint16_t fromPort;
	uint16_t toPort;
	uint32_t seqNum;
	uint32_t ackNum;
	uint8_t dataOffset;
	uint8_t flags;
	uint16_t window;
	uint16_t cksum;
	uint16_t urgentPtr;
} TCP_Header_T;

typedef struct {
	uint8_t type;
	uint8_t len;
	uint16_t mss;
} TCP_MSS_T;

typedef struct {
	uint32_t fromAddr;
	uint32_t toAddr;
	uint8_t zero;
	uint8_t protocol;
	uint16_t len;
} TCP_ChecksumHeader_T;

uint16_t TCP_Filter(uint32_t fromAddr, uint8_t *data, uint16_t len, uint16_t responseMaxLen);
uint16_t TCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6]);

extern uint32_t TCP_GetIpExt(void);
extern const uint8_t *TCP_GetMacExt(void);
extern int TCP_IsPortListening(uint16_t port);

#endif
