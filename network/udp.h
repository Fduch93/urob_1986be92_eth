#ifndef UDP_H
#define UDP_H

#include <stdint.h>

#define UDP_DHCP_SERVER_PORT		htons(67)
#define UDP_DHCP_CLIENT_PORT		htons(68)
#define UDP_NBNS_PORT				htons(137)
#define UDP_LLMNR_PORT				htons(5355)

typedef struct {
	uint16_t fromPort;
	uint16_t toPort;
	uint16_t len;
	uint16_t cksum;
} UDP_Header_T;

uint16_t UDP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast);
uint16_t UDP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6]);

#endif
