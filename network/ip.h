#ifndef IP_H
#define IP_H

#include "beconv.h"
#include <stdint.h>

#define IP_PROTOCOL_ICMP	1
#define IP_PROTOCOL_TCP		6
#define IP_PROTOCOL_UDP		17

typedef struct {
	uint8_t verHeadLen;
	uint8_t tos;
	uint16_t totalLen;

	uint16_t fragmentId;
	uint16_t flagsFramgentOffset;

	uint8_t ttl;
	uint8_t protocol;
	uint16_t cksum;

	uint32_t fromAddr;
	uint32_t toAddr;
} IP_Header_T;

uint32_t IP_GetAddr(void);
uint32_t IP_GetMask(void);
uint32_t IP_GetGateway(void);
void IP_SetAddr(uint32_t ip);
void IP_SetMask(uint32_t mask);
void IP_SetGateway(uint32_t gateway);

uint16_t IP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen, uint8_t *isBroadcast);
uint16_t IP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint8_t toMac[6]);

#endif
