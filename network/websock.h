#ifndef WEBSOCK_H
#define WEBSOCK_H

#include <stdint.h>

uint16_t WEBSOCK_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);
void WEBSOCK_MakeResponseKey(char *key, uint8_t keyLen, char base64[29]);

#endif
