#ifndef ETHERNET_H
#define ETHERNET_H

#include "beconv.h"
#include <stdint.h>

#define ETH_TYPE_ARP	htons(0x0806)
#define ETH_TYPE_IP		htons(0x0800)

typedef struct  {
	uint8_t toAddr[6];
	uint8_t fromAddr[6];
	uint16_t type;
} ETH_Header_T;

void ETH_SetMac(const uint8_t mac[6]);

uint16_t ETH_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);
uint16_t ETH_Process(uint8_t *sendData, uint16_t sendMaxLen);

extern const uint8_t *ETH_GetMacExt(void);

#endif
