#include "http.h"
#include "TICK.h"
#include <string.h>
#include "whebbpakk.h"

static void HTTP_ParseRequest(char *data, uint16_t len, HTTP_Request_T *request);

uint16_t HTTP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	HTTP_Request_T request;
	HTTP_ParseRequest((char *)data, len, &request);

	uint16_t responseLen = 0;

	if (!strncmp(request.method, "GET", 3))
	{
		responseLen = HTTP_HandleGet(&request, data, responseMaxLen);
	}
	else if (!strncmp(request.method, "POST", 4))
	{
		responseLen = HTTP_HandlePost(&request, data, responseMaxLen);
	}

	return responseLen;
}

char HTTP_CompareUrl(const char *path, uint16_t pathLen, const char *url)
{
	return !strncmp(path, url, pathLen) && pathLen == strlen(url);
}

uint16_t HTTP_Build(
	uint8_t *response,
	const char *status,
	const char *contentType,
	const char *body)
{
	char *resp = (char *)response;
	*resp = 0;

	strcat(resp, status);
	strcat(resp, "\r\nServer: MDR32");

	if (contentType)
	{
		strcat(resp, "\r\nContent-Type: ");
		strcat(resp, contentType);
	}

	if (body)
	{
		strcat(resp, "\r\nContent-Length: ");
		num2dec(resp, strlen(body));
		strcat(resp, "\r\n\r\n");
		strcat(resp, body);
	}
	else
	{
		strcat(resp, "\r\n\r\n");
	}

	return strlen(resp);
}

static void HTTP_ParseRequest(char *data, uint16_t len, HTTP_Request_T *request)
{
	char *method = data;
	char *url = strchr(data, ' ') + 1;
	char *httpVer = strchr(url, ' ') + 1;
	char *eol = strstr(data, "\r\n");
	eol = eol ? eol : (data + len);

	request->method = method;
	request->url = url;
	request->httpVer = httpVer;

	if (HTTP_CompareUrl(request->url, request->urlLen, "/log") &&
		HTTP_CompareUrl(request->method, 4, "POST"))
		*data = *data;

	request->methodLen = url - method - 1;
	request->urlLen = httpVer - url - 1;
	request->httpVerLen = eol - httpVer;

	char *headers = 0;
	char *body = 0;
	int headersLen = 0;
	int bodyLen = 0;

	if (eol + 2 < data + len)
	{
		headers = eol + 2;
		char *header_end = strstr(headers, "\r\n");
		while (header_end &&
			header_end + 2 < len + data &&
			strncmp(header_end + 2, "\r\n", 2))
		{
			// TODO: check if last header isn't lost
			headersLen = header_end - headers;
			header_end = strstr(header_end + 2, "\r\n");
		}

		if (header_end &&
			header_end + 4 < len + data)
		{
			body = header_end + 4;
			bodyLen = len + data - body;
		}
	}

	request->headers = headers;
	request->body = body;
	request->headersLen = headersLen;
	request->bodyLen = bodyLen;
}
