#ifndef ICMP_H
#define ICMP_H

#include <stdint.h>

#define ICMP_TYPE_ECHO_RQ	8
#define ICMP_TYPE_ECHO_RPLY	0

typedef struct {
	uint8_t type;
	uint8_t code;
	uint16_t cksum;
	uint16_t id;
	uint16_t seq;
} ICMP_Header_T;

typedef struct {
	ICMP_Header_T header;
	uint8_t data[];
} ICMP_Packet_T;

uint16_t ICMP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);

#endif
