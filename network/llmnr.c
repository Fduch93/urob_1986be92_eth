#include "llmnr.h"
#include "beconv.h"
#include <string.h>

typedef struct
{
	NBNS_Header_T header;
	uint8_t nameLen;
	char name[16];
	uint16_t queryType;
	uint16_t queryClass;
} LLMNR_Query_T;

typedef struct
{
	NBNS_Header_T header;
	uint8_t nameLen;
	char name[16];
	uint16_t queryType;
	uint16_t queryClass;
	uint32_t ttl;
	uint16_t rdlen;
	uint32_t ip;
} LLMNR_Response_T;

static void LLMNR_ParseQuery(LLMNR_Query_T *query, const uint8_t *data, uint16_t len);
static uint16_t LLMNR_BuildResponse(LLMNR_Query_T *query, uint8_t *buf, uint16_t responseMaxLen);

uint16_t LLMNR_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	LLMNR_Query_T query = {0};
	LLMNR_ParseQuery(&query, data, len);

	if ((!strcmp(query.name,"MDR32") ||
		 !strcmp(query.name,"mdr32")) &&
		query.nameLen == 5 &&
		query.queryType == 0x01 &&
		query.queryClass == 0x01)
	{
		return LLMNR_BuildResponse(&query, data, responseMaxLen);
	}

	return 0;
}

static void LLMNR_ParseQuery(LLMNR_Query_T *query, const uint8_t *data, uint16_t len)
{
	const uint8_t *pReq = data;

	memcpy(query->header.u16, pReq, sizeof(NBNS_Header_T));
	for (uint32_t i = 0; i < sizeof(NBNS_Header_T)/2; i++)
		query->header.u16[i] = ntohs(query->header.u16[i]);
	pReq += sizeof(NBNS_Header_T);

	query->nameLen = *pReq;
	pReq++;
	memcpy(&query->name[0], pReq, query->nameLen+1);
	pReq+=query->nameLen+1;

	memcpy(&query->queryType, pReq, 2); pReq += 2;
	memcpy(&query->queryClass, pReq, 2); pReq += 2;
	query->queryType = ntohs(query->queryType);
	query->queryClass = ntohs(query->queryClass);
}

static uint16_t LLMNR_BuildResponse(LLMNR_Query_T *query, uint8_t *buf, uint16_t responseMaxLen)
{
	LLMNR_Response_T resp = {0};

	memcpy(&resp, query, sizeof(LLMNR_Query_T));
	resp.header.msgType = MSG_REPLY;
	resp.header.authAnswer = 0;
	resp.header.questionCnt = 0;
	resp.header.answerCnt = 1;
	resp.ttl = 0x1E000000;
	resp.rdlen = 4;
	resp.ip = NBNS_GetIpExt();

	uint8_t *pBuf = buf;

	for (uint32_t i = 0; i < sizeof(NBNS_Header_T)/2; i++)
		resp.header.u16[i] = ntohs(resp.header.u16[i]);
	memcpy(pBuf, resp.header.u16, sizeof(NBNS_Header_T));
	pBuf += sizeof(NBNS_Header_T);

	*pBuf = query->nameLen;
	pBuf++;

	memcpy(pBuf, query->name, query->nameLen+1);
	pBuf += query->nameLen+1;

	resp.queryType = ntohs(resp.queryType);
	resp.queryClass = ntohs(resp.queryClass);
	memcpy(pBuf, &resp.queryType, 2);
	pBuf += 2;
	memcpy(pBuf, &resp.queryClass, 2);
	pBuf += 2;

	memcpy(pBuf, &resp.ttl, 4);
	pBuf += 4;

	resp.rdlen = ntohs(resp.rdlen);
	memcpy(pBuf, &resp.rdlen, 2);
	pBuf += 2;

	memcpy(pBuf, &resp.ip, 4);
	pBuf += 4;

	return pBuf - buf;
}

