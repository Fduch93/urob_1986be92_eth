#ifndef CHECKSUM_H
#define CHECKSUM_H

#include <stdint.h>

uint32_t checksumPart(uint32_t sum, const uint8_t *buf, uint16_t len);
uint16_t checksum(uint32_t sum, const uint8_t *buf, uint16_t len);

#endif
