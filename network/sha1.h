#ifndef SHA1_H
#define SHA1_H

// https://git.kernel.org/pub/scm/git/git.git/tree/block-sha1/sha1.c?id=66c9c6c0fbba0894ebce3da572f62eb05162e547

typedef struct {
	unsigned int H[5];
	unsigned int W[16];
	unsigned long long size;
} blk_SHA_CTX;

void blk_SHA1_Init(blk_SHA_CTX *ctx);
void blk_SHA1_Update(blk_SHA_CTX *ctx, const void *dataIn, unsigned long len);
void blk_SHA1_Final(unsigned char hashout[20], blk_SHA_CTX *ctx);

#endif
