#include "arp.h"
#include <string.h>

static ARP_CacheEntry_T arpCache[ARP_CACHE_SIZE];
static int arpCachePos = 0;
static int arpCacheCnt = 0;

static const uint8_t broadcastMac[6] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

const uint8_t *ARP_GetBroadcastMac() { return broadcastMac; }

static void ARP_AddEntry(const uint8_t *macAddr, const uint8_t *ipAddr);
static uint8_t *ARP_SearchCache(const uint8_t *ipAddr);

uint16_t ARP_Filter(uint8_t *data, uint16_t len, uint16_t responseLen)
{
	ARP_Message_T arpMsg = {0};
	memcpy(&arpMsg, data, sizeof(ARP_Message_T));

	if (len >= sizeof(ARP_Message_T))
	{
		uint32_t selfIpAddr = ARP_GetIpExt();

		if ((arpMsg.hwType == ARP_HW_TYPE_ETH) &&
			(arpMsg.protoType == ARP_PROTO_TYPE_IP))
		{
			switch (arpMsg.type)
			{
				case ARP_TYPE_REQUEST:
				{
					ARP_Message_T arpResponse = {0};
					memcpy(&arpResponse, &arpMsg, sizeof(ARP_Message_T));
					arpResponse.type = ARP_TYPE_RESPONSE;
					memcpy(arpResponse.ipAddrTo, arpMsg.ipAddrFrom, 4);
					memcpy(arpResponse.ipAddrFrom, &selfIpAddr, 4);
					memcpy(arpResponse.macAddrTo, arpMsg.macAddrFrom, 6);
					memcpy(arpResponse.macAddrFrom, ARP_GetMacExt(), 6);

					memcpy(data, &arpResponse, sizeof(ARP_Message_T));

					return sizeof(ARP_Message_T);
				}
				case ARP_TYPE_RESPONSE:
				{
					ARP_AddEntry(arpMsg.macAddrFrom, arpMsg.ipAddrFrom);
					break;
				}
			}
		}
	}

	return 0;
}

const uint8_t *ARP_Resolve(const uint8_t *ipAddr)
{
	ARP_Message_T arpMsg;

	// search arp cache
	uint8_t *macCached = ARP_SearchCache(ipAddr);
	if (macCached)
		return macCached;

	uint32_t selfIpAddr = ARP_GetIpExt();

	// send request
	arpMsg.hwType = ARP_HW_TYPE_ETH;
	arpMsg.protoType = ARP_PROTO_TYPE_IP;
	arpMsg.hwAddrLen = 6;
	arpMsg.protoAddrLen = 4;
	arpMsg.type = ARP_TYPE_REQUEST;
	memcpy(arpMsg.ipAddrFrom, &selfIpAddr, 4);
	memcpy(arpMsg.ipAddrTo, ipAddr, 4);
	memcpy(arpMsg.macAddrFrom, ARP_GetMacExt(), 6);
	memset(arpMsg.macAddrTo, 0xFF, 6);

	//ARP_SendExt((uint8_t *)&arpMsg, sizeof(ARP_Message_T), broadcastMac);
	return 0;
}

static void ARP_AddEntry(const uint8_t *macAddr, const uint8_t *ipAddr)
{
	if (!ARP_SearchCache(ipAddr))
	{
		memcpy(arpCache[arpCachePos].macAddr, macAddr, 6);
		memcpy(arpCache[arpCachePos].ipAddr, ipAddr, 4);
		arpCachePos++;
		if (arpCachePos == ARP_CACHE_SIZE)
			arpCachePos = 0;
		if (arpCacheCnt < ARP_CACHE_SIZE)
			arpCacheCnt++;
	}
}

static uint8_t *ARP_SearchCache(const uint8_t *ipAddr)
{
	for (int i = 0; i < arpCacheCnt; i++)
	{
		if (!memcmp(ipAddr, arpCache[i].ipAddr, 4))
			return arpCache[i].macAddr;
	}
	return 0;
}
