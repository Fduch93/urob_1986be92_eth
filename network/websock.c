#include "websock.h"
#include "sha1.h"
#include "TICK.h"
#include "LOG.h"
#include <string.h>

void WEBSOCK_MakeResponseKey(char *key, uint8_t keyLen, char base64[29])
{
	static const char *uuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

	struct {
		uint32_t u32[16];
		uint8_t u8[64];
	} src;
	memcpy(src.u8, key, keyLen);
	memcpy(src.u8+keyLen, uuid, strlen(uuid));
	uint8_t len = keyLen + strlen(uuid);

	blk_SHA_CTX blk;
//	memcpy(blk.W, key, keyLen);
//	memcpy(blk.W+keyLen, uuid, strlen(uuid));
//	memset(blk.W+keyLen+strlen(uuid), 0, sizeof(blk.W)-keyLen-strlen(uuid));

	uint8_t hashout[21];
	blk_SHA1_Init(&blk);
	blk_SHA1_Update(&blk, src.u8, len);
	blk_SHA1_Final(hashout, &blk);
	hashout[20] = 0;

	static const char base[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	//uint8_t base64[29]; // = 20*4/3 + 1 padding + 1 eol '\0'
	for (uint8_t i = 0; i < 7; i++)
	{
		base64[4*i+0] = base[(hashout[3*i+0] >> 2)];
		base64[4*i+1] = base[((hashout[3*i+0] << 4) & 0x3F) | (hashout[3*i+1] >> 4)];
		base64[4*i+2] = base[((hashout[3*i+1] << 2) & 0x3F) | (hashout[3*i+2] >> 6)];
		base64[4*i+3] = base[(hashout[3*i+2]) & 0x3F];
	}
	base64[27] = '=';
	base64[28] = 0;
}

#define WS_OPCODE_CONT	0
#define WS_OPCODE_TXT	1
#define WS_OPCODE_BIN	2
#define WS_OPCODE_CLOSE	8
#define WS_OPCODE_PING	9
#define WS_OPCODE_PONG	10

typedef struct
{
	struct {
		uint8_t opCode:4;
		uint8_t reserved:3;
		uint8_t fin:1;

		uint8_t payloadLen:7;
		uint8_t isMasked:1;
	};
} WEBSOCK_Header_T;

extern void num2dec(char *out, uint32_t num) __attribute__((weak, alias("WEBSOCK_Num2DecDefault")));

uint16_t WEBSOCK_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	WEBSOCK_Header_T msg = {0};
	memcpy(&msg, data, sizeof(WEBSOCK_Header_T));

	uint8_t *payload = data + sizeof(WEBSOCK_Header_T);
	uint16_t payloadLen = len - sizeof(WEBSOCK_Header_T);

	if (msg.payloadLen == 126)
	{
		memcpy(&payloadLen, payload, 2);
		payload += 2;
	}
	else if (msg.payloadLen == 12)
	{
		payloadLen = 0; // no support for that large frame
		payload += 8;
	}

	uint8_t mask[4] = {0};
	if (msg.isMasked)
	{
		memcpy(mask, payload, 4);
		payloadLen -= 4;
		payload += 4;

		for (uint16_t i = 0; i < payloadLen; i++)
		{
			payload[i] = payload[i] ^ mask[i%4]; // (i&3) is equivalent to (i%4)
		}
	}

	switch (msg.opCode)
	{
		case WS_OPCODE_PING:
			msg.opCode = WS_OPCODE_PONG;
			msg.isMasked = 0;
			msg.payloadLen -= 4; // TODO: implement variable length pong response

			memcpy(data, &msg, sizeof(WEBSOCK_Header_T));
			memmove(data + sizeof(WEBSOCK_Header_T), payload, payloadLen);

			return sizeof(WEBSOCK_Header_T) + payloadLen;

		case WS_OPCODE_TXT:
		{
			if (!strncmp((char *)payload,"log",strlen("log")))
			{
				uint32_t logTotalRead = 0;
				uint32_t mul = 1;
				for (int i = payloadLen-1; i >= 3; i--)
				{
					logTotalRead += (payload[i] - '0') * mul;
					mul *= 10;
				}
				uint32_t logTotalCount = LOG_GetTotalCount();

				if (logTotalRead + LOG_SIZE < logTotalCount)
					logTotalRead = logTotalCount - LOG_SIZE;

				if (logTotalRead < logTotalCount)
				{
					uint16_t logLen = LOG_Get(logTotalCount - logTotalRead, 0, 0);

					WEBSOCK_Header_T msg = {
						.opCode = WS_OPCODE_TXT,
						.reserved = 0,
						.fin = 1,
						.payloadLen = strlen("log") + logLen,
						.isMasked = 0,
					};


					memcpy(data, &msg, sizeof(WEBSOCK_Header_T));
					char *logMsg = (char *)data + sizeof(WEBSOCK_Header_T);
					*logMsg = 0;
					strcat(logMsg, "log");
					LOG_Get(
						logTotalCount - logTotalRead,
						logMsg + strlen("log"),
						logLen);

					return sizeof(WEBSOCK_Header_T) + msg.payloadLen;
				}
				else
					return 0;

			}
		}
		case WS_OPCODE_BIN:
		case WS_OPCODE_PONG:
		case WS_OPCODE_CONT:
		case WS_OPCODE_CLOSE:
		default:
			break;
	}

	return 0;
}

uint16_t WEBSOCK_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])
{
	static uint32_t sec = 0;

	if (SECONDS > sec)
	{
		sec = SECONDS;

		char str[11] = {0};
		num2dec(str, sec);

		WEBSOCK_Header_T msg = {
			.opCode = WS_OPCODE_TXT,
			.reserved = 0,
			.fin = 1,
			.payloadLen = strlen("sec") + strlen(str),
			.isMasked = 0,
		};

		memcpy(sendData, &msg, sizeof(WEBSOCK_Header_T));
		char *payload = (char *)sendData + sizeof(WEBSOCK_Header_T);
		*payload = 0;
		strcat(payload, "sec");
		strcat(payload, str);

		return sizeof(WEBSOCK_Header_T) + msg.payloadLen;
	}

	return 0;
}

static void WEBSOCK_Num2DecDefault(char *out, uint32_t num)
{
	char str[11] = "0000000000\0";

	if (num == 0)
	{
		strcat(out, &str[9]);
	}
	else
	{
		char *c = &str[10];

		for (uint32_t div = 1; c > str; div *= 10)
		{
			uint32_t quantient = (num / div);
			if (quantient > 0 || div == 1)
			{
				*(--c) = '0' + quantient % 10;
			}
			else break;
		}

		strcat(out, c);
	}
}
