#include "beconv.h"
#include <stdint.h>

uint32_t checksumPart(uint32_t sum, const uint8_t *buf, uint16_t len)
{
	while(len >= 2)
	{
		sum += (((uint16_t)*buf) << 8) | *(buf+1);
		buf += 2;
		len -= 2;
	}

	if(len)
		sum += (uint16_t)*buf << 8;

	return sum;
}

uint16_t checksum(uint32_t sum, const uint8_t *buf, uint16_t len)
{
	sum = checksumPart(sum, buf, len);

	while(sum >> 16)
		sum = (sum & 0xffff) + (sum >> 16);

	return ~htons((uint16_t)sum);
}
