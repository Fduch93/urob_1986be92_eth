#ifndef ARP_H
#define ARP_H

#include "beconv.h"
#include <stdint.h>

#define ARP_CACHE_SIZE		10

#define ARP_HW_TYPE_ETH		htons(0x0001)
#define ARP_PROTO_TYPE_IP	htons(0x0800)

#define ARP_TYPE_REQUEST	htons(1)
#define ARP_TYPE_RESPONSE	htons(2)

typedef struct
{
	uint16_t hwType;
	uint16_t protoType;
	uint8_t hwAddrLen;
	uint8_t protoAddrLen;
	uint16_t type;
	uint8_t macAddrFrom[6];
	uint8_t ipAddrFrom[4];
	uint8_t macAddrTo[6];
	uint8_t ipAddrTo[4];
} ARP_Message_T;

typedef struct {
	uint8_t ipAddr[4];
	uint8_t macAddr[6];
} ARP_CacheEntry_T;

uint16_t ARP_Filter(uint8_t *data, uint16_t len, uint16_t responseLen);
const uint8_t *ARP_Resolve(const uint8_t *ipAddr);

extern const uint8_t *ARP_GetBroadcastMac(void);
extern void ARP_SendExt(const uint8_t *data, uint16_t len, const uint8_t *macAddr);
extern const uint8_t *ARP_GetMacExt(void);
extern uint32_t ARP_GetIpExt(void);

#endif
