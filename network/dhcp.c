#include "dhcp.h"
#include "beconv.h"
#include "TICK.h"
#include <string.h>

static uint8_t hostName[5] = "mdr32";
static uint16_t hostNameLen = sizeof(hostName);

static DHCP_StatusCode_T dhcpStatus;
static uint32_t dhcpServer = 0;
static uint32_t dhcpRenewTime = 0;
static uint32_t dhcpRetryTime = 0;
static uint32_t dhcpTransactionId;

static uint32_t offeredNetMask = 0;
static uint32_t offeredGateway = 0;
static uint32_t leaseTime = 0;
static uint32_t renewTime = 0;
static uint32_t renewServer = 0;

static uint16_t DHCP_HandleOffer(DHCP_Header_T *header, uint8_t *response, uint16_t responseMaxLen);
static uint16_t DHCP_HandleAck(uint32_t offeredAddr);

static uint16_t DHCP_Discover(uint8_t *sendData, uint16_t sendMaxLen);
static uint16_t DHCP_Renew(uint8_t *sendData, uint16_t sendMaxLen);

static uint32_t DHCP_NewTransactionId(void);

static void DHCP_Build(
	uint8_t *pHeader,
	const uint8_t operation,
	const uint32_t transactionId,
	const uint16_t flags);

static DHCP_Option_T *AddOption(DHCP_Option_T *optPtr, uint8_t optCode, uint16_t len, uint8_t *value);
static DHCP_Option_T *AddOption8(DHCP_Option_T *optPtr, uint8_t optCode, uint8_t value);
static DHCP_Option_T *AddOption16(DHCP_Option_T *optPtr, uint8_t optCode, uint16_t value);
static DHCP_Option_T *AddOption32(DHCP_Option_T *optPtr, uint8_t optCode, uint32_t value);
static DHCP_Option_T *AddOptionEnd(DHCP_Option_T *optPtr);
static uint8_t ParseOptions(const uint8_t *op, uint16_t len);

uint16_t DHCP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	DHCP_Header_T header = {0};
	memcpy(&header, data, sizeof(DHCP_Header_T));
	const uint8_t *options = &data[sizeof(DHCP_Header_T)];

	// Check if DHCP messages directed to us
	if ((len >= sizeof(DHCP_Header_T)) &&
		(header.operation == DHCP_OP_REPLY) &&
		(header.transactionId == dhcpTransactionId) &&
		(header.magicCookie == DHCP_MAGIC_COOKIE))
	{
		len -= sizeof(DHCP_Header_T);

		// parse DHCP message
		uint8_t type = ParseOptions(options, len);

		switch (type)
		{
			case DHCP_MESSAGE_OFFER:
				return DHCP_HandleOffer(&header, data, responseMaxLen);
			case DHCP_MESSAGE_ACK:
				return DHCP_HandleAck(header.offeredAddr);
		}
	}

	return 0;
}

uint16_t DHCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint16_t *fromPort, uint16_t *toPort, uint32_t *toIp, uint8_t toMac[6])
{
	if (dhcpStatus == DHCP_ASSIGNED && SECONDS < dhcpRenewTime)
		return 0;
	else if (dhcpStatus != DHCP_ASSIGNED && SECONDS < dhcpRetryTime)
		return 0;
	else
	{
		*fromPort = DHCP_CLIENT_PORT;
		*toPort = DHCP_SERVER_PORT;
		*toIp = 0xFFFFFFFF;
		memset(toMac, 0xFF, 6);

		if (dhcpStatus != DHCP_ASSIGNED)
			return DHCP_Discover(sendData, sendMaxLen);
		else
			return DHCP_Renew(sendData, sendMaxLen);
	}
}

static uint16_t DHCP_HandleOffer(DHCP_Header_T *header,  uint8_t *response, uint16_t responseMaxLen)
{
	if ((dhcpStatus == DHCP_WAITING_OFFER) &&
		(header->offeredAddr != 0))
	{
		dhcpStatus = DHCP_WAITING_ACK;

		if (!header->serverAddr)
			header->serverAddr = renewServer;
		if (!renewServer)
			renewServer = header->serverAddr;

		DHCP_Build(
			response,
			DHCP_OP_REQUEST,
			header->transactionId,
			DHCP_FLAG_BROADCAST);

		DHCP_Option_T *options = (DHCP_Option_T *)(&response[sizeof(DHCP_Header_T)]);

		options = AddOption8 (options, DHCP_CODE_MESSAGETYPE, DHCP_MESSAGE_REQUEST);
		options = AddOption  (options, DHCP_CODE_HOSTNAME, hostNameLen, hostName);
		options = AddOption32(options, DHCP_CODE_REQUESTEDADDR, header->offeredAddr);
		options = AddOption32(options, DHCP_CODE_DHCPSERVER, header->serverAddr);
		options = AddOptionEnd(options);

		return (uint8_t *)options - (uint8_t *)(response);
	}

	return 0;
}

static uint16_t DHCP_HandleAck(uint32_t offeredAddr)
{
	if (dhcpStatus == DHCP_WAITING_ACK && leaseTime > 0)
	{
		dhcpStatus = DHCP_ASSIGNED;
		dhcpServer = renewServer;

		if(!renewTime)
			renewTime = leaseTime/2;
		dhcpRenewTime = SECONDS + renewTime;
		dhcpRetryTime = SECONDS + leaseTime;

		// network up
		DHCP_SetIpMaskGateway(
			offeredAddr,
			offeredNetMask,
			offeredGateway);
	}

	return 0;
}

static uint16_t DHCP_Discover(uint8_t *sendData, uint16_t sendMaxLen)
{
	dhcpStatus = DHCP_WAITING_OFFER;
	dhcpRetryTime = SECONDS + 1;

	DHCP_Build(
		sendData,
		DHCP_OP_REQUEST,
		DHCP_NewTransactionId(),
		DHCP_FLAG_BROADCAST);

	DHCP_Option_T *options = (DHCP_Option_T *)(sendData + sizeof(DHCP_Header_T));

	options = AddOption8(options, DHCP_CODE_MESSAGETYPE, DHCP_MESSAGE_DISCOVER);
	options = AddOption(options, DHCP_CODE_HOSTNAME, hostNameLen, hostName);
	options = AddOptionEnd(options);

	return (uint8_t *)options - sendData;
}

static uint16_t DHCP_Renew(uint8_t *sendData, uint16_t sendMaxLen)
{
	dhcpStatus = DHCP_WAITING_ACK;

	DHCP_Build(
		sendData,
		DHCP_OP_REQUEST,
		DHCP_NewTransactionId(),
		DHCP_FLAG_BROADCAST);

	DHCP_Option_T *options = (DHCP_Option_T *)(sendData + sizeof(DHCP_Header_T));

	options = AddOption8 (options, DHCP_CODE_MESSAGETYPE, DHCP_MESSAGE_REQUEST);
	options = AddOption  (options, DHCP_CODE_HOSTNAME, hostNameLen, hostName);
	options = AddOption32(options, DHCP_CODE_REQUESTEDADDR, DHCP_GetIpExt());
	options = AddOption32(options, DHCP_CODE_DHCPSERVER, dhcpServer);
	options = AddOptionEnd(options);

	return (uint8_t *)options - sendData;
}

static uint32_t DHCP_NewTransactionId()
{
	uint16_t now = TICK; // smth more random?
	dhcpTransactionId = now  | (now << 16);
	return dhcpTransactionId;
}

static void DHCP_Build(
	uint8_t *pHeader,
	const uint8_t operation,
	const uint32_t transactionId,
	const uint16_t flags)
{
	DHCP_Header_T header = {0};

	header.operation = operation;
	header.hwAddrType = DHCP_HW_ADDR_TYPE;
	header.hwAddrLen = DHCP_HW_ADDR_LEN;
	header.transactionId = transactionId;
	header.secondCount = 0;
	header.flags = flags;
	header.clientAddr = (dhcpStatus == DHCP_ASSIGNED) ? DHCP_GetIpExt() : 0;
	header.offeredAddr = 0;
	header.serverAddr = 0;
	memcpy(header.hwAddr, DHCP_GetMacExt(), DHCP_HW_ADDR_LEN);
	header.magicCookie = DHCP_MAGIC_COOKIE;

	memcpy(pHeader, &header, sizeof(DHCP_Header_T));
}

static DHCP_Option_T *AddOption(DHCP_Option_T *optPtr, uint8_t optCode, uint16_t len, uint8_t *value)
{
	uint8_t *ptr = (uint8_t *)optPtr;
	optPtr->header.code = optCode;

	if (optCode == DHCP_CODE_PAD)
	{
		ptr++;
		return (DHCP_Option_T *)ptr;
	}

	optPtr->header.len = len;
	memcpy(optPtr->data, value, len);
	ptr += sizeof(DHCP_OptionHeader_T) + len;
	optPtr = (DHCP_Option_T *)ptr;
	if (len & 1) {
		optPtr = AddOption(optPtr, DHCP_CODE_PAD, 1, 0);
	}
	return optPtr;
}

static DHCP_Option_T *AddOption8(DHCP_Option_T *optPtr, uint8_t optCode, uint8_t value)
{
	return AddOption(optPtr, optCode, 1, &value);
}

__attribute__((unused))
static DHCP_Option_T *AddOption16(DHCP_Option_T *optPtr, uint8_t optCode, uint16_t value)
{
	return AddOption(optPtr, optCode, sizeof(uint16_t), (uint8_t *)&value);
}

static DHCP_Option_T *AddOption32(DHCP_Option_T *optPtr, uint8_t optCode, uint32_t value)
{
	return AddOption(optPtr, optCode, sizeof(uint32_t), (uint8_t *)&value);
}

static DHCP_Option_T *AddOptionEnd(DHCP_Option_T *optPtr)
{
	uint8_t *ptr = (uint8_t *)optPtr;
	*ptr = DHCP_CODE_END;
	ptr++;
	return (DHCP_Option_T *)ptr;
}

static uint8_t ParseOptions(const uint8_t *op, uint16_t len)
{
	uint8_t type = 0;

	offeredNetMask = 0;
	offeredGateway = 0;
	leaseTime = 0;
	renewTime = 0;
	renewServer = 0;

	uint8_t optlen;
	while (len >= sizeof(DHCP_OptionHeader_T))
	{
		DHCP_Option_T *option = (DHCP_Option_T *)op;
		switch (option->header.code)
		{
			case DHCP_CODE_PAD:
				op++;
				len--;
				continue; // PAD option is not followed by length byte
			case DHCP_CODE_END:
				len = 0;
				continue; // exit loop
			case DHCP_CODE_MESSAGETYPE:
				type = *(option->data);
				break;
			case DHCP_CODE_SUBNETMASK:
				memcpy(&offeredNetMask, option->data, sizeof(offeredNetMask));
				break;
			case DHCP_CODE_GATEWAY:
				memcpy(&offeredGateway, option->data, sizeof(offeredGateway));
				break;
			case DHCP_CODE_DHCPSERVER:
				memcpy(&renewServer, option->data, sizeof(renewServer));
				break;
			case DHCP_CODE_LEASETIME:
				memcpy(&leaseTime, option->data, sizeof(leaseTime));
				leaseTime = htonl(leaseTime);
				if (leaseTime > 65000)
					leaseTime = 65000;
				break;
			case DHCP_CODE_RENEWTIME:
				memcpy(&renewTime, option->data, sizeof(renewTime));
				renewTime = htonl(renewTime);
				if (renewTime > 64000)
					renewTime = 64000;
				break;
		}

		optlen = sizeof(DHCP_OptionHeader_T) + option->header.len;
		op += optlen;
		len -= optlen;
	}

	return type;
}
