#ifndef HTTP_H
#define HTTP_H

#include <stdint.h>

#define HTTP_BUFFER_SIZE 1024

typedef struct {
	char *method;
	char *url;
	char *httpVer;
	char *headers;
	char *body;
	int methodLen;
	int urlLen;
	int httpVerLen;
	int headersLen;
	int bodyLen;
} HTTP_Request_T;

uint16_t HTTP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen);

char HTTP_CompareUrl(const char *path, uint16_t pathLen, const char *url);
void HTTP_Num2Str(char *out, uint32_t num);
uint16_t HTTP_Build(
	uint8_t *response,
	const char *status,
	const char *contentType,
	const char *body);

uint16_t HTTP_HandlePost(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen);
uint16_t HTTP_HandleGet(HTTP_Request_T *request, uint8_t *response, uint16_t responseMaxLen);

extern void num2dec(char *out, uint32_t num) __attribute__((weak, alias("HTTP_Num2DecDefault")));

#include <string.h>
static void HTTP_Num2DecDefault(char *out, uint32_t num)
{
	char str[11] = "0000000000\0";

	if (num == 0)
	{
		strcat(out, &str[9]);
	}
	else
	{
		char *c = &str[10];

		for (uint32_t div = 1; c > str; div *= 10)
		{
			uint32_t quantient = (num / div);
			if (quantient > 0 || div == 1)
			{
				*(--c) = '0' + quantient % 10;
			}
			else break;
		}

		strcat(out, c);
	}
}

#endif
