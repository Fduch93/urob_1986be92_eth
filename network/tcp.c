#include "tcp.h"
#include "tcp_connections.h"
#include "checksum.h"
#include "beconv.h"
#include "TICK.h"
#include "log.h"
#include <string.h>

static const TCP_MSS_T mss = {
	.type = 2,
	.len = 4,
	.mss = htons(TCP_SYN_MSS)
};

static uint16_t TCP_HandleNoConnection(
	uint32_t fromAddr,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_HandleConnection(
	TCP_Connection_T *connection,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_HandleSynRecieved(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_HandleEstablished(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_BuildRstResponseImplicit(
	uint32_t fromAddr,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_BuildRstResponse(TCP_Connection_T *connection, uint8_t *resp, uint16_t responseMaxLen);
static uint16_t TCP_BuildSynAckResponse(TCP_Connection_T *connection, uint8_t *resp, uint16_t responseMaxLen);
static uint16_t TCP_BuildFinAckResponse(TCP_Connection_T *connection, uint8_t *resp, uint16_t responseMaxLen);

static uint16_t TCP_BuildResponse(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *options, uint16_t optionsLen,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen);

static uint32_t TCP_PseudoHeaderChecksum(uint32_t remoteAddr, uint16_t packetLen);

extern uint16_t HTTP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)									__attribute__((weak, alias("HTTP_FilterDefault")));
extern uint16_t WEBSOCK_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)								__attribute__((weak, alias("WEBSOCK_FilterDefault")));

extern uint16_t HTTP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])				__attribute__((weak, alias("HTTP_ProcessDefault")));
extern uint16_t WEBSOCK_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])			__attribute__((weak, alias("WEBSOCK_ProcessDefault")));

static uint16_t HTTP_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)							{ return 0; }
static uint16_t WEBSOCK_FilterDefault(uint8_t *data, uint16_t len, uint16_t responseMaxLen)							{ return 0; }

static uint16_t HTTP_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])		{ return 0; }
static uint16_t WEBSOCK_ProcessDefault(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])	{ return 0; }

uint16_t TCP_Filter(uint32_t fromAddr, uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	TCP_Header_T header = {0};
	memcpy(&header, data, sizeof(TCP_Header_T));

	uint8_t *payload = &data[(header.dataOffset & 0xf0) >> 2];
	uint16_t payloadLen = len - ((header.dataOffset & 0xf0) >> 2);

	TCP_Connection_T *connection = TCP_FindConnection(fromAddr, header.fromPort, header.toPort);

	if (connection)
	{
		return TCP_HandleConnection(
			connection,
			&header,
			payload, payloadLen,
			data, responseMaxLen);
	}
	else
	{
		return TCP_HandleNoConnection(
			fromAddr,
			&header,
			payload, payloadLen,
			data, responseMaxLen - (len - payloadLen));
	}
}

uint16_t TCP_Process(uint8_t *sendData, uint16_t sendMaxLen, uint32_t *toIp, uint8_t toMac[6])
{

	uint8_t *payload = sendData + sizeof(TCP_Header_T);
	uint16_t payloadMaxLen = sendMaxLen - sizeof(TCP_Header_T);

	uint16_t payloadLen = 0;

	for (int i = 0; i < TCP_MAX_CONNECTIONS; i++)
	{
		TCP_Connection_T *connection = TCP_GetConnection(i);

		if (connection->status == TCP_ESTABLISHED)
		{
			switch (connection->protocol)
			{
				case TCP_PROTOCOL_HTTP:
					payloadLen = HTTP_Process(payload, payloadMaxLen, toIp, toMac);
					break;
				case TCP_PROTOCOL_WEBSOCK:
					payloadLen = WEBSOCK_Process(payload, payloadMaxLen, toIp, toMac);
					break;
				case TCP_PROTOCOL_UNKNOWN:
				default:
					break;
			}

			if (payloadLen)
			{
				TCP_BuildResponse(
					connection,
					TCP_FLAG_ACK | TCP_FLAG_PSH,
					0, 0, // no options
					payload, payloadLen,
					sendData, sendMaxLen);
				*toIp = connection->remoteAddr;

				extern const uint8_t *ARP_Resolve(const uint8_t *ipAddr);
				memcpy(toMac, ARP_Resolve((uint8_t *)toIp), 6);
				return payloadLen + sizeof(TCP_Header_T);
			}
		}
	}

	return 0;
}

static uint16_t TCP_HandleNoConnection(
	uint32_t fromAddr,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen)
{
	if (header->flags & TCP_FLAG_RST)
	{
		return 0;
	}
	else if (header->flags == TCP_FLAG_SYN)
	{
		if (TCP_IsPortListening(header->toPort))
		{
			TCP_Connection_T *connection = TCP_NewConnection(
				fromAddr,
				header->fromPort,
				header->toPort,
				header->seqNum);

			if (connection)
			{
				if (header->toPort == htons(80))
					connection->protocol = TCP_PROTOCOL_HTTP;

				connection->status = TCP_SYN_RECEIVED;
				connection->eventTime = SECONDS;

				return TCP_BuildSynAckResponse(connection, resp, responseMaxLen);
			}
			else
			{
				LOG("INSUFFICIENT CONNECTION POOL");
			}
		}
		else
		{
			LOG("Rejected TCP connection to port: %d", htons(header->fromPort));
		}
	}

	// TODO: implement:
	// If the incoming segment has an ACK field, the reset takes its
	// sequence number from the ACK field of the segment, otherwise the
	// reset has sequence number zero and the ACK field is set to the sum
	// of the sequence number and segment length of the incoming segment.
	// The connection remains in the same state.

	return TCP_BuildRstResponseImplicit(
		fromAddr,
		header,
		payload, payloadLen,
		resp, responseMaxLen);
}

static uint16_t TCP_HandleConnection(
	TCP_Connection_T *connection,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen)
{
	if (header->flags & TCP_FLAG_RST)
	{
		connection->status = TCP_CLOSED;
		return 0;
	}

	connection->eventTime = SECONDS;

	switch (connection->status)
	{
		case TCP_SYN_RECEIVED:
			return TCP_HandleSynRecieved(connection, header->flags, resp, responseMaxLen);

		case TCP_ESTABLISHED:
			if (header->ackNum == htonl(connection->seqNum))
			{
				if (header->flags == TCP_FLAG_ACK &&
					payloadLen == 1 &&
					header->seqNum == htonl(connection->ackNum-1))
				{
					// reject tcp keep-alive
					connection->status = TCP_FIN_WAIT;
					return TCP_BuildFinAckResponse(connection, resp, responseMaxLen);
				}
				else
				{
					return TCP_HandleEstablished(
						connection,
						header->flags,
						payload, payloadLen,
						resp, responseMaxLen);
				}
			}
			else
			{
				LOG("TCP RST: seqnum %d was not acknoledged", htonl(connection->seqNum));
				connection->status = TCP_CLOSED;
				return TCP_BuildRstResponse(connection, resp, responseMaxLen);
			}

		case TCP_FIN_ACK_WAIT:
			if (header->flags == TCP_FLAG_ACK)
				connection->status = TCP_CLOSED;
			return 0;

		case TCP_FIN_WAIT:
			if (header->flags == (TCP_FLAG_FIN | TCP_FLAG_ACK) ||
				header->flags == (TCP_FLAG_FIN | TCP_FLAG_ACK | TCP_FLAG_PSH))
			{
				connection->status = TCP_CLOSED;
				connection->ackNum++; // for received FIN flag

				return TCP_BuildResponse(
					connection,
					TCP_FLAG_ACK,
					0, 0, // no options
					0, 0, // no payload
					resp, responseMaxLen);
			}
			return 0;

		default:
			return 0;
	}
}

static uint16_t TCP_HandleSynRecieved(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *resp, uint16_t responseMaxLen)
{
	if (flags == TCP_FLAG_ACK) // handshake complete
	{
		connection->status = TCP_ESTABLISHED;
		return 0;
	}
	else if (flags == TCP_FLAG_SYN) // syn retransmition after lost syn ack?
	{
		return TCP_BuildSynAckResponse(connection, resp, responseMaxLen);
	}
	else // handshake complete ack lost?
	{
		LOG("TCP RST: Syn handshake fail");
		connection->status = TCP_CLOSED;
		return TCP_BuildRstResponse(connection, resp, responseMaxLen);
	}
}

static uint16_t TCP_HandleEstablished(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen)
{
	uint8_t respFlags = TCP_FLAG_ACK;

	if (flags & TCP_FLAG_FIN)
	{
		respFlags |= TCP_FLAG_FIN;
		connection->ackNum++;
		connection->status = TCP_FIN_ACK_WAIT;
	}

	if ((flags & TCP_FLAG_PSH) && payloadLen > 0)
	{
		uint16_t tcpResponseLen = 0;
		switch (connection->protocol)
		{
			case TCP_PROTOCOL_HTTP:
				tcpResponseLen = HTTP_Filter(payload, payloadLen, responseMaxLen - sizeof(TCP_Header_T));
				break;
			case TCP_PROTOCOL_WEBSOCK:
				tcpResponseLen = WEBSOCK_Filter(payload, payloadLen, responseMaxLen - sizeof(TCP_Header_T));
				break;
			case TCP_PROTOCOL_UNKNOWN:
			default:
				break;
		}

		connection->ackNum += payloadLen;
		connection->eventTime = SECONDS;

		if (!memcmp(payload, "HTTP/1.1 101", strlen("HTTP/1.1 101")))
		{
			connection->protocol = TCP_PROTOCOL_WEBSOCK;
		}

		return TCP_BuildResponse(
			connection,
			respFlags | (tcpResponseLen ? TCP_FLAG_PSH : 0),
			0, 0, // no options
			payload, tcpResponseLen,
			resp, responseMaxLen);
	}
	else if (flags & TCP_FLAG_FIN)
		return TCP_BuildFinAckResponse(connection, resp, responseMaxLen);
	else
		return 0;
}

static uint16_t TCP_BuildRstResponseImplicit(
	uint32_t fromAddr,
	TCP_Header_T *header,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen)
{
	header->ackNum = htonl(header->ackNum);
	header->ackNum += payloadLen;
	header->ackNum = htonl(header->ackNum);

	TCP_Header_T respHeader = {
		.fromPort	= header->toPort,
		.toPort		= header->fromPort,
		.seqNum		= header->ackNum,
		.ackNum		= header->seqNum,
		.dataOffset	= sizeof(TCP_Header_T) << 2,
		.flags		= TCP_FLAG_RST,
		.window		= htons(responseMaxLen),
		.cksum		= 0,
		.urgentPtr	= 0
	};

	uint32_t chSum = TCP_PseudoHeaderChecksum(fromAddr, sizeof(TCP_Header_T));
	chSum = checksum(chSum, (uint8_t *)&respHeader, sizeof(TCP_Header_T));
	respHeader.cksum = chSum;

	memcpy(resp, &respHeader, sizeof(TCP_Header_T));

	return sizeof(TCP_Header_T);
}

static uint16_t TCP_BuildRstResponse(
	TCP_Connection_T *connection,
	uint8_t *resp, uint16_t responseMaxLen)
{
	return TCP_BuildResponse(
		connection,
		TCP_FLAG_RST,
		0, 0, // no options
		0, 0, // no payload
		resp, responseMaxLen);
}

static uint16_t TCP_BuildSynAckResponse(
	TCP_Connection_T *connection,
	uint8_t *resp, uint16_t responseMaxLen)
{
	return TCP_BuildResponse(
		connection,
		TCP_FLAG_SYN | TCP_FLAG_ACK,
		(uint8_t *)&mss, sizeof(TCP_MSS_T),
		0, 0, // no payload
		resp, responseMaxLen);
}

static uint16_t TCP_BuildFinAckResponse(
	TCP_Connection_T *connection,
	uint8_t *resp, uint16_t responseMaxLen)
{
	return TCP_BuildResponse(
		connection,
		TCP_FLAG_FIN | TCP_FLAG_ACK,
		0, 0, // no options
		0, 0, // no payload
		resp, responseMaxLen);
}

static uint16_t TCP_BuildResponse(
	TCP_Connection_T *connection,
	uint8_t flags,
	uint8_t *options, uint16_t optionsLen,
	uint8_t *payload, uint16_t payloadLen,
	uint8_t *resp, uint16_t responseMaxLen)
{
	uint16_t respHeaderLen = sizeof(TCP_Header_T) + optionsLen;

	TCP_Header_T respHeader = {
		.fromPort	= connection->localPort,
		.toPort		= connection->remotePort,
		.seqNum		= htonl(connection->seqNum),
		.ackNum		= htonl(connection->ackNum),
		.dataOffset	= respHeaderLen << 2,
		.flags		= flags,
		.window		= htons(1024),//htons(responseMaxLen),
		.cksum		= 0,
		.urgentPtr	= 0
	};

	uint32_t chSum = TCP_PseudoHeaderChecksum(connection->remoteAddr, respHeaderLen + payloadLen);
	chSum = checksumPart(chSum, (uint8_t *)&respHeader, sizeof(TCP_Header_T));
	chSum = checksumPart(chSum, (uint8_t *)options, optionsLen);
	chSum = checksum(chSum, (uint8_t *)payload, payloadLen);
	respHeader.cksum = chSum;

	memcpy(resp, &respHeader, sizeof(TCP_Header_T));
	memcpy(resp + sizeof(TCP_Header_T), options, optionsLen);
	if (resp + respHeaderLen != payload)
		memmove(resp + respHeaderLen, payload, payloadLen);

	connection->seqNum += payloadLen;
	if (flags & (TCP_FLAG_SYN | TCP_FLAG_FIN | TCP_FLAG_RST))
		connection->seqNum++;

	return sizeof(TCP_Header_T) + optionsLen + payloadLen;
}

#define IP_PROTOCOL_TCP 6
static uint32_t TCP_PseudoHeaderChecksum(uint32_t remoteAddr, uint16_t packetLen)
{
	TCP_ChecksumHeader_T chSumHeader;
	chSumHeader.fromAddr = TCP_GetIpExt();
	chSumHeader.toAddr = remoteAddr;
	chSumHeader.zero = 0;
	chSumHeader.protocol = IP_PROTOCOL_TCP;
	chSumHeader.len = htons(packetLen);

	return checksumPart(0, (uint8_t *)&chSumHeader, sizeof(TCP_ChecksumHeader_T));
}
