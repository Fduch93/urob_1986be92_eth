#include "network.h"
#include "enc28j60.h"
#include "ethernet.h"
#include "arp.h"
#include "ip.h"
#include "udp.h"
#include "dhcp.h"
#include "icmp.h"
#include "tcp.h"
#include "tcp_connections.h"
#include "http.h"

#if ARP_CACHE_SIZE < TCP_MAX_CONNECTIONS
	#error Increase ARP cache
#endif

#define NET_BUF_SIZE 2048
static uint8_t netBuf[NET_BUF_SIZE];

static const uint8_t mac[6] = {0x00, 0x23, 0x8B, 0x0A, 0xC6, 0x2C};

void NET_Init(void)
{
	enc28j60_init(mac);
}

void NET_Process(void)
{
	if (enc28j60_read_phy(PHSTAT2) & PHSTAT2_LSTAT)
	{
		enc28j60_send_packet(netBuf,
			ETH_Filter(netBuf,
				enc28j60_recv_packet(netBuf, sizeof(netBuf)),
				sizeof(netBuf)));

		enc28j60_send_packet(netBuf,
			ETH_Process(netBuf, sizeof(netBuf)));
	}
}

//static const uint8_t *resolveMac(uint32_t toAddr);

static const uint8_t *NET_GetMac(void){ return mac; }
const uint8_t *ETH_GetMacExt(void)	__attribute__((alias("NET_GetMac")));
const uint8_t *ARP_GetMacExt(void)	__attribute__((alias("NET_GetMac")));
const uint8_t *DHCP_GetMacExt(void)	__attribute__((alias("NET_GetMac")));
const uint8_t *TCP_GetMacExt(void)	__attribute__((alias("NET_GetMac")));

static uint32_t NET_GetIp(void){ return IP_GetAddr(); }
uint32_t ARP_GetIpExt(void)			__attribute__((alias("NET_GetIp")));
uint32_t DHCP_GetIpExt(void)		__attribute__((alias("NET_GetIp")));
uint32_t NBNS_GetIpExt(void)		__attribute__((alias("NET_GetIp")));
uint32_t TCP_GetIpExt(void)			__attribute__((alias("NET_GetIp")));

void DHCP_SetIpMaskGateway(uint32_t ipAddr, uint32_t mask, uint32_t gateway)
{
	IP_SetAddr(ipAddr);
	IP_SetMask(mask);
	IP_SetGateway(gateway);
}

int TCP_IsPortListening(uint16_t port)
{
	// accept connections to port 80
	return (port == htons(80));
}
/*
static const uint8_t *resolveMac(uint32_t toAddr)
{
	uint32_t ipAddr = IP_GetAddr();
	uint32_t ipMask = IP_GetMask();
	uint32_t ipGateway = IP_GetGateway();

	int isBroadcast = toAddr == (ipAddr | ~ipMask);
	if (isBroadcast)
		return ARP_GetBroadcastMac();

	int is_remote = (toAddr ^ ipAddr) & ipMask;
	uint32_t route_ip = is_remote ? ipGateway : toAddr;

	const uint8_t *resolvedMac = ARP_Resolve((uint8_t *)&route_ip);
	return resolvedMac;
}*/
