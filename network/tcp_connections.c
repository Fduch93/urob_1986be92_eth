#include "tcp_connections.h"
#include "beconv.h"
#include "TICK.h"

static TCP_Connection_T tcpPool[TCP_MAX_CONNECTIONS] = {{
	.status		= TCP_CLOSED,
	.eventTime	= 0,
	.seqNum		= 0,
	.ackNum		= 0,
	.remoteAddr	= 0,
	.remotePort	= 0,
	.localPort	= 0,
	.bufStatus	= TCP_BUF_EMPTY,
	.bufLen		= 0,
	.buf		= 0
}};

TCP_Connection_T *TCP_GetConnection(int index)
{
	return &tcpPool[index];
}

TCP_Connection_T *TCP_NewConnection(uint32_t fromAddr, uint16_t remotePort, uint16_t localPort, uint32_t peerSeqNum)
{
	uint32_t now = SECONDS;
	for (int i = 0; i < TCP_MAX_CONNECTIONS; i++)
	{
		TCP_Connection_T *connection = &tcpPool[i];
		if (connection->status == TCP_CLOSED ||
			now - connection->eventTime > TCP_CONN_TIMEOUT)
		{
			connection->status = TCP_SYN_RECEIVED;
			connection->eventTime = now;
			connection->seqNum = now | (now << 16);
			connection->ackNum = ntohl(peerSeqNum) + 1;
			connection->remoteAddr = fromAddr;
			connection->remotePort = remotePort;
			connection->localPort = localPort;
			return connection;
		}
	}
	return 0;
}

TCP_Connection_T *TCP_FindConnection(uint32_t fromAddr, uint16_t remotePort, uint16_t localPort)
{
	for (int i = 0; i < TCP_MAX_CONNECTIONS; i++)
	{
		TCP_Connection_T *connection = &tcpPool[i];
		if ((connection->status != TCP_CLOSED) &&
			(fromAddr == connection->remoteAddr) &&
			(remotePort == connection->remotePort) &&
			(localPort == connection->localPort))
			return connection;
	}
	return 0;
}
