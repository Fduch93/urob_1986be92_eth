#ifndef beconv_h
#define beconv_h

#define htons(a)			((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs(a)			htons(a)

#define htonl(a)			( ((((unsigned)a)>>24) & 0x000000ffU) | ((((unsigned)a)>>8)  & 0x0000ff00U) \
							| ((((unsigned)a)<<8)  & 0x00ff0000U) | ((((unsigned)a)<<24) & 0xff000000U) )
#define ntohl(a)			htonl(a)

#define inetAddr(a,b,c,d)	( ((unsigned)a) | ((unsigned)b << 8) |\
							((unsigned)c << 16) | ((unsigned)d << 24) )

#define arrayToIp(a)	( ((unsigned)a[0]) | ((unsigned)a[1] << 8) |\
							((unsigned)a[2] << 16) | ((unsigned)a[3] << 24) )

#endif
