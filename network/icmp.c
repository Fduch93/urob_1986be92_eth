#include "icmp.h"
#include <string.h>

uint16_t ICMP_Filter(uint8_t *data, uint16_t len, uint16_t responseMaxLen)
{
	if (len >= sizeof(ICMP_Header_T))
	{
		ICMP_Header_T header = {0};
		memcpy(&header, data, sizeof(ICMP_Header_T));

		if (header.type == ICMP_TYPE_ECHO_RQ)
		{
			header.type = ICMP_TYPE_ECHO_RPLY;
			header.cksum += 8; // update cksum

			memcpy(data, &header, sizeof(ICMP_Header_T));

			return len;
		}
	}
	return 0;
}
