#include "swap_init.h"
#include "FAT_volume.h"
#include "FAT_entry.h"
#include <string.h>

void SWAP_MakeMbr(
	FAT_MasterBootRecord_T *mbr,
	uint32_t partFirstSector,
	uint32_t partSectorCount)
{
	memset(mbr, 0, sizeof(FAT_MasterBootRecord_T));

	mbr->mbrSignature[0] = 0x55;
	mbr->mbrSignature[1] = 0xAA;

	FAT_PartitionEntry_T *part = &mbr->partitions[0];

	part->isActive = 0x80;
	part->partitionType = 0x0C;

	memcpy(part->firstSector, &partFirstSector, sizeof(uint32_t));
	memcpy(part->sectorCount, &partSectorCount, sizeof(uint32_t));

	uint32_t headCount = 255;
	uint32_t sectorsPerTrack = 63;
	uint32_t lastSector = (partFirstSector + partSectorCount - 1);

	uint32_t startHead		= (partFirstSector % (headCount * sectorsPerTrack)) / sectorsPerTrack;
	uint32_t startSector	= (partFirstSector % sectorsPerTrack) + 1;
	uint32_t startCylinder	=  partFirstSector / (headCount * sectorsPerTrack);

	uint32_t stopHead		= (lastSector % (headCount * sectorsPerTrack)) / sectorsPerTrack;
	uint32_t stopSector		= (lastSector % sectorsPerTrack) + 1;
	uint32_t stopCylinder	=  lastSector / (headCount * sectorsPerTrack);

	startHead		= 0xFE; //(startHead		< 0xFF) ? startHead		: 0xFE;
	startSector		= 0xFF; //(startSector		< 0xFF) ? startSector	: 0xFF;
	startCylinder	= 0xFF; //(startCylinder	< 0xFF) ? startCylinder	: 0xFF;

	stopHead		= 0xFE; //(stopHead			< 0xFF) ? stopHead		: 0xFE;
	stopSector		= 0xFF; //(stopSector		< 0xFF) ? stopSector	: 0xFF;
	stopCylinder	= 0xFF; //(stopCylinder		< 0xFF) ? stopCylinder	: 0xFF;

	part->startHead = startHead;
	part->startSector = startSector;
	part->startCylinder = startCylinder;

	part->stopHead = stopHead;
	part->stopSector = stopSector;
	part->stopCylinder = stopCylinder;
}

void SWAP_MakeBootSector(FAT_BootSector_T *bootSector,
	uint32_t partFirstSector,
	uint32_t partSectorCount)
{
	bootSector->bootJump[0] = 0xEB;
	bootSector->bootJump[1] = 0xFE;
	bootSector->bootJump[2] = 0x90;
	memcpy(bootSector->oemName, "MSDOS5.0", 8);

	uint16_t bytesPerSector = sizeof(SWAP_Sector_T);
	memcpy(bootSector->bytesPerSector, &bytesPerSector, 2);

	bootSector->sectorsPerCluster = 1;

	uint16_t sectorsReserved = 4; // bs, fsinfo, bkp_bs, bkp_fsinfo
	memcpy(bootSector->sectorsReserved, &sectorsReserved, 2);

	bootSector->fatCount = 1;

	memset(bootSector->fatRootCount, 0x00, 2);
	memset(bootSector->sectorsCount, 0x00, 2);

	bootSector->mediaDescriptor = 0xF8;

	memset(bootSector->sectorsPerFatDOS2, 0x00, 2);

	uint16_t sectorsPerTrack = 0x3F;
	memcpy(bootSector->sectorsPerTrack, &sectorsPerTrack, 2);
	uint16_t heads = 0xFF;
	memcpy(bootSector->heads, &heads, 2);

	memcpy(bootSector->sectorsHidden, &partFirstSector, 4);

	uint32_t sectorsTotalDOS3 = partSectorCount;
	memcpy(bootSector->sectorsTotalDOS3, &sectorsTotalDOS3, 4);
//	memcpy(bootSector->sectorsTotalDOS3, &partSectorCount, 4);

	uint32_t sectorsPerFat = 1;
	memcpy(bootSector->sectorsPerFat, &sectorsPerFat, 4);

	memset(bootSector->descrMirror, 0x00, 2);
	memset(bootSector->version, 0x00, 2);

	uint32_t rootCluster = 2;
	memcpy(bootSector->rootCluster, &rootCluster, 4);

	uint16_t infoSectorOffset = 0x0001;
	memcpy(bootSector->infoSectorOffset, &infoSectorOffset, 2);

	uint16_t bootBackupSectorOffset = 0x0002;
	memcpy(bootSector->bootBackupSectorOffset, &bootBackupSectorOffset, 2);

	memset(bootSector->reserved34h, 0x00, 12);

	bootSector->physicalDriveNumber = 0x80;
	
	bootSector->reserved41h = 0x00;
		bootSector->dityFlag = 0;
		bootSector->errorFlag = 0;

	bootSector->extebdedBootSignature = 0x29;

	uint32_t volumeID = 0xACAB1488;
	memcpy(bootSector->volumeID, &volumeID, 4);
	memcpy(bootSector->volumeLabel, "MDR", 3);
	memset(&bootSector->volumeLabel[3], ' ', 11-3);
	memcpy(bootSector->fileSystemType, "FAT32", 5);
	memset(&bootSector->fileSystemType[5], ' ', 8-5);

	memset(bootSector->bootCode, 0x00, 420);

	bootSector->bootSectorSignature[0] = 0x55;
	bootSector->bootSectorSignature[1] = 0xAA;
}

void SWAP_MakeFat(SWAP_FatSector_T *fatSector)
{
	memset(fatSector, 0x00, sizeof(SWAP_Sector_T));

	fatSector->clusters[0] = 0xFFFFFF00 | 0xF8;
	fatSector->clusters[1] = 0x0FFFFFFF;
	fatSector->clusters[2] = 0x0FFFFFFF;
}

void SWAP_MakeRoot(SWAP_RootSector_T *rootSector)
{
	memset(rootSector, 0x00, sizeof(SWAP_Sector_T));

	memcpy(rootSector->entries[0].shortName, "MDR_RAM ", 8);
	memcpy(rootSector->entries[0].shortExtension, "   ", 3);
	rootSector->entries[0].attributes = 0x08;
}
