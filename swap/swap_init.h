#ifndef SWAP_INIT_H
#define SWAP_INIT_H

#include "FAT_volume.h"
#include "FAT_entry.h"
#include <string.h>

typedef uint8_t SWAP_Sector_T[512];

typedef union {
	SWAP_Sector_T sect;
	uint32_t clusters[sizeof(SWAP_Sector_T)/sizeof(uint32_t)];
} SWAP_FatSector_T;

typedef union {
	SWAP_Sector_T sect;
	FAT_Entry_T entries[sizeof(SWAP_Sector_T)/sizeof(FAT_Entry_T)];
} SWAP_RootSector_T;

void SWAP_MakeMbr(
	FAT_MasterBootRecord_T *mbr,
	uint32_t partFirstSector,
	uint32_t partSectorCount);

void SWAP_MakeBootSector(FAT_BootSector_T *bootSector,
	uint32_t partFirstSector,
	uint32_t partSectorCount);

void SWAP_MakeFat(SWAP_FatSector_T *fatSector);
void SWAP_MakeRoot(SWAP_RootSector_T *rootSector);

#endif
