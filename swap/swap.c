#include "swap.h"
#include "swap_init.h"
#include "FAT_volume.h"
#include "FAT_fat.h"
#include "FAT_entry.h"
#include "FAT_utils.h"
#include <string.h>

static union {
	SWAP_Sector_T driveSect[17];
	struct {
		struct {
			FAT_MasterBootRecord_T mbr;
			FAT_BootSector_T bootSector;
			SWAP_Sector_T fsInfo;
			FAT_BootSector_T bootSectorBackup;
			SWAP_Sector_T fsInfoBackup;
			SWAP_FatSector_T fat;
			SWAP_RootSector_T root;
		} headerSect;
		SWAP_Sector_T dataSect[10];
	};
} swap;

static uint8_t sectorBuffer[sizeof(SWAP_Sector_T)] __attribute__((aligned(4)));

uint8_t *FAT_GetSectorBuffer()
{
	return sectorBuffer;
}

uint32_t *FAT_GetSectorBuffer32()
{
	return (uint32_t *)sectorBuffer;
}

#include "log.h"

static char *SWAP_GetSectorDescription(uint32_t sector)
{
	switch (sector)
	{
		case 0: return "MBR";
		case 1: return "PBR";
		case 2: return "FS Info";
		case 3: return "Boot backup";
		case 4: return "FS Info backup";
		case 5: return "FAT ";
		case 6: return "Root directory";
		default:
			if (sector < sizeof(swap)/sizeof(SWAP_Sector_T))
				return "Data";
			else
				return "\0";
	}
}

char FAT_ReadSector(uint32_t sector)
{
	LOG("SWAP Read sector %d, %s", sector, (uint32_t)SWAP_GetSectorDescription(sector));
	
	if (sector < sizeof(swap)/sizeof(SWAP_Sector_T))
		memcpy(sectorBuffer, &swap.driveSect[sector], sizeof(SWAP_Sector_T));
	else
		memset(sectorBuffer, 0, sizeof(SWAP_Sector_T));
		
	return 1;
}

char FAT_WriteSector(uint32_t sector)
{
	LOG("SWAP Write sector %d, %s", sector, (uint32_t)SWAP_GetSectorDescription(sector));
	
	if (sector < sizeof(swap)/sizeof(SWAP_Sector_T))
		memcpy(&swap.driveSect[sector], sectorBuffer, sizeof(SWAP_Sector_T));
	
	return 1;
}

void SWAP_Init(void)
{
	uint32_t driveSectorCount = sizeof(swap)/sizeof(SWAP_Sector_T);
	uint32_t partFirstSector = 1;
	uint32_t partSectorCount = driveSectorCount - partFirstSector;
	SWAP_MakeMbr(&swap.headerSect.mbr, partFirstSector, driveSectorCount);
	SWAP_MakeBootSector(&swap.headerSect.bootSector, partFirstSector, partSectorCount);
	
	SWAP_MakeFat(&swap.headerSect.fat);
	SWAP_MakeRoot(&swap.headerSect.root);
	
	FAT_Volume_T volume = {0};
	FAT_ReadFirstVolume(&volume);
	
	FAT_Directory_T root = {0};
	FAT_ReadRootDirectory(&root, &volume, 0,0,0,0);
	
	FAT_Directory_T dir = {0};
	FAT_CreateDirectory(&dir, "QWE", &root, &volume);
	
	uint32_t startCluster = FAT_AllocateSingleCluster(&volume);
	
	FAT_File_T file = {0};
	FAT_CreateFile(&file, "QWE.TXT", startCluster, &dir, &volume);
	
	uint8_t *buf = FAT_GetSectorBuffer();
	memset(buf, 0, sizeof(SWAP_Sector_T));
	const char *str = "AYE AM MELANDER\r\nAYE CONNECT\r\nAYE INTERACT";
	memcpy(buf, str, strlen(str));
	FAT_WriteSector(FAT_ClusterToSector(file.firstCluster, &volume));
	
	FAT_SetFileSize(file.firstCluster,strlen(str), &dir, &volume);
}
