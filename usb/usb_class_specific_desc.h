#ifndef USB_CLASS_SPECIFIC_DESC_H
#define USB_CLASS_SPECIFIC_DESC_H

#include <stdint.h>

#define USB_DESC_SIZE_CLASS_SPECIFIC_HEADER	3
#define USB_DESC_TYPE_CLASS_SPECIFIC		0x24

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint8_t subtype;
	uint8_t *data;
} USB_ClassSpecificDesc_T
__attribute__((aligned(4)));

void USB_ClassSpecificDesc(
	USB_ClassSpecificDesc_T *classSpecific,
	uint8_t size,
	uint8_t subtype,
	uint8_t dataSize,
	void *data);

#endif
