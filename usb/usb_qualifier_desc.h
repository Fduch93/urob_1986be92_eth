#ifndef USB_QUALIFIER_DESC_H
#define USB_QUALIFIER_DESC_H

#include "usb_device_desc.h"

#define USB_DESC_SIZE_QUALIFIER	10
#define USB_DESC_TYPE_QUALIFIER	0x06

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint16_t usbVer;
	uint8_t devClass;
	uint8_t devSubclass;
	uint8_t devProtocol;
	uint8_t maxPacketSize;
	uint8_t configurationCnt;
	uint8_t reserved;
} USB_QualifierDesc_T;

uint8_t *USB_QualifierDesc(USB_DeviceDesc_T *deviceDesc);

#endif
