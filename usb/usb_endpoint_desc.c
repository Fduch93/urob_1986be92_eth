#include "usb_endpoint_desc.h"

void USB_EndpointDesc(
	USB_EndpointDesc_T *desc,
	uint8_t address,
	uint8_t transferType,
	uint16_t maxPacketSize,
	uint8_t pollingInterval)
{
	desc->size = USB_DESC_SIZE_ENDPOINT;
	desc->type = USB_DESC_TYPE_ENDPOINT;
	desc->address = address;
	desc->transferType = transferType;
	desc->maxPacketSize = maxPacketSize;
	desc->pollingInterval = pollingInterval;
}
