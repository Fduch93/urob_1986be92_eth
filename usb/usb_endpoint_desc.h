#ifndef USB_ENDPOINT_DESC_H
#define USB_ENDPOINT_DESC_H

#include <stdint.h>

#define EP_TYPE_CTRL 0U
#define EP_TYPE_ISOC 1U
#define EP_TYPE_BULK 2U
#define EP_TYPE_INTR 3U

#define USB_DESC_SIZE_ENDPOINT	7
#define USB_DESC_TYPE_ENDPOINT	0x05
#define USB_DESC_TYPE_CS_ENDPOINT 0x25

#define ENDPOINT_IN			0x80
#define ENDPOINT_OUT		0x00

#define NO_POLLING_INTERVAL	0

#define ENDPOINT_STALL		0x0001
#define ENDPOINT_NO_STALL	0x0000

typedef struct
{
	uint8_t size;
	uint8_t type;
	union {
		uint8_t address;
		struct {
			uint8_t number:7;
			uint8_t isInEndpoint:1;
		};
	};
	uint8_t transferType;
	uint16_t maxPacketSize;
	uint8_t pollingInterval;
} USB_EndpointDesc_T
__attribute__((aligned(4)));

void USB_EndpointDesc(
	USB_EndpointDesc_T *desc,
	uint8_t address,
	uint8_t transferType,
	uint16_t maxPacketSize,
	uint8_t pollingInterval);

#endif
