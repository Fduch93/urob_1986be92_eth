#include "usb_device_desc.h"

void USB_DeviceDesc(
	USB_DeviceDesc_T *desc,
	uint16_t usbVer,
	uint8_t devClass,
	uint8_t devSubclass,
	uint8_t devProtocol,
	uint8_t maxPacketSize,
	uint16_t vid,
	uint16_t pid,
	uint16_t devVer,
	uint8_t manufacturerStrIdx,
	uint8_t productStrIdx,
	uint8_t serialStrIdx,
	uint8_t configurationCnt)
{
	desc->size = USB_DESC_SIZE_DEVICE;
	desc->type = USB_DESC_TYPE_DEVICE;
	desc->usbVer = usbVer;
	desc->devClass = devClass;
	desc->devSubclass = devSubclass;
	desc->devProtocol = devProtocol;
	desc->maxPacketSize = maxPacketSize;
	desc->vid = vid;
	desc->pid = pid;
	desc->devVer = devVer;
	desc->manufacturerStrIdx = manufacturerStrIdx;
	desc->productStrIdx = productStrIdx;
	desc->serialStrIdx = serialStrIdx;
	desc->configurationCnt = configurationCnt;
}
