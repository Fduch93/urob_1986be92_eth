#ifndef USB_DEVICE_DESC_H
#define USB_DEVICE_DESC_H

#include <stdint.h>

#define USB_DESC_SIZE_DEVICE	18
#define USB_DESC_TYPE_DEVICE	0x01

#define DEV_CLASS_BY_INTERFACE		0x00
#define DEV_SUBCLASS_BY_INTERFACE	0x00
#define DEV_PROTOCOL_BY_INTERFACE	0x00

#define USB_VER_2_0 (0x0200)
#define USB_VER_1_1 (0x0110)
#define DEV_VER_2_0 (0x0200)
#define USB_LANG_EN (1033)

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint16_t usbVer;
	uint8_t devClass;
	uint8_t devSubclass;
	uint8_t devProtocol;
	uint8_t maxPacketSize;
	uint16_t vid;
	uint16_t pid;
	uint16_t devVer;
	uint8_t manufacturerStrIdx;
	uint8_t productStrIdx;
	uint8_t serialStrIdx;
	uint8_t configurationCnt;
} USB_DeviceDesc_T
__attribute__((aligned(4)));

void USB_DeviceDesc(
	USB_DeviceDesc_T *desc,
	uint16_t usbVer,
	uint8_t devClass,
	uint8_t devSubclass,
	uint8_t devProtocol,
	uint8_t maxPacketSize,
	uint16_t vid,
	uint16_t pid,
	uint16_t devVer,
	uint8_t manufacturerStrIdx,
	uint8_t productStrIdx,
	uint8_t serialStrIdx,
	uint8_t configurationCnt);

#endif
