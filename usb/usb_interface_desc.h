#ifndef USB_INTERFACE_DESC_H
#define USB_INTERFACE_DESC_H

#include <stdint.h>

#define USB_DESC_SIZE_INTERFACE	9
#define USB_DESC_TYPE_INTERFACE	0x04

#define USB_INTERFACE_NO_ALTERNATIVE	0x00

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint8_t interfaceIdx;
	uint8_t altSetting;
	uint8_t endpointCnt;
	uint8_t interfaceClass;
	uint8_t interfaceSubclass;
	uint8_t interfaceProtocol;
	uint8_t interfaceStrIdx;
} USB_Interface_Desc_T
__attribute__((aligned(4)));

void USB_Interface_Desc(
	USB_Interface_Desc_T *desc,
	uint8_t interfaceIdx,
	uint8_t altSetting,
	uint8_t endpointCnt,
	uint8_t interfaceClass,
	uint8_t interfaceSubclass,
	uint8_t interfaceProtocol,
	uint8_t interfaceStrIdx);

#endif
