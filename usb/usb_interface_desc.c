#include "usb_interface_desc.h"

void USB_Interface_Desc(
	USB_Interface_Desc_T *desc,
	uint8_t interfaceIdx,
	uint8_t altSetting,
	uint8_t endpointCnt,
	uint8_t interfaceClass,
	uint8_t interfaceSubclass,
	uint8_t interfaceProtocol,
	uint8_t interfaceStrIdx)
{
	desc->size = USB_DESC_SIZE_INTERFACE;
	desc->type = USB_DESC_TYPE_INTERFACE;
	desc->interfaceIdx = interfaceIdx;
	desc->altSetting = altSetting;
	desc->endpointCnt = endpointCnt;
	desc->interfaceClass = interfaceClass;
	desc->interfaceSubclass = interfaceSubclass;
	desc->interfaceProtocol = interfaceProtocol;
	desc->interfaceStrIdx = interfaceStrIdx;
}
