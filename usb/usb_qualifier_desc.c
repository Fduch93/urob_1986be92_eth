#include "usb_qualifier_desc.h"

USB_QualifierDesc_T qualifier;

uint8_t *USB_QualifierDesc(USB_DeviceDesc_T *deviceDesc)
{
	qualifier.size = USB_DESC_SIZE_QUALIFIER;
	qualifier.type = USB_DESC_TYPE_QUALIFIER;
	qualifier.usbVer = deviceDesc->usbVer;
	qualifier.devClass = deviceDesc->devClass;
	qualifier.devSubclass = deviceDesc->devSubclass;
	qualifier.devProtocol = deviceDesc->devProtocol;
	qualifier.maxPacketSize = deviceDesc->maxPacketSize;
	qualifier.configurationCnt = deviceDesc->configurationCnt;
	qualifier.reserved = 0;

	return (uint8_t *)&qualifier;
}
