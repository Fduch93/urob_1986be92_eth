#ifndef USB_CONFIGURATION_DESC_H
#define USB_CONFIGURATION_DESC_H

#include <stdint.h>

#define USB_DESC_SIZE_CONFIGURATION	9
#define USB_DESC_TYPE_CONFIGURATION	0x02

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint16_t cfgSize;
	uint8_t interfaceCnt;
	uint8_t cfgIdx;
	uint8_t cfgStrIdx;
	uint8_t attributes;
	uint8_t maxPower;
} USB_ConfigurationDesc_T
__attribute__((aligned(4)));

void USB_ConfigurationDesc(
	USB_ConfigurationDesc_T *desc,
	uint16_t cfgSize,
	uint8_t interfaceCnt,
	uint8_t cfgIdx,
	uint8_t cfgStrIdx,
	uint8_t attributes,
	uint8_t maxPower);

#endif
