#include "usb_configuration_desc.h"

void USB_ConfigurationDesc(
	USB_ConfigurationDesc_T *desc,
	uint16_t cfgSize,
	uint8_t interfaceCnt,
	uint8_t cfgIdx,
	uint8_t cfgStrIdx,
	uint8_t attributes,
	uint8_t maxPower)
{
	desc->size = USB_DESC_SIZE_CONFIGURATION;
	desc->type = USB_DESC_TYPE_CONFIGURATION;
	desc->cfgSize = cfgSize;
	desc->interfaceCnt = interfaceCnt;
	desc->cfgIdx = cfgIdx;
	desc->cfgStrIdx = cfgStrIdx;
	desc->attributes = attributes;
	desc->maxPower = maxPower;
}
