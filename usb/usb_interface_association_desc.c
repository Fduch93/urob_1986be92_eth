#include "usb_interface_association_desc.h"

void USB_InterfaceAssociationDesc(
	USB_InterfaceAssociationDesc_T *desc,
	uint8_t interfaceIdx,
	uint8_t interfaceCnt,
	uint8_t functionClass,
	uint8_t functionSubclass,
	uint8_t functionProtocol,
	uint8_t functionStrIdx)
{
	desc->size = USB_DESC_SIZE_INTERFACE_ASSOCIATION;
	desc->type = USB_DESC_TYPE_INTERFACE_ASSOCIATION;
	desc->interfaceIdx = interfaceIdx;
	desc->interfaceCnt = interfaceCnt;
	desc->functionClass = functionClass;
	desc->functionSubclass = functionSubclass;
	desc->functionProtocol = functionProtocol;
	desc->functionStrIdx = functionStrIdx;
}
