#ifndef USB_INTERFACE_ASSOCIATION_DESC_H
#define USB_INTERFACE_ASSOCIATION_DESC_H

#include <stdint.h>

#define USB_DESC_SIZE_INTERFACE_ASSOCIATION	8
#define USB_DESC_TYPE_INTERFACE_ASSOCIATION	0x0B

typedef struct
{
	uint8_t size;
	uint8_t type;
	uint8_t interfaceIdx;
	uint8_t interfaceCnt;
	uint8_t functionClass;
	uint8_t functionSubclass;
	uint8_t functionProtocol;
	uint8_t functionStrIdx;
} USB_InterfaceAssociationDesc_T
__attribute__((aligned(4)));

void USB_InterfaceAssociationDesc(
	USB_InterfaceAssociationDesc_T *desc,
	uint8_t interfaceIdx,
	uint8_t interfaceCnt,
	uint8_t functionClass,
	uint8_t functionSubclass,
	uint8_t functionProtocol,
	uint8_t functionStrIdx);

void USB_ParseInterfaceAssociationDescOnly(
	USB_InterfaceAssociationDesc_T *desc,
	uint8_t *buffer);

#endif
