#include "usb_class_specific_desc.h"

void USB_ClassSpecificDesc(
	USB_ClassSpecificDesc_T *classSpecific,
	uint8_t size,
	uint8_t subtype,
	uint8_t dataSize,
	void *data)
{
	classSpecific->size = size;//USB_DESC_SIZE_CLASS_SPECIFIC_HEADER + dataSize;
	classSpecific->type = USB_DESC_TYPE_CLASS_SPECIFIC;
	classSpecific->subtype = subtype;
	classSpecific->data = (uint8_t *)data;
}
