#ifndef USB_SETUP_REQUEST_H
#define USB_SETUP_REQUEST_H

#include <stdint.h>

#define USB_REQ_DIR_TO_DEVICE	0
#define USB_REQ_DIR_TO_HOST		1

#define USB_REQ_TYPE_STANDARD	0
#define USB_REQ_TYPE_CLASS		1
#define USB_REQ_TYPE_VENDOR		2

#define USB_REQ_RECIPIENT_DEVICE	0
#define USB_REQ_RECIPIENT_INTERFACE	1
#define USB_REQ_RECIPIENT_ENDPOINT	2

#define USB_LEN_LANGID_STR_DESC	0x04
#define USB_DESC_TYPE_STRING	3

#define USB_REQ_GET_STATUS			0
#define USB_REQ_CLEAR_FEATURE		1
#define USB_REQ_SET_FEATURE			3
#define USB_REQ_SET_ADDRESS			5
#define USB_REQ_GET_DESCRIPTOR		6
#define USB_REQ_SET_DESCRIPTOR		7
#define USB_REQ_GET_CONFIGURATION	8
#define USB_REQ_SET_CONFIGURATION	9
#define USB_REQ_GET_INTERFACE		10
#define USB_REQ_SET_INTERFACE		11
#define USB_REQ_SYNCH_FRAME			12

typedef union
{
	uint8_t u8[8];
	struct
	{
		struct {
			uint8_t recipient	:5;
			uint8_t type		:2;
			uint8_t direction	:1;
		};
		uint8_t				request;
		uint16_t			value;
		uint16_t			index;
		uint16_t			length;
	};
} USB_SetupReq_T;

typedef enum
{
	USBD_REQ_OP_NONE,
	USBD_REQ_OP_ERROR,
	USBD_REQ_OP_SEND_STATUS,
	USBD_REQ_OP_RECEIVE_STATUS,
	USBD_REQ_OP_SEND_DATA,
	USBD_REQ_OP_RECEIVE_DATA
} USB_SetupReqResponseOp_E;

typedef struct
{
	USB_SetupReqResponseOp_E operation;
	uint8_t *data;
	uint16_t len;
} USB_SetupReqResponse_T;

#endif
