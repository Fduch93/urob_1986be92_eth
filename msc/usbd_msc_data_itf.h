#ifndef USBD_MSC_DATA_ITF_H
#define USBD_MSC_DATA_ITF_H

#include "usbd_interface.h"

#define BOT_GET_MAX_LUN			0xFE
#define BOT_RESET				0xFF

typedef struct
{
	USBD_Interface_T interface;
	union {
		USBD_Endpoint_T mscEndpoints[2];
		struct {
			USBD_Endpoint_T mscInEP;
			USBD_Endpoint_T mscOutEP;
		};
	};
} USBD_MSC_Interface_T;

void USBD_MSC_DataInterface(USBD_MSC_Interface_T *interface);

#endif
