#include "usbd_msc_bot.h"
#include "usbd_msc_cfg.h"

#define USBD_CONFIGURATION_STRING_MSC "MSC Config"

void USBD_MSC_Configuration(USBD_MSC_Configuration_T *mscConfiguration)
{
	USBD_MSC_DataInterface(&mscConfiguration->mscInterface);
	
	mscConfiguration->mscInterfaces[0] = &mscConfiguration->mscInterface.interface;
		
	USBD_Configuration(
		&mscConfiguration->configuration,
		1,
		USBD_CONFIGURATION_STRING_MSC,
		0xC0,
		0x32,
		1,
		mscConfiguration->mscInterfaces,
		0, 0,
		
		USBD_NO_CONFIGURATION_REBUILD_CALLBACK,
		MSC_BOT_Init,
		MSC_BOT_DeInit,
		USBD_NO_CONFIGURATION_SETUP_CALLBACK,
		USBD_NO_CONFIGURATION_EP0TX_CALLBACK,  
		USBD_NO_CONFIGURATION_EP0RX_CALLBACK,
		USBD_NO_CONFIGURATION_SOF_CALLBACK);
}

