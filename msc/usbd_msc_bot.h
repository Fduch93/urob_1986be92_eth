#ifndef MSC_BOT_H
#define MSC_BOT_H

#include <stdint.h>
#include "scsi.h"

#define MSC_BOT_CBW_SIGNATURE   0x43425355
#define MSC_BOT_CSW_SIGNATURE   0x53425355
#define MSC_BOT_CBW_LENGTH      31
#define MSC_BOT_CSW_LENGTH      13

#define MSC_BOT_STATUS_NORMAL   0
#define MSC_BOT_STATUS_RECOVERY 1
#define MSC_BOT_STATUS_ERROR    2

#define MSC_BLOCKS_PER_PACKET 1

typedef enum uint8_t
{
	MSC_BOT_CMD_PASSED	= 0x00,
	MSC_BOT_CMD_FAILED	= 0x01,
	MSC_BOT_PHASE_ERROR	= 0x02
} MSC_BOT_CommandStatus_T;

typedef union
{
	uint8_t u8[MSC_BOT_CBW_LENGTH];
	struct {
		uint32_t dSignature;
		uint32_t dTag;
		uint32_t dDataLength;
		uint8_t bmFlags;
		uint8_t bLUN;
		uint8_t bCBLength;
		union {
			uint8_t CB[16];
			SCSI_CommandBlock_T commandBlock;
		};
	};
} MSC_BOT_CBW_T;

typedef union
{
	uint8_t u8[MSC_BOT_CSW_LENGTH];
	struct {
		uint32_t dSignature;
		uint32_t dTag;
		uint32_t dDataResidue;
		MSC_BOT_CommandStatus_T  bStatus;
	};
} MSC_BOT_CSW_T;

typedef enum
{
	MSC_BOT_CMD_STATUS_RECEIVED,
	MSC_BOT_CMD_STATUS_FAILED,
	MSC_BOT_CMD_STATUS_PASSED,
	MSC_BOT_CMD_STATUS_SEND_DATA,
	MSC_BOT_CMD_STATUS_RECEIVE_DATA,
	MSC_BOT_CMD_STATUS_ABORT
} MSC_BOT_Result_T;

typedef struct
{
	uint32_t		bot_data_length;
	uint8_t			bot_data[MSC_BLOCKS_PER_PACKET * 512];  
	MSC_BOT_CBW_T	cbw;
	MSC_BOT_CSW_T	csw;
	MSC_BOT_Result_T cmdStatus;

	uint32_t		scsi_blk_addr;
	uint32_t		scsi_blk_len;
	
	SCSI_Sense_T	scsi_sense [SENSE_LIST_DEEPTH];
	uint8_t			scsi_sense_head;
	uint8_t			scsi_sense_tail;
	
	uint8_t			bot_status;
	uint8_t			max_lun;
	
} MSC_BOT_T; 

void MSC_BOT_Init(void);
void MSC_BOT_Reset(void);
void MSC_BOT_DeInit(void);
void MSC_BOT_DataIn(void);
void MSC_BOT_DataOut(void);
void MSC_BOT_CplClrFeature(uint8_t isInEndpoint);

extern MSC_BOT_T *MSC_BOT_GetMscBot(void);

extern void MSC_FlushInEndpoint(void);
extern void MSC_FlushOutEndpoint(void);
extern void MSC_Transmit(uint8_t *buf, uint32_t len);
extern void MSC_Receive(uint8_t *buf, uint32_t len);
extern void MSC_StallInEndpoint(void);
extern void MSC_StallOutEndpoint(void);
extern uint32_t MSC_Rx_Count(void);

#endif
