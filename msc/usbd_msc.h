#ifndef USBD_MSC_H
#define USBD_MSC_H

#include <stdint.h>
 
typedef struct
{
	char (* Init)				(uint8_t lun);
	char (* GetCapacity)		(uint8_t lun, uint32_t *block_num, uint16_t *block_size);
	char (* IsReady)			(uint8_t lun);
	char (* IsWriteProtected)	(uint8_t lun);
	char (* Read)				(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len);
	char (* Write)			(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len);
	char (* GetMaxLun)(void);
	char *pInquiry;
	
} USBD_StorageTypeDef;

#endif
