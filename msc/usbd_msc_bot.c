#include "usbd_msc_bot.h"
#include "msc_bot_scsi.h"

static char MSC_BOT_ReceiveCBW(void);
static void MSC_BOT_ProcessCBW(void);
static void MSC_BOT_SendData(uint8_t* buf, uint16_t len);
static void MSC_BOT_ReceiveData(uint8_t* buf, uint16_t len);
static void MSC_BOT_SendCSW(MSC_BOT_CommandStatus_T CSW_Status);
static void MSC_BOT_Abort(void);

void MSC_BOT_Init()
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	mscBot->bot_status = MSC_BOT_STATUS_NORMAL;
	mscBot->cmdStatus = MSC_BOT_CMD_STATUS_PASSED;
	
	mscBot->csw.dSignature = MSC_BOT_CSW_SIGNATURE;
	
	mscBot->scsi_sense_tail = 0;
	mscBot->scsi_sense_head = 0;
	
	MSC_FlushOutEndpoint();
	MSC_FlushInEndpoint();
	
	/* Prapare EP to Receive First BOT Cmd */
	MSC_Receive(
		(uint8_t *)&(mscBot->cbw),
		MSC_BOT_CBW_LENGTH);    
}

void MSC_BOT_Reset()
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	mscBot->bot_status = MSC_BOT_STATUS_RECOVERY;  
	
	/* Prapare EP to Receive First BOT Cmd */
	MSC_Receive(
		(uint8_t *)&mscBot->cbw,
		MSC_BOT_CBW_LENGTH);   
}

void MSC_BOT_DeInit()
{
	// TODO:
}

#include "log.h"

#define MSC_BOT_LOG(...)
void MSC_BOT_DataIn()
{
	MSC_BOT_LOG("MSC_BOT_DataIn, cmdStatus == %d", MSC_BOT_GetMscBot()->cmdStatus);
	switch (MSC_BOT_GetMscBot()->cmdStatus)
	{
		case MSC_BOT_CMD_STATUS_PASSED:
		case MSC_BOT_CMD_STATUS_FAILED:
			MSC_Receive(
				(uint8_t *)&MSC_BOT_GetMscBot()->cbw, 
				MSC_BOT_CBW_LENGTH);
		case MSC_BOT_CMD_STATUS_ABORT:
			break;
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			MSC_BOT_ProcessCBW();
			break;
		case MSC_BOT_CMD_STATUS_RECEIVE_DATA:
			MSC_BOT_SendCSW(MSC_BOT_PHASE_ERROR);
			break;
		default:
			break;
	}
}

void MSC_BOT_DataOut()
{
	MSC_BOT_LOG("MSC_BOT_DataOut, cmdStatus == %d", MSC_BOT_GetMscBot()->cmdStatus);
	switch (MSC_BOT_GetMscBot()->cmdStatus)
	{
		case MSC_BOT_CMD_STATUS_PASSED:
		case MSC_BOT_CMD_STATUS_FAILED:
		case MSC_BOT_CMD_STATUS_ABORT:
			if (MSC_BOT_ReceiveCBW())
				MSC_BOT_ProcessCBW();
			break;
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			MSC_BOT_SendCSW(MSC_BOT_PHASE_ERROR);
			break;
		case MSC_BOT_CMD_STATUS_RECEIVE_DATA:
			MSC_BOT_ProcessCBW();
			break;
		default:
			break;
	}
}

static char MSC_BOT_ReceiveCBW()
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	mscBot->csw.dTag = mscBot->cbw.dTag;
	mscBot->csw.dDataResidue = mscBot->cbw.dDataLength;
	
	if ((MSC_Rx_Count() == MSC_BOT_CBW_LENGTH) &&
		(mscBot->cbw.dSignature == MSC_BOT_CBW_SIGNATURE) &&
		(mscBot->cbw.bLUN == 0) &&
		(mscBot->cbw.bCBLength > 0) &&
		(mscBot->cbw.bCBLength <= 16))
	{
		mscBot->cmdStatus = MSC_BOT_CMD_STATUS_RECEIVED;
		return 1;
	}
	else
	{
		MSC_BOT_Abort();
		return 0;
	}
}

static void MSC_BOT_ProcessCBW()
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();

	mscBot->cmdStatus = BOT_ProcessScsiCmd(
		&mscBot->cbw,
		mscBot->cmdStatus,
		mscBot->bot_data,
		&mscBot->bot_data_length);
	
	switch (mscBot->cmdStatus)
	{
		case MSC_BOT_CMD_STATUS_PASSED:
			MSC_BOT_LOG("MSC_BOT_SendCSW PASSED");
			MSC_BOT_SendCSW(MSC_BOT_CMD_PASSED);
			break;
		case MSC_BOT_CMD_STATUS_FAILED:
			MSC_BOT_LOG("MSC_BOT_SendCSW FAILED");
			MSC_BOT_SendCSW(MSC_BOT_CMD_FAILED);
			break;
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			MSC_BOT_LOG("MSC_BOT_SendData: %d", mscBot->bot_data_length);
			MSC_BOT_SendData(mscBot->bot_data, mscBot->bot_data_length);
			break;
		case MSC_BOT_CMD_STATUS_RECEIVE_DATA:
			MSC_BOT_LOG("MSC_BOT_ReceiveData: %d", mscBot->bot_data_length);
			MSC_BOT_ReceiveData(mscBot->bot_data, mscBot->bot_data_length);
			break;
		case MSC_BOT_CMD_STATUS_ABORT:
			MSC_BOT_LOG("MSC_BOT_Abort");
			MSC_BOT_Abort();
			break;
		default:
			break;
	}
}

static void MSC_BOT_SendData(uint8_t* buf, uint16_t len)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	len = mscBot->cbw.dDataLength < len
		? mscBot->cbw.dDataLength
		: len;
	mscBot->csw.dDataResidue -= len;
	mscBot->csw.bStatus = MSC_BOT_CMD_PASSED;
	
	MSC_Transmit(buf, len);
}

static void MSC_BOT_ReceiveData(uint8_t* buf, uint16_t len)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	len = mscBot->cbw.dDataLength < len
		? mscBot->cbw.dDataLength
		: len;
	mscBot->csw.dDataResidue -= len;
	MSC_Receive(buf, len);		
}

static void MSC_BOT_SendCSW(MSC_BOT_CommandStatus_T CSW_Status)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	mscBot->csw.bStatus = CSW_Status;
	
	MSC_Transmit(
		(uint8_t *)&mscBot->csw, 
		MSC_BOT_CSW_LENGTH);
}

static void MSC_BOT_Abort()
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	if ((mscBot->cbw.bmFlags == 0) && 
		(mscBot->cbw.dDataLength != 0) &&
		(mscBot->bot_status == MSC_BOT_STATUS_NORMAL))
	{
		MSC_StallOutEndpoint();
	}
	
	MSC_StallInEndpoint();
	
	if (mscBot->bot_status == MSC_BOT_STATUS_ERROR)
		MSC_Receive(
			(uint8_t *)&mscBot->cbw, 
			MSC_BOT_CBW_LENGTH);
}

void MSC_BOT_CplClrFeature(uint8_t isInEndpoint)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	/* Bad CBW Signature */
	if (mscBot->bot_status == MSC_BOT_STATUS_ERROR)
	{
		MSC_StallInEndpoint();
		mscBot->bot_status = MSC_BOT_STATUS_NORMAL;    
	}
	else if (isInEndpoint && (mscBot->bot_status != MSC_BOT_STATUS_RECOVERY))
	{
		MSC_BOT_SendCSW(MSC_BOT_CMD_FAILED);
	}
}
