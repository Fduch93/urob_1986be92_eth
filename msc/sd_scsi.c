#include "sd_scsi.h"
#include "FAT_hal.h"

static char SWAP_GetCapacity		(uint8_t lun, uint32_t *blockCnt, uint32_t *bytesPerBlock);
static char SWAP_IsReady			(uint8_t lun);
static char SWAP_IsWriteProtected	(uint8_t lun);
static char SWAP_Read				(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt);
static char SWAP_Write				(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt);
static uint16_t SWAP_GetInquirySize	(uint8_t lun);
static const SCSI_InquiryResponse_T *SWAP_GetInquiry(uint8_t lun);
static uint16_t SWAP_GetVPDPageSize(uint8_t lun, uint8_t pageCode);
static const uint8_t *SWAP_GetVPDPage(uint8_t lun, uint8_t pageCode);
static const uint8_t *SWAP_GetModeSense6(void);
static const uint8_t *SWAP_GetModeSense10(void);

static const SCSI_Drive_T SWAP_ScsiDrive = {
	SWAP_GetCapacity,
	SWAP_IsReady,
	SWAP_IsWriteProtected,
	SWAP_Read,
	SWAP_Write,
	SWAP_GetInquirySize,
	SWAP_GetInquiry,
	SWAP_GetVPDPageSize,
	SWAP_GetVPDPage,
	SWAP_GetModeSense6,
	SWAP_GetModeSense10,
};

const SCSI_Drive_T *BOT_GetDrive(void)
{
	return &SWAP_ScsiDrive;
}

static const SCSI_InquiryResponse_T SWAP_ScsiInquiry = {
	.periphDevType = 0,
	.periphQualifier = 0,
	.isRemovable = 1,
	.spcVer = 0x02,
	.respDataFormat = 2,
	.hierarchicalSupport = 0,
	.normalACASupported = 0,
	.additionalLen = (sizeof(SCSI_InquiryResponse_T) - 5),
	.zero = {0,0,0},
	.vendor		= "MDR32   ",			/* Manufacturer : 8 bytes */
	.product	= "RAM Storage     ",	/* Product      : 16 Bytes */
	.revision	= "0.01",				/* Version      : 4 Bytes */
//	{0xAB, 0xCD, 0x12, 0x34}
};

static const uint8_t SWAP_ScsiVPD0[] = {					
	0x00,		
	0x00, 
	0x00, 
	(7 - 4),
	0x00, 
	0x80, 
	0x83 
};

static const uint8_t SWAP_ScsiModeSense6[] = {	0,0,0,0,0,0,0,0};	
static const uint8_t SWAP_ScsiModeSense10[] = { 0,6,0,0,0,0,0,0 };

static char SWAP_GetCapacity(uint8_t lun, uint32_t *blockCnt, uint32_t *bytesPerBlock)
{
	*blockCnt  = 0x00FFFFFF;// 2*1024;//SD_GetSectorConut();
	*bytesPerBlock = 512;
	return 1;
}

static char SWAP_IsReady(uint8_t lun)
{
	// TODO: implement ready check
	return 1;
}

static char SWAP_IsWriteProtected(uint8_t lun)
{
	// TODO: implement write protection check
	return 0;
}

#include <string.h>
static char SWAP_Read(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt)
{
	uint8_t *pSect = FAT_GetSectorBuffer();
	for (uint32_t i = block; i < block+blockCnt; i++)
	{
		FAT_ReadSector(i);
		memcpy(buf, pSect, 512);
		buf += 512;
	}
	return 1;
}

static char SWAP_Write(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt)
{
	uint8_t *pSect = FAT_GetSectorBuffer();
	for (uint32_t i = block; i < block+blockCnt; i++)
	{
		memcpy(pSect, buf, 512);
		FAT_WriteSector(i);
		buf += 512;
	}
	return 1;
}

static uint16_t SWAP_GetInquirySize(uint8_t lun)
{
	return sizeof(SCSI_InquiryResponse_T);
}

static const SCSI_InquiryResponse_T *SWAP_GetInquiry(uint8_t lun)
{
	return &SWAP_ScsiInquiry;
}

static uint16_t SWAP_GetVPDPageSize(uint8_t lun, uint8_t pageCode)
{
	return sizeof(SWAP_ScsiVPD0);
}

static const uint8_t *SWAP_GetVPDPage(uint8_t lun, uint8_t pageCode)
{
	return SWAP_ScsiVPD0;
}

static const uint8_t *SWAP_GetModeSense6(void)
{
	return SWAP_ScsiModeSense6;
}

static const uint8_t *SWAP_GetModeSense10(void)
{
	return SWAP_ScsiModeSense10;
}
