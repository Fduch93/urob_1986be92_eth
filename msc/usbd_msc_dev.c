#include "usbd_msc_dev.h"

#define USBD_VID_MSC						0x0483
#define USBD_PID_MSC						0x5720
#define USBD_LANGID_MSC						1033
#define USBD_MANUFACTURER_STRING_MSC		"STMicroelectronics"
#define USBD_PRODUCT_STRING_MSC				"STM32 Mass Storage"
#define USBD_SERIALNUMBER_STRING_MSC		"00000000001A"

#define USB_MAX_PACKET_SIZE					64

void USBD_MSC_Device(USBD_MSC_Device_T *mscDevice)
{
	USBD_MSC_Configuration(&mscDevice->mscConfiguration);

	USBD_Device(
		&mscDevice->device,
		USB_VER_1_1,
		DEV_CLASS_BY_INTERFACE,
		DEV_SUBCLASS_BY_INTERFACE,
		DEV_PROTOCOL_BY_INTERFACE,
		USB_MAX_PACKET_SIZE,
		USBD_VID_MSC,
		USBD_PID_MSC,
		DEV_VER_2_0,
		USB_LANG_EN,
		USBD_MANUFACTURER_STRING_MSC,
		USBD_PRODUCT_STRING_MSC,
		USBD_SERIALNUMBER_STRING_MSC,
		&mscDevice->mscConfiguration.configuration,
		USBD_NO_DEVICE_CALLBACKS);
}

MSC_BOT_T *MSC_BOT_GetMscBot()
{
	return &MSC_GetDevice()->mscBot;
}

#include "usbd_hal.h"

void MSC_FlushInEndpoint()
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscInEP = &mscDevice->mscConfiguration.mscInterface.mscInEP;
	USBD_HAL_FlushEndpoint(mscInEP);
}

void MSC_FlushOutEndpoint()
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscOutEP = &mscDevice->mscConfiguration.mscInterface.mscOutEP;
	USBD_HAL_FlushEndpoint(mscOutEP);
}

void MSC_Transmit(uint8_t *buf, uint32_t len)
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscInEP = &mscDevice->mscConfiguration.mscInterface.mscInEP;
	USBD_HAL_EndpointTransmit(mscInEP, buf, len);
}

void MSC_Receive(uint8_t *buf, uint32_t len)
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscOutEP = &mscDevice->mscConfiguration.mscInterface.mscOutEP;
	USBD_HAL_EndpointReceive(mscOutEP, buf, len);
}

void MSC_StallInEndpoint()
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscInEP = &mscDevice->mscConfiguration.mscInterface.mscInEP;
	USBD_HAL_StallEndpoint(mscInEP);
}

void MSC_StallOutEndpoint()
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscOutEP = &mscDevice->mscConfiguration.mscInterface.mscOutEP;
	USBD_HAL_StallEndpoint(mscOutEP);
}

uint32_t MSC_Rx_Count()
{
	USBD_MSC_Device_T *mscDevice = MSC_GetDevice();
	USBD_Endpoint_T *mscOutEP = &mscDevice->mscConfiguration.mscInterface.mscOutEP;
	return MSC_BOT_CBW_LENGTH - mscOutEP->bufferLen;
}
