#ifndef USBD_MSC_DEV_H
#define USBD_MSC_DEV_H

#include "usbd_device.h"
#include "usbd_msc_cfg.h"
#include "usbd_msc_bot.h"

typedef struct
{
	USBD_Device_T device;
	USBD_MSC_Configuration_T mscConfiguration;
	MSC_BOT_T mscBot;
} USBD_MSC_Device_T;

void USBD_MSC_Device(USBD_MSC_Device_T *mscDevice);

extern USBD_MSC_Device_T *MSC_GetDevice(void);

#endif
