#ifndef USBD_MSC_CFG_H
#define USBD_MSC_CFG_H

#include "usbd_configuration.h"
#include "usbd_msc_data_itf.h"

typedef struct
{
	USBD_Configuration_T configuration;
	USBD_Interface_T *mscInterfaces[1];
	USBD_MSC_Interface_T mscInterface;
} USBD_MSC_Configuration_T;

void USBD_MSC_Configuration(USBD_MSC_Configuration_T *mscConfiguration);

#endif
