#ifndef MSC_BOT_SCSI_H
#define MSC_BOT_SCSI_H

#include <stdint.h>

#include "usbd_msc_bot.h"

MSC_BOT_Result_T BOT_ProcessScsiCmd(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength);

#endif
