#ifndef SCSI_H
#define SCSI_H

#include <stdint.h>

#define SENSE_LIST_DEEPTH				4

/* SCSI Commands */
#define SCSI_FORMAT_UNIT				0x04
#define SCSI_INQUIRY					0x12
#define SCSI_MODE_SELECT6				0x15
#define SCSI_MODE_SELECT10				0x55
#define SCSI_MODE_SENSE6				0x1A
#define SCSI_MODE_SENSE10				0x5A
#define SCSI_ALLOW_MEDIUM_REMOVAL		0x1E
#define SCSI_READ6						0x08
#define SCSI_READ10						0x28
#define SCSI_READ12						0xA8
#define SCSI_READ16						0x88

#define SCSI_READ_CAPACITY10			0x25
#define SCSI_READ_CAPACITY16			0x9E

#define SCSI_REQUEST_SENSE				0x03
#define SCSI_START_STOP_UNIT			0x1B
#define SCSI_TEST_UNIT_READY			0x00
#define SCSI_WRITE6						0x0A
#define SCSI_WRITE10					0x2A
#define SCSI_WRITE12					0xAA
#define SCSI_WRITE16					0x8A

#define SCSI_VERIFY10					0x2F
#define SCSI_VERIFY12					0xAF
#define SCSI_VERIFY16					0x8F

#define SCSI_SEND_DIAGNOSTIC			0x1D
#define SCSI_READ_FORMAT_CAPACITIES		0x23

#define NO_SENSE						0
#define RECOVERED_ERROR					1
#define NOT_READY						2
#define MEDIUM_ERROR					3
#define HARDWARE_ERROR					4
#define ILLEGAL_REQUEST					5
#define UNIT_ATTENTION					6
#define DATA_PROTECT					7
#define BLANK_CHECK						8
#define VENDOR_SPECIFIC					9
#define COPY_ABORTED					10
#define ABORTED_COMMAND					11
#define VOLUME_OVERFLOW					13
#define MISCOMPARE						14

#define INVALID_CDB						0x20
#define INVALID_FIELED_IN_COMMAND		0x24
#define PARAMETER_LIST_LENGTH_ERROR		0x1A
#define INVALID_FIELD_IN_PARAMETER_LIST	0x26
#define ADDRESS_OUT_OF_RANGE			0x21
#define MEDIUM_NOT_PRESENT				0x3A
#define MEDIUM_HAVE_CHANGED				0x28
#define WRITE_PROTECTED					0x27 
#define UNRECOVERED_READ_ERROR			0x11
#define WRITE_FAULT						0x03 

#define READ_FORMAT_CAPACITY_DATA_LEN	0x0C
#define READ_CAPACITY10_DATA_LEN		0x08
#define MODE_SENSE10_DATA_LEN			0x08
#define MODE_SENSE6_DATA_LEN			0x04
#define REQUEST_SENSE_DATA_LEN			0x12
#define STANDARD_INQUIRY_DATA_LEN		0x24
#define BLKVFY							0x04

extern uint8_t Page00_Inquiry_Data[];
extern uint8_t Standard_Inquiry_Data[];
extern uint8_t Standard_Inquiry_Data2[];
extern uint8_t Mode_Sense6_data[];
extern uint8_t Mode_Sense10_data[];
extern uint8_t Scsi_Sense_Data[];
extern uint8_t ReadCapacity10_Data[];
extern uint8_t ReadFormatCapacity_Data [];

typedef struct
{
	uint8_t senseKey;
	union {
		unsigned int uASC;
		char *pData;
		struct {
			char ASC;
			char ASCQ;
		};
	};
} SCSI_Sense_T;

typedef struct
{
	struct {
		uint8_t infoValid:1;
		uint8_t responseCode:7;
	};
	uint8_t zero1;
	uint8_t senseKey;
	uint8_t info[4];
	uint8_t additionalSenseLen;
	uint8_t commandSpecific[4];
	uint8_t additionalSenseCode;
	uint8_t additionalSenseCodeQualifier;
} SCSI_SenseResponse_T;

typedef	union
{
	uint8_t u8;
	struct {
		uint8_t groupCode:3;
		uint8_t cmdCode:5;
	};
} SCSI_OperationCode_T;

typedef	struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t epvd:1; // enable vital product data
		uint8_t :4;
		uint8_t lun:3;
	};
	uint8_t pageCode;
	uint8_t allocLen[2];
	uint8_t zero3;
} SCSI_InquiryCDB_T;

typedef struct
{
	struct {
		uint8_t periphDevType:5;
		uint8_t periphQualifier:3;
	};
	struct {
		uint8_t :7;
		uint8_t isRemovable:1;
	};
	uint8_t spcVer;
	struct {
		uint8_t respDataFormat:4; // = 2
		uint8_t hierarchicalSupport:1;
		uint8_t normalACASupported:1;
		uint8_t :2;
	};
	uint8_t additionalLen;
	uint8_t zero[3];
	uint8_t vendor[8];
	uint8_t product[16];
	uint8_t revision[4];
	//uint8_t serialNumber[8];	
} SCSI_InquiryResponse_T;

typedef	struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t :3;
		uint8_t disableBlockDescriptors:1;
		uint8_t :4;
	};
	struct {
		uint8_t pageCode:6;
		uint8_t pageControl:2;
	};
	uint8_t subpageCode;
	uint8_t allocLen;
	uint8_t zero3;
} SCSI_ModeSense6CDB_T;

typedef struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t desc:1; // descriptor format
		uint8_t :4;
		uint8_t lun:3;
	};
	uint8_t zero1[2];
	uint8_t allocLen;
	uint8_t zero2[1];		
} SCSI_RequestSenseCDB_T;

typedef struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t :5;
		uint8_t lun:3;
	};
	uint8_t zero[4];
} SCSI_TestUnitReadyCDB_T;

typedef struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t :5;
		uint8_t lun:3;
	};
	uint8_t zero[8];
} SCSI_ReadCapacity10CDB_T;

typedef struct
{
	SCSI_OperationCode_T opCode;
	struct {
		uint8_t :2;
		uint8_t rebuildAssistRecoveryControl:1;
		uint8_t forceUnitAccess:1;
		uint8_t disablePageOut:1;
		uint8_t readProtect:3;
	};
	uint8_t blockAddress[4];
	struct {
		uint8_t groupNumber:5;
		uint8_t :3;
	};
	uint8_t xferLen[2];
	uint8_t zero[1];
} SCSI_Read10CDB_T, SCSI_Write10CDB_T;

typedef struct
{
	uint8_t blockCnt[4];
	uint8_t bytesPerBlock[4];
} SCSI_ReadCapacity10Resp_T;

typedef union
{
	uint8_t u8[16];
	SCSI_OperationCode_T opCode;
	SCSI_RequestSenseCDB_T requestSense;
	SCSI_InquiryCDB_T inquiry;
	SCSI_TestUnitReadyCDB_T testUnitReady;
	SCSI_ReadCapacity10CDB_T capacity;
	SCSI_Read10CDB_T read;
	SCSI_Write10CDB_T write;
} SCSI_CommandBlock_T;

typedef struct
{
	char (*GetCapacity)			(uint8_t lun, uint32_t *blockCnt, uint32_t *bytesPerBlock);
	char (*IsReady)				(uint8_t lun);
	char (*IsWriteProtected)	(uint8_t lun);
	char (*Read)				(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt);
	char (*Write)				(uint8_t lun, uint8_t *buf, uint32_t block, uint32_t blockCnt);
	uint16_t (*GetInquirySize)(uint8_t lun);
	const SCSI_InquiryResponse_T *(*GetInquiry)(uint8_t lun);
	uint16_t (*GetVPDPageSize)(uint8_t lun, uint8_t pageCode);
	const uint8_t *(*GetVPDPage)(uint8_t lun, uint8_t pageCode);
	const uint8_t *(*GetModeSense6)(void);
	const uint8_t *(*GetModeSense10)(void);
	
//	SCSI_Sense_T	senseBuffer[SENSE_LIST_DEEPTH];
//	uint8_t			senseBufferHead;
//	uint8_t			senseBufferTail;
	
} SCSI_Drive_T;

void SCSI_SenseCode(const SCSI_Drive_T *drive, uint8_t senseKey, uint8_t ASC, uint8_t ASCQ);

char SCSI_TestUnitReady(
	const SCSI_Drive_T *drive,
	SCSI_TestUnitReadyCDB_T *commandBlock);
char SCSI_RequestSense(
	const SCSI_Drive_T *drive,
	SCSI_RequestSenseCDB_T *reqSenseCDB,
	SCSI_SenseResponse_T *respSense);
uint16_t SCSI_GetInquirySize(
	const SCSI_Drive_T *drive,
	SCSI_InquiryCDB_T *reqInquiryCDB);
char SCSI_GetInquiry(
	const SCSI_Drive_T *drive,
	SCSI_InquiryCDB_T *reqInquiryCDB,
	uint8_t *respBuffer);
char SCSI_ReadCapacity10(
	const SCSI_Drive_T *drive,
	SCSI_ReadCapacity10CDB_T *readCapacityCDB,
	SCSI_ReadCapacity10Resp_T *readCapacityResp);
char SCSI_Read10(
	const SCSI_Drive_T *drive,
	SCSI_Read10CDB_T *read10CDB,
	uint8_t *readBuffer);
char SCSI_Write10(
	const SCSI_Drive_T *drive,
	SCSI_Read10CDB_T *write10CDB,
	uint8_t *writeBuffer);

#endif
