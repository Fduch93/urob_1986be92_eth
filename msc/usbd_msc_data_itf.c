#include "usbd_msc_data_itf.h"
#include "usbd_msc_bot.h"

#define USBD_INTERFACE_STRING_MSC "MSC Interface"
#define MSC_DATA_PACKET_SIZE 64

static void MSC_DataInterfaceSetup(USB_SetupReq_T *req, USB_SetupReqResponse_T *response);

void USBD_MSC_DataInterface(USBD_MSC_Interface_T *mscInterface)
{
	USBD_Endpoint(
		&mscInterface->mscInEP,
		ENDPOINT_IN,
		EP_TYPE_BULK,
		MSC_DATA_PACKET_SIZE,
		NO_POLLING_INTERVAL,
		USBD_NO_ENDPOINT_SETUP,
		MSC_BOT_DataIn,
		USBD_NO_ENDPOINT_DATA_OUT);

	USBD_Endpoint(
		&mscInterface->mscOutEP,
		ENDPOINT_OUT,
		EP_TYPE_BULK,
		MSC_DATA_PACKET_SIZE,
		NO_POLLING_INTERVAL,
		USBD_NO_ENDPOINT_SETUP,
		USBD_NO_ENDPOINT_DATA_IN,
		MSC_BOT_DataOut);

	USBD_Interface(
		&mscInterface->interface,
		USB_INTERFACE_NO_ALTERNATIVE,
		0x08,
		0x06,
		0x50,
		USBD_INTERFACE_STRING_MSC,
		0, 0,
		2,
		mscInterface->mscEndpoints,
		MSC_DataInterfaceSetup);
}

static void MSC_DataInterfaceSetup(USB_SetupReq_T *req, USB_SetupReqResponse_T *response)
{
	switch (req->request)
	{
		case BOT_GET_MAX_LUN:

			if (req->value  == 0 &&
				req->length == 1 &&
				req->direction == USB_REQ_DIR_TO_HOST)
			{
				MSC_BOT_GetMscBot()->max_lun = 0;
				response->operation = USBD_REQ_OP_SEND_DATA;
				response->data = (uint8_t *)&MSC_BOT_GetMscBot()->max_lun;
				response->len = req->length;
			}
			else
			{
				response->operation = USBD_REQ_OP_ERROR;
			}
			break;

		case BOT_RESET:
			if (req->value  == 0 &&
				req->length == 0 &&
				req->direction == USB_REQ_DIR_TO_DEVICE)
			{
				MSC_BOT_Reset();
			}
			else
			{
				response->operation = USBD_REQ_OP_ERROR;
			}
			break;

		default:
			response->operation = USBD_REQ_OP_ERROR;

		/* Interface & Endpoint request
		case USB_REQ_TYPE_STANDARD:
			switch (req->request)
			{
			case USB_REQ_CLEAR_FEATURE:
				if ((uint8_t)req->index & ENDPOINT_IN)
				{
					MSC_FlushInEndpoint();
					MSC_Close_In_Endpoint();
					MSC_Open_In_Endpoint();
				}
				else
				{
					MSC_FlushOutEndpoint();
					MSC_Close_Out_Endpoint();
					MSC_Open_Out_Endpoint();
				}

				// Handle BOT error
				MSC_BOT_CplClrFeature((uint8_t)req->index & ENDPOINT_IN);
				break;
			} */
	}
}
