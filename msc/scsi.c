#include "scsi.h"
#include <string.h>

void SCSI_SenseCode(const SCSI_Drive_T *drive, uint8_t senseKey, uint8_t ASC, uint8_t ASCQ)
{
	// TODO: maybe implement separate buffers for different luns
//	drive->senseBuffer[drive->senseBufferTail].senseKey  = senseKey;
//	drive->senseBuffer[drive->senseBufferTail].ASC = ASC;
//	drive->senseBuffer[drive->senseBufferTail].ASCQ = ASCQ;
//	drive->senseBufferTail++;
//	if (drive->senseBufferTail == SENSE_LIST_DEEPTH)
//		drive->senseBufferTail = 0;
}

char SCSI_TestUnitReady(const SCSI_Drive_T *drive, SCSI_TestUnitReadyCDB_T *commandBlock)
{
	if (drive->IsReady(commandBlock->lun))
	{
		return 1;
	}
	else
	{
		SCSI_SenseCode(drive, NOT_READY, MEDIUM_NOT_PRESENT, 0);
		return 0;
	}
}

char SCSI_RequestSense(
	const SCSI_Drive_T *drive,
	SCSI_RequestSenseCDB_T *reqSenseCDB,
	SCSI_SenseResponse_T *respSense)
{
	if (reqSenseCDB->desc)
		// TODO: find out what to do, maybe see seagate scsi specification some more
		return 0;

	if (reqSenseCDB->allocLen != 13)
		// TODO: find out what to do, maybe implement different response formats
		return 0;
	
	respSense->zero1 = 0;
	memset(respSense->info, 0, 4);
	memset(respSense->commandSpecific, 0, 4);
	
	respSense->infoValid = 0;
	respSense->responseCode = 0x70;	
	respSense->additionalSenseLen = REQUEST_SENSE_DATA_LEN - 6;	
	
//	if ((drive->senseBufferHead != drive->senseBufferTail))
//	{		
//		respSense->senseKey = drive->senseBuffer[drive->senseBufferHead].senseKey;
//		if (drive->IsReady(reqSenseCDB->lun))
//		{
//			respSense->additionalSenseCode = 4;//drive->senseBuffer[drive->senseBufferHead].ASC;
//			respSense->additionalSenseCodeQualifier = 1;//drive->senseBuffer[drive->senseBufferHead].ASCQ;
//		}
//		else
//		{
//			respSense->additionalSenseCode = 0;//drive->senseBuffer[drive->senseBufferHead].ASC;
//			respSense->additionalSenseCodeQualifier = 0;//drive->senseBuffer[drive->senseBufferHead].ASCQ;
//		}
//		drive->senseBufferHead++;
//		
//		if (drive->senseBufferHead == SENSE_LIST_DEEPTH)
//			drive->senseBufferHead = 0;
//	}
		
	return 1;
}

uint16_t SCSI_GetInquirySize(
	const SCSI_Drive_T *drive,
	SCSI_InquiryCDB_T *reqInquiryCDB)
{
	uint16_t allocLen = reqInquiryCDB->allocLen[0] << 8 | reqInquiryCDB->allocLen[1];
	uint16_t len = 0;
	
	if (reqInquiryCDB->epvd)
		len = drive->GetVPDPageSize(reqInquiryCDB->lun, reqInquiryCDB->pageCode);
	else
		len = drive->GetInquirySize(reqInquiryCDB->lun);
	
	if (len < allocLen)
		return len;
	else
		return allocLen;
}

char SCSI_GetInquiry(
	const SCSI_Drive_T *drive,
	SCSI_InquiryCDB_T *reqInquiryCDB,
	uint8_t *respBuffer)
{
	uint16_t len = SCSI_GetInquirySize(drive, reqInquiryCDB);
	uint16_t allocLen = reqInquiryCDB->allocLen[0] << 8 | reqInquiryCDB->allocLen[1];
	
	if (len > allocLen)
		len = allocLen;
	
	if (reqInquiryCDB->epvd)
		memcpy(respBuffer, drive->GetVPDPage(reqInquiryCDB->lun, reqInquiryCDB->pageCode), len);
	else
		// fixme i'm replying with supported page list to a s/n request
		memcpy(respBuffer, drive->GetInquiry(reqInquiryCDB->lun), len);

	return 1;
}

char SCSI_ReadCapacity10(
	const SCSI_Drive_T *drive,
	SCSI_ReadCapacity10CDB_T *readCapacityCDB,
	SCSI_ReadCapacity10Resp_T *readCapacityResp)
{
	uint32_t blockCnt = 0;
	uint32_t bytesPerBlock = 0;
	
	if (!drive->GetCapacity(readCapacityCDB->lun, &blockCnt, &bytesPerBlock))
	{
		SCSI_SenseCode(drive, NOT_READY, MEDIUM_NOT_PRESENT, 0);
		return 0;
	} 
	else
	{
		readCapacityResp->blockCnt[0] = (blockCnt & 0xFF000000) >> 24;
		readCapacityResp->blockCnt[1] = (blockCnt & 0x00FF0000) >> 16;
		readCapacityResp->blockCnt[2] = (blockCnt & 0x0000FF00) >> 8;
		readCapacityResp->blockCnt[3] = (blockCnt & 0x000000FF) >> 0;
		
		readCapacityResp->bytesPerBlock[0] = (bytesPerBlock & 0xFF000000) >> 24;
		readCapacityResp->bytesPerBlock[1] = (bytesPerBlock & 0x00FF0000) >> 16;
		readCapacityResp->bytesPerBlock[2] = (bytesPerBlock & 0x0000FF00) >> 8;
		readCapacityResp->bytesPerBlock[3] = (bytesPerBlock & 0x000000FF) >> 0;
		return 1;
	}
}

char SCSI_Read10(
	const SCSI_Drive_T *drive,
	SCSI_Read10CDB_T *read10CDB,
	uint8_t *readBuffer)
{
	uint32_t blockAddr	= (read10CDB->blockAddress[0] << 24)
						| (read10CDB->blockAddress[1] << 16)
						| (read10CDB->blockAddress[2] << 8)
						| (read10CDB->blockAddress[3] << 0);
	
	uint16_t blockCnt	= (read10CDB->xferLen[0] << 8)
						| (read10CDB->xferLen[1] << 0);

// TODO: check address range
//	if (!SCSI_CheckAddressRange(lun, blockAddr, blockCnt))
//		return 0; /* error */
	
	return drive->Read(0, readBuffer, blockAddr, blockCnt);
}

char SCSI_Write10(
	const SCSI_Drive_T *drive,
	SCSI_Write10CDB_T *write10CDB,
	uint8_t *writeBuffer)
{
	uint32_t blockAddr	= (write10CDB->blockAddress[0] << 24)
						| (write10CDB->blockAddress[1] << 16)
						| (write10CDB->blockAddress[2] << 8)
						| (write10CDB->blockAddress[3] << 0);

	uint32_t blockCnt	= (write10CDB->xferLen[0] << 8)
						| (write10CDB->xferLen[1] << 0);
	
// TODO: check address range
//	if (!SCSI_CheckAddressRange(lun, blockAddr, blockCnt))
//		return 0; /* error */
	
	return drive->Write(0, writeBuffer, blockAddr, blockCnt);
}

