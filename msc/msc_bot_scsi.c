#include "usbd_msc_bot.h"
#include "scsi.h"
#include "sd_scsi.h"

#include "log.h"
#define BOT_SCSI_LOG(...)

#include <string.h>

#define BOT_SCSI_ARGUMENTS			\
	MSC_BOT_CBW_T *cbw, 			\
	MSC_BOT_Result_T lastCmdStatus,	\
	uint8_t *dataBuffer,			\
	uint32_t *dataLength
	
static MSC_BOT_Result_T BOT_SCSI_TestUnitReady(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_RequestSense(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_Inquiry(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_ReadFormatCapacities(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_ReadCapacity10(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_Read10(BOT_SCSI_ARGUMENTS);
static MSC_BOT_Result_T BOT_SCSI_Write10(BOT_SCSI_ARGUMENTS);
	
MSC_BOT_Result_T BOT_ProcessScsiCmd(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (cbw->commandBlock.opCode.u8)
	{
		case SCSI_TEST_UNIT_READY:
			BOT_SCSI_LOG("BOT_SCSI_TestUnitReady");
			return BOT_SCSI_TestUnitReady(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_REQUEST_SENSE:
			BOT_SCSI_LOG("BOT_SCSI_RequestSense");
			return BOT_SCSI_RequestSense(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_INQUIRY:
			BOT_SCSI_LOG("BOT_SCSI_Inquiry");
			return BOT_SCSI_Inquiry(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_READ_FORMAT_CAPACITIES:
			BOT_SCSI_LOG("BOT_SCSI_ReadFormatCapacities");
			return BOT_SCSI_ReadFormatCapacities(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_READ_CAPACITY10:
			BOT_SCSI_LOG("BOT_SCSI_ReadCapacity10");
			return BOT_SCSI_ReadCapacity10(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_READ10:
			BOT_SCSI_LOG("BOT_SCSI_Read10");
			return BOT_SCSI_Read10(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_WRITE10:
			BOT_SCSI_LOG("BOT_SCSI_Write10");
			return BOT_SCSI_Write10(cbw, lastCmdStatus, dataBuffer, dataLength);
		case SCSI_MODE_SENSE6:
		{
			BOT_SCSI_LOG("BOT_SCSI_ModeSense6");
			switch (lastCmdStatus)
			{
				case MSC_BOT_CMD_STATUS_RECEIVED:
					memcpy(dataBuffer, BOT_GetDrive()->GetModeSense6(), 8);
					*dataLength = 8;
					
					return MSC_BOT_CMD_STATUS_SEND_DATA;
					
				case MSC_BOT_CMD_STATUS_SEND_DATA:
					return MSC_BOT_CMD_STATUS_PASSED;
				
				default:
					return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
		
		case SCSI_MODE_SENSE10:
		{
			BOT_SCSI_LOG("BOT_SCSI_ModeSense10");
			switch (lastCmdStatus)
			{
				case MSC_BOT_CMD_STATUS_RECEIVED:
					memcpy(dataBuffer, BOT_GetDrive()->GetModeSense10(), 8);
					*dataLength = 8;
					
					return MSC_BOT_CMD_STATUS_SEND_DATA;
				
				case MSC_BOT_CMD_STATUS_SEND_DATA:
					return MSC_BOT_CMD_STATUS_PASSED;
				
				default:
					return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
		
		case SCSI_START_STOP_UNIT:
		case SCSI_ALLOW_MEDIUM_REMOVAL:
		case SCSI_VERIFY10:
			BOT_SCSI_LOG("BOT_SCSI_UNIMPLEMENTED");
			return MSC_BOT_CMD_STATUS_PASSED;
			
		default:
			BOT_SCSI_LOG("BOT_SCSI_UNKNOWN");
			return MSC_BOT_CMD_STATUS_FAILED;
	}
}

static MSC_BOT_Result_T BOT_SCSI_TestUnitReady(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			if (cbw->bLUN != cbw->commandBlock.testUnitReady.lun)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (SCSI_TestUnitReady(BOT_GetDrive(), &cbw->commandBlock.testUnitReady))
			{
				return MSC_BOT_CMD_STATUS_PASSED;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
		}
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_RequestSense(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			if (cbw->bLUN != cbw->commandBlock.requestSense.lun)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (SCSI_RequestSense(
					BOT_GetDrive(),
					&cbw->commandBlock.requestSense,
					(SCSI_SenseResponse_T *)dataBuffer))
			{
				if (cbw->commandBlock.requestSense.allocLen < sizeof(SCSI_SenseResponse_T))
					*dataLength = cbw->commandBlock.requestSense.allocLen;
				else
					*dataLength = sizeof(SCSI_SenseResponse_T);
				
				return MSC_BOT_CMD_STATUS_SEND_DATA;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
	
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			return MSC_BOT_CMD_STATUS_PASSED;
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_Inquiry(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			if (cbw->bLUN != cbw->commandBlock.inquiry.lun)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (SCSI_GetInquiry(
				BOT_GetDrive(),
				&cbw->commandBlock.inquiry,
				dataBuffer))
			{
				*dataLength = SCSI_GetInquirySize(BOT_GetDrive(), &cbw->commandBlock.inquiry);
				
				return MSC_BOT_CMD_STATUS_SEND_DATA;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
	
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			return MSC_BOT_CMD_STATUS_PASSED;
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_ReadFormatCapacities(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			SCSI_ReadCapacity10Resp_T capacity;
			if (cbw->bLUN != cbw->commandBlock.inquiry.lun)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (SCSI_ReadCapacity10(BOT_GetDrive(), &cbw->commandBlock.capacity, &capacity))
			{
				dataBuffer[0] = 0x00;
				dataBuffer[1] = 0x00;
				dataBuffer[2] = 0x00;
				dataBuffer[3] = 0x08;
				
				memcpy(&dataBuffer[4], capacity.blockCnt, 4);
				
				dataBuffer[8] = 0x02;//0x08;
				
				memcpy(&dataBuffer[9], &capacity.bytesPerBlock[1], 3);
								
				*dataLength = 12;
				
				return MSC_BOT_CMD_STATUS_SEND_DATA;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
	
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			return MSC_BOT_CMD_STATUS_PASSED;
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_ReadCapacity10(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			if (cbw->bLUN != cbw->commandBlock.inquiry.lun)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (SCSI_ReadCapacity10(
				BOT_GetDrive(),
				&cbw->commandBlock.capacity,
				(SCSI_ReadCapacity10Resp_T *)dataBuffer))
			{
				*dataLength = sizeof(SCSI_ReadCapacity10Resp_T);
				return MSC_BOT_CMD_STATUS_SEND_DATA;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
	
		case MSC_BOT_CMD_STATUS_SEND_DATA:
			return MSC_BOT_CMD_STATUS_PASSED;
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_Read10(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			/* case 10 : Ho <> Di */		
			if (!(cbw->bmFlags & 0x80))
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
//			else if (cbw->bLUN != cbw->commandBlock.read.lun)
//			{
//				return MSC_BOT_CMD_STATUS_FAILED;
//			}
			else if (!BOT_GetDrive()->IsReady(cbw->bLUN))
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else 
			{
				mscBot->scsi_blk_addr	= (cbw->commandBlock.read.blockAddress[0] << 24)
												| (cbw->commandBlock.read.blockAddress[1] << 16)
												| (cbw->commandBlock.read.blockAddress[2] << 8)
												| (cbw->commandBlock.read.blockAddress[3] << 0);
									   
				mscBot->scsi_blk_len	= (cbw->commandBlock.read.xferLen[0] << 8)
											| (cbw->commandBlock.read.xferLen[1] << 0);
				
				
				SCSI_Read10CDB_T partialReadCDB = {0};
				memcpy(&partialReadCDB, &cbw->commandBlock.read, sizeof(SCSI_Read10CDB_T));
				
				uint16_t xferLen = mscBot->scsi_blk_len;
					
				if (xferLen > MSC_BLOCKS_PER_PACKET)
				{
					xferLen = MSC_BLOCKS_PER_PACKET;
					partialReadCDB.xferLen[0] = MSC_BLOCKS_PER_PACKET >> 8;
					partialReadCDB.xferLen[1] = MSC_BLOCKS_PER_PACKET & 0xFF;
				}
			
				if (SCSI_Read10(BOT_GetDrive(), &partialReadCDB, dataBuffer))
				{
					*dataLength = xferLen * 512;
					
					mscBot->scsi_blk_addr += xferLen;
					mscBot->scsi_blk_len -= xferLen;
					
					return MSC_BOT_CMD_STATUS_SEND_DATA;
				}
				else
				{
					return MSC_BOT_CMD_STATUS_ABORT;
				}
			}
		}
	
		case MSC_BOT_CMD_STATUS_SEND_DATA:
		{
			if (mscBot->scsi_blk_len == 0)
				return MSC_BOT_CMD_STATUS_PASSED;
			
			SCSI_Read10CDB_T partialReadCDB = {0};
			memcpy(&partialReadCDB, &cbw->commandBlock.read, sizeof(SCSI_Read10CDB_T));
					
			partialReadCDB.blockAddress[0] = (mscBot->scsi_blk_addr & 0xFF000000) >> 24;
			partialReadCDB.blockAddress[1] = (mscBot->scsi_blk_addr & 0x00FF0000) >> 16;
			partialReadCDB.blockAddress[2] = (mscBot->scsi_blk_addr & 0x0000FF00) >> 8;
			partialReadCDB.blockAddress[3] = (mscBot->scsi_blk_addr & 0x000000FF) >> 0;

			uint16_t xferLen = mscBot->scsi_blk_len;
				
			if (xferLen > MSC_BLOCKS_PER_PACKET)
				xferLen = MSC_BLOCKS_PER_PACKET;
			
			partialReadCDB.xferLen[0] = xferLen >> 8;
			partialReadCDB.xferLen[1] = xferLen & 0xFF;
			
			if (SCSI_Read10(BOT_GetDrive(), &partialReadCDB, dataBuffer))
			{
				*dataLength = xferLen * 512;
				
				mscBot->scsi_blk_addr += xferLen;
				mscBot->scsi_blk_len -= xferLen;
				
				return MSC_BOT_CMD_STATUS_SEND_DATA;
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}

static MSC_BOT_Result_T BOT_SCSI_Write10(
	MSC_BOT_CBW_T *cbw,
	MSC_BOT_Result_T lastCmdStatus,
	uint8_t *dataBuffer,
	uint32_t *dataLength)
{
	MSC_BOT_T *mscBot = MSC_BOT_GetMscBot();
	
	switch (lastCmdStatus)
	{
		case MSC_BOT_CMD_STATUS_RECEIVED:
		{
			/* case 10 : Ho <> Di */		
			if (cbw->bmFlags & 0x80)
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
//			else if (cbw->bLUN != cbw->commandBlock.write.lun)
//			{
//				return MSC_BOT_CMD_STATUS_FAILED;
//			}
			else if (!BOT_GetDrive()->IsReady(cbw->bLUN))
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else if (BOT_GetDrive()->IsWriteProtected(cbw->bLUN))
			{
				return MSC_BOT_CMD_STATUS_FAILED;
			}
			else 
			{
				mscBot->scsi_blk_addr	= (cbw->commandBlock.write.blockAddress[0] << 24)
										| (cbw->commandBlock.write.blockAddress[1] << 16)
										| (cbw->commandBlock.write.blockAddress[2] << 8)
										| (cbw->commandBlock.write.blockAddress[3] << 0);
					   
				mscBot->scsi_blk_len	= (cbw->commandBlock.write.xferLen[0] << 8)
										| (cbw->commandBlock.write.xferLen[1] << 0);
				
				
				SCSI_Write10CDB_T partialWriteCDB = {0};
				memcpy(&partialWriteCDB, &cbw->commandBlock.write, sizeof(SCSI_Write10CDB_T));
				
				uint16_t xferLen = mscBot->scsi_blk_len;
					
				if (xferLen > MSC_BLOCKS_PER_PACKET)
					xferLen = MSC_BLOCKS_PER_PACKET;
				
				*dataLength = xferLen * 512;
				
				return MSC_BOT_CMD_STATUS_RECEIVE_DATA;
			}
		}
	
		case MSC_BOT_CMD_STATUS_RECEIVE_DATA:
		{
			SCSI_Write10CDB_T partialWriteCDB = {0};
			memcpy(&partialWriteCDB, &cbw->commandBlock.write, sizeof(SCSI_Write10CDB_T));
					
			partialWriteCDB.blockAddress[0] = (mscBot->scsi_blk_addr & 0xFF000000) >> 24;
			partialWriteCDB.blockAddress[1] = (mscBot->scsi_blk_addr & 0x00FF0000) >> 16;
			partialWriteCDB.blockAddress[2] = (mscBot->scsi_blk_addr & 0x0000FF00) >> 8;
			partialWriteCDB.blockAddress[3] = (mscBot->scsi_blk_addr & 0x000000FF) >> 0;

			uint16_t xferLen = mscBot->scsi_blk_len;
				
			if (xferLen > MSC_BLOCKS_PER_PACKET)
			{
				xferLen = MSC_BLOCKS_PER_PACKET;
			}
			
			partialWriteCDB.xferLen[0] = xferLen >> 8;
			partialWriteCDB.xferLen[1] = xferLen & 0xFF;
			
			if (SCSI_Write10(BOT_GetDrive(), &partialWriteCDB, dataBuffer))
			{
				mscBot->scsi_blk_addr += xferLen;
				mscBot->scsi_blk_len -= xferLen;
				
				if (mscBot->scsi_blk_len > MSC_BLOCKS_PER_PACKET)
				{
					*dataLength = MSC_BLOCKS_PER_PACKET * 512;
					return MSC_BOT_CMD_STATUS_RECEIVE_DATA;
				}
				else if (mscBot->scsi_blk_len > 0)
				{
					*dataLength = mscBot->scsi_blk_len * 512;
					return MSC_BOT_CMD_STATUS_RECEIVE_DATA;
				}
				else
				{
					return MSC_BOT_CMD_STATUS_PASSED;
				}
			}
			else
			{
				return MSC_BOT_CMD_STATUS_ABORT;
			}
		}
		
		default:
			return MSC_BOT_CMD_STATUS_ABORT;
	}
}
