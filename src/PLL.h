#ifndef PLL_H
#define PLL_H

#include <stdint.h>

void PLL_Init(void);
uint32_t PLL_GetHCLK(void);

#endif
