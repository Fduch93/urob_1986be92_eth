#ifndef scpi_callbacks_h
#define scpi_callbacks_h

char *idn(char *command, int length);
char *status(char *command, int length);

#endif
