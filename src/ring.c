#include "ring.h"

char RingIsEmpty(Ring_T *ring)
{
	return ring->readPos == ring->writePos;
}

char RingWrite(Ring_T *ring, const uint8_t* buf, uint16_t len)
{
	for (int i = 0; i < len; i++)
	{
		ring->buf[ring->writePos++] = buf[i];
		if (ring->writePos >= ring->size)
			ring->writePos -= ring->size;
		if (ring->readPos == ring->writePos)
			ring->readPos++;
	}
	return 1;
}

uint16_t RingRead(Ring_T *ring, uint8_t* buf, uint16_t maxLen)
{
	if (RingIsEmpty(ring))
		return 0;
	
	for (int i = 0; i < maxLen; i++)
	{
		buf[i] = ring->buf[ring->readPos++];
		
		if (ring->readPos >= ring->size)
			ring->readPos -= ring->size;

		if (ring->readPos == ring->writePos)
			return i+1;
	}
	
	return maxLen;
}
