#include "MDR32Fx.h"

#include "GPIO.h"
#include "PLL.h"
#include "TICK.h"
#include "SSP.h"
#include "UART.h"

#include "network.h"
#include "ip.h"

#include "log.h"

#include "usbd.h"
#include "swap.h"

static uint8_t buffer[1024];

#include "SCPI.h"
#include "usbd_cdc_data_itf.h"
#include <string.h>

int	main(void)
{
	GPIO_Init(); if (MDR_PORTF->RXTX & (1<<6)) while(1);
	PLL_Init();
	TICK_Init();
	SSP_Init();
	UART_Init();
	LOG("MCU initialization complete");
	
//	SWAP_Init();
	
	USBD_SetMode(USBD_MODE_CDC);
	LOG("USBD initialization complete");

	NET_Init();
	LOG("NET initialization complete");

	while (1)
	{
		NET_Process();

		uint32_t ip = IP_GetAddr();
		if (ip)
		{
			LOG_ONCE("IP address = %a", ip);
			MDR_PORTE->RXTX |= 0x08U;
		}
		else
		{
			MDR_PORTE->RXTX &= ~0x08U;
		}

		if (TICK % 500 > 250 && !(MDR_PORTE->RXTX & 1))
			MDR_PORTE->RXTX |= 0x01U;
		else if (MDR_PORTE->RXTX & 1U)
			MDR_PORTE->RXTX &= ~0x01U;
		
		uint16_t cnt = rxRead(buffer, sizeof(buffer));
		while (cnt && !VCP_Transmit(buffer, cnt));
	}
}

void CDC_DataReceivedHandler(uint8_t *buf, uint32_t len)
{
	txWrite(buf, len);
//	const char *response = SCPI_HandleCommand((char *)buf, len - 1);
//	if (response)
//		VCP_Transmit((const uint8_t *)response, (uint16_t)strlen(response));
}

void CDC_DataSentHandler(void) {};
