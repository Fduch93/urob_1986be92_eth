#include "num2str.h"
#include <string.h>

void num2hex(char *out, uint32_t num)
{
	char str[11] = "0000000000\0";

	if (num == 0)
	{
		strcat(out, &str[9]);
	}
	else
	{
		char *c = &str[10];

		for (uint32_t div = 1; c > str; div *= 16)
		{
			uint32_t quantient = (num / div);
			if (quantient > 0 || div == 1)
			{
				uint8_t digit = quantient % 16;
				if (digit < 10)
					*(--c) = '0' + digit;
				else
					*(--c) = 'A' + digit - 10;
			}
			else break;
		}

		strcat(out, c);
	}
}

void num2dec(char *out, uint32_t num)
{
	char str[11] = "0000000000\0";

	if (num == 0)
	{
		strcat(out, &str[9]);
	}
	else
	{
		char *c = &str[10];

		for (uint32_t div = 1; c > str; div *= 10)
		{
			uint32_t quantient = (num / div);
			if (quantient > 0 || div == 1)
			{
				*(--c) = '0' + quantient % 10;
			}
			else break;
		}

		strcat(out, c);
	}
}

void num2str(char *out, uint32_t num, uint8_t base)
{
	if (base == 10)
		num2dec(out, num);
	else if (base == 16)
		num2hex(out, num);
}
