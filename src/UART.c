#include "UART.h"
#include "MDR32Fx.h"

#include "PLL.h"
#include "SCPI.h"
#include "ring.h"

#define UART MDR_UART2

#define RD 5
#define WR 2
#define DATA_ETH 0
#define DATA_8	 3
#define DATA_16	 4
#define DATA_32	 5
#define DATA_PAR 6
#define DATA_SEQ 7

#define RX_NOT_EMPTY (!(UART->FR & 1<<4))
#define TX_NOT_FULL (!(UART->FR & 1<<5))

static char rxWrite(const uint8_t* buf, uint16_t len);

void UART_Init(void)
{
	MDR_RST_CLK->PER_CLOCK	|=  1 << 7;	// UART2_EN
	MDR_RST_CLK->UART_CLOCK	|=	1 << RST_CLK_UART_CLOCK_UART2_CLK_EN_Pos	// UART2_CLK_EN
							|	0 << RST_CLK_UART_CLOCK_UART2_BRG_Pos;		// UART2_BRG = pclock_divisor - 1

//	UART->IBRD	= 34;					// 115200 = 64 MHz / 16 / (34 +
//	UART->FBRD	= 46;					// 								+ 46/64) = 115211,52115211(...)
	UART->LCR_H  = 0 << 0				// don't send break
				 | 0 << 1				// disable parity
				 | 1 << 2				// parity even
				 | 0 << 3				// one stop bit
				 | 0 << 4				// FIFO
				 | 3 << 5				// 8bit data
				 | 0 << 7;				// no forced parity
	UART_SetBaudRate(9600);
	
	UART->IFLS  = 0 << 0				// Tx FIFO threshold 1/2 of 16 = 8 bytes
				| 0 << 3;				// Rx FIFO threshold 1/2 of 16 = 8 bytes
	UART->IMSC  = 1 << 4				// Rx IRQ
				| 1 << 5				// Tx IRQ
				| 1 << 6				// Rx timeout IRQ
				| 0 << 7				// frame error IRQ
				| 0 << 8				// parity error IRQ
				| 0 << 9 				// break IRQ enable
				| 0 << 10;				// overflow IRQ enable
	UART->DMACR = 0 << 0				// Rx DMARQ
				| 0 << 1				// Tx DMARQ
				| 0 << 2;				// error DMARQ
	UART->CR	= 1 << 0				// UART enable
				| 0 << 1				// no IrDA
				| 1 << 7				// Rx-Tx short cicuit enable
				| 1 << 8				// Tx enable
				| 1 << 9;				// Rx enable
	while (UART->DR & 0xff) UART->DR = 0;
	while (!(UART->FR & 1<<4)) UART->DR;
	UART->CR	= 1 << 0				// UART enable
				| 0 << 1				// no IrDA
				| 0 << 7				// Rx-Tx short cicuit disable
				| 1 << 8				// Tx enable
				| 1 << 9;				// Rx enable
				
	NVIC_EnableIRQ(UART2_IRQn);			// enable UART IRQ handler
}

void UART_SetBaudRate(uint32_t baud)
{
	uint32_t uartClk = PLL_GetHCLK();
	uint32_t ibrd = uartClk / 16 / baud;
	uint32_t fbrd = 4*uartClk/baud - 64*ibrd;
	
	UART->CR = 0;
	
	UART->IBRD = ibrd;
	UART->FBRD = fbrd;
	UART->LCR_H = UART->LCR_H;
	
	UART->CR	= 1 << 0				// UART enable
				| 0 << 1				// no IrDA
				| 0 << 7				// Rx-Tx short cicuit disable
				| 1 << 8				// Tx enable
				| 1 << 9;				// Rx enable
}

static uint8_t rx[512];
static Ring_T rxRing = {
	.buf = rx,
	.size = sizeof(rx),
	.readPos = 0,
	.writePos = 0
};

char rxIsEmpty(void)
{
	return RingIsEmpty(&rxRing);
}

uint16_t rxRead(uint8_t* buf, uint16_t maxLen)
{
	return RingRead(&rxRing, buf, maxLen);
}

static char rxWrite(const uint8_t* buf, uint16_t len)
{
	return RingWrite(&rxRing, buf, len);
}

static uint8_t tx[512];
static Ring_T txRing = {
	.buf = tx,
	.size = sizeof(tx),
	.readPos = 0,
	.writePos = 0
};

static uint16_t txRead(uint8_t* buf, uint16_t maxLen)
{
	return RingRead(&txRing, buf, maxLen);
}

char txWrite(const uint8_t* buf, uint16_t len)
{
	if (RingWrite(&txRing, buf, len))
	{
		uint8_t wr = 0;
		if (txRead(&wr, 1) == 1 && TX_NOT_FULL)
			MDR_UART2->DR = wr;
		
		return 1;
	}
	else
		return 0;
}

void UART2_IRQHandler(void)
{
	while (RX_NOT_EMPTY)
	{
		uint8_t rd = UART->DR;
		rxWrite(&rd, 1);
	}
	
	while (TX_NOT_FULL)
	{
		uint8_t wr = 0;
		if (txRead(&wr, 1) == 1)
			MDR_UART2->DR = wr;
		else
			break;
	}
		
	if (UART->MIS & 1<<4)		// if Rx interrupt
	{
		UART->ICR = 1<<4;		// clear Rx interrupt flag
	}
	if (UART->MIS & 1<<6)		// if Rx timeout interrupt
	{
		UART->ICR = 1<<6;		// clear Rx timeout interrupt flag
	}
	if (UART->MIS & 1<<5)		// if Tx interrupt
	{
		UART->ICR = 1<<5;		// clear Tx interrupt flag
	}
}

/*
static void _UART_IRQHandler(MDR_UART_TypeDef *UART)
{
	if (UART->MIS & 1<<4){		// if Rx interrupt
		while (!(UART->FR & 1<<4))
		{
			rx[rxCnt] = UART->DR;
			if (rx[rxCnt] == '\n')
			{
				char *response = SCPI_HandleCommand((char *)rx, rxCnt);
				while (*response)
				{
					while (UART->FR & 1<<5);
					UART->DR = *response;
					response++;
				}
				rxCnt = 0;
			}
			else
			{
				rxCnt++;
				rxCnt %= 1024;
			}
		}

		UART->ICR = 1<<4;		// clear Rx interrupt flag
	}
	if (UART->MIS & 1<<6)		// if Rx timeout interrupt
	{
		while (UART->FR ^ 1<<4)
		{
			rx[rxCnt] = UART->DR;
			if (rx[rxCnt] == '\n')
			{
				char *response = SCPI_HandleCommand((char *)rx, rxCnt - 1);
				while (response)
				{
					while (UART->FR & 1<<5);
					UART->DR = *response;
					response++;
				}
				rxCnt = 0;
			}
			else
			{
				rxCnt++;
			}
		}
		UART->ICR = 1<<6;		// clear Rx timeout interrupt flag
	}
	if (UART->MIS & 1<<5)		// if Tx interrupt
	{
		UART->ICR = 1<<5;		// clear Tx interrupt flag
	}
}
*/
