#ifndef TICK_H
#define TICK_H

#include <stdint.h>

void TICK_Init(void);
void TICK_Delay(uint32_t ms);
uint32_t TICK_GetTick(void);
uint32_t TICK_GetSeconds(void);
uint32_t TICK_GetHCLK(void);

#define TICK		TICK_GetTick()
#define DELAY(m)	TICK_Delay(m)
#define SECONDS		TICK_GetSeconds()

#endif
