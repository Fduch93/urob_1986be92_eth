#ifndef UART_H
#define UART_H

#include <stdint.h>

void UART_Init(void);
void UART_SetBaudRate(uint32_t baud);

char rxIsEmpty(void);
uint16_t rxRead(uint8_t* buf, uint16_t maxLen);
char txWrite(const uint8_t* buf, uint16_t len);

#endif
