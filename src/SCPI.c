#include "SCPI.h"

static void upperCase(char *command, uint16_t length);
static int getCommandIndex(char *command, uint16_t length);

char *SCPI_HandleCommand(char *command, uint16_t length)
{
	const SCPI_Callback_T *callbacks = SCPI_GetCallbacks();

	upperCase(command, length);
	int commandIndex = getCommandIndex(command, length);
	if (commandIndex > -1)
	{
		for (int i = 0; callbacks[commandIndex].command[i]; i++, command++, length--); //skip command name

		char *response = callbacks[commandIndex].callback(command, length);

		return response;
	}
	else
	{
		return 0;
	}
}

static void upperCase(char *command, uint16_t length)
{
	static const char upperCaseShift = 'a' - 'A';

	for (int i = 0; i < length; i++)
	{
		char chr = command[i];
		if ('a' <= chr && chr <= 'z')
			command[i] = chr - upperCaseShift;
	}
}

static int getCommandIndex(char *command, uint16_t length)
{
	const SCPI_Callback_T *callbacks = SCPI_GetCallbacks();
	const uint32_t callbackCnt = SCPI_GetCallbackCount();

	for (uint32_t i = 0; i < callbackCnt; i++)
	{
		for (uint16_t c = 0; c < length && callbacks[i].command[c]; c++)
		{
			if (callbacks[i].command[c] != command[c]) // command differs from Commands[i]
				break;
			if (callbacks[i].command[c+1] == 0 && c == length - 1) // command is Commands[i]
				return i;
			if (callbacks[i].command[c+1] == 0 && callbacks[i].command[c] == ':') // command is Commands[i] with params
				return i;
		}
	}
	return -1; // command not found
}
