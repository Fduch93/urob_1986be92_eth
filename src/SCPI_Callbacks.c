#include "SCPI.h"

static char *SCPI_Idn(char *command, uint16_t length);

static const SCPI_Callback_T SCPI_Callbacks[] =
{
	{"*IDN?",	*SCPI_Idn}
};

const SCPI_Callback_T *SCPI_GetCallbacks(void)
{
	return SCPI_Callbacks;
}

uint32_t SCPI_GetCallbackCount(void)
{
	return sizeof(SCPI_Callbacks) / sizeof(SCPI_Callback_T);
}

static char *SCPI_Idn(char *command, uint16_t length)
{
	return "MDR32 Web Server\n";
}
