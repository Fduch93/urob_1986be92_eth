#include "TICK.h"
#include "MDR32Fx.h"

void TICK_Init()
{
	MDR_RST_CLK->PER_CLOCK	|= 1 << 14;		// TIM1 clock enable
	MDR_RST_CLK->TIM_CLOCK	&= ~RST_CLK_TIM_CLOCK_TIM1_BRG_Msk;
	MDR_RST_CLK->TIM_CLOCK	|= 4 << RST_CLK_TIM_CLOCK_TIM1_BRG_Pos; //TIM1_CLK = 80MHz/16 = 5MHz
	MDR_RST_CLK->TIM_CLOCK	|= RST_CLK_TIM_CLOCK_TIM1_CLK_EN;

	MDR_TIMER1->PSG = 5000-1;				// TIM counter prescaler = 5MHz / 5000 = 1 kHz
	MDR_TIMER1->ARR = 0xFFFF;				// up to 65536 ms, 1 ms per tick
	while (MDR_TIMER1->CNTRL & TIMER_CNTRL_WR_CMPL);

	MDR_TIMER1->CNTRL	= 0 << 0 			// counter enable
						| 0 << 1			// preloaded ARR
						| 0 << 2			// PSC and ARR written and ready
						| 0 << 3			// direction
						| 0 << 4			// FTDS
						| 0 << 6			// no monkey buisness w/DIR
						| 0 << 8;			// no event generation
	while (MDR_TIMER1->CNTRL & TIMER_CNTRL_WR_CMPL);

	MDR_TIMER1->CNTRL	|= 1 << 0; 			// counter enable

	MDR_RST_CLK->PER_CLOCK	|= 1 << 15;			// TIM2 clock enable
	MDR_RST_CLK->TIM_CLOCK	|= RST_CLK_TIM_CLOCK_TIM2_CLK_EN;
	MDR_RST_CLK->TIM_CLOCK	&= ~RST_CLK_TIM_CLOCK_TIM2_BRG_Msk;
	MDR_RST_CLK->TIM_CLOCK	|= 7 << RST_CLK_TIM_CLOCK_TIM2_BRG_Pos; //TIM2_CLK = 80MHz/128 = 625kHz

	MDR_TIMER2->PSG = 62500-1;					// TIM counter prescaler = 625kHz / 62500 = 10 Hz
	MDR_TIMER2->ARR = 0xFFFF;					// up to 8192 s, 1/10 s per tick
	while (MDR_TIMER2->CNTRL & TIMER_CNTRL_WR_CMPL);

	MDR_TIMER2->CNTRL	= 0 << 0 				// counter enable
						| 0 << 1				// preloaded ARR
						| 0 << 2				// PSC and ARR written and ready
						| 0 << 3				// direction
						| 0 << 4				// FTDS
						| 0 << 6				// no monkey buisness w/DIR
						| 0 << 8;				// count TIM1 ovewflow events
	while (MDR_TIMER2->CNTRL & TIMER_CNTRL_WR_CMPL);

	MDR_TIMER2->CNTRL	|= 1 << 0; 				// counter enable
	
	MDR_RST_CLK->PER_CLOCK	|= 1 << 16;		// TIM3 clock enable
	MDR_RST_CLK->TIM_CLOCK	|= RST_CLK_TIM_CLOCK_TIM3_CLK_EN;

	MDR_TIMER3->PSG = 80-1;					// TIM counter prescaler = us
	MDR_TIMER3->ARR = 0xFFFF;				// 
	while (MDR_TIMER3->CNTRL & TIMER_CNTRL_WR_CMPL);

	MDR_TIMER3->CNTRL	= 0 << 0 			// counter enable
						| 0 << 1			// preloaded ARR
						| 0 << 2			// PSC and ARR written and ready
						| 0 << 3			// direction
						| 0 << 4			// FTDS
						| 0 << 6			// no monkey buisness w/DIR
						| 0 << 8;			// count TIM_CLK ( == HCLK )

	MDR_TIMER3->CNTRL	|= 1 << 0; 			// counter enable
}

void TICK_Delay(uint32_t ms)
{
	uint32_t now = MDR_TIMER1->CNT;
	uint32_t until = now + 1 + ms;
	until %= MDR_TIMER1->ARR;
	if (now < until)
	{
		while(MDR_TIMER1->CNT < until);
	}
	else
	{
		while(MDR_TIMER1->CNT > until);
		while(MDR_TIMER1->CNT < until);
	}
}

uint32_t TICK_GetTick(void)
{
	return MDR_TIMER1->CNT;
}

uint32_t TICK_GetSeconds(void)
{
	return MDR_TIMER2->CNT / 10;// >> 3;
}

uint32_t TICK_GetHCLK(void)
{
	return MDR_TIMER3->CNT;
}
