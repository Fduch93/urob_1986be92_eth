#include "GPIO.h"
#include "MDR32Fx.h"

#define	in	0U
#define out	1U

#define prt 0U
#define pri 1U
#define alt 2U
#define ovr 3U

#define alg 0U
#define dig 1U

#define no	0U
#define up	1U
#define dwn	1U

#define pp	0U
#define odr	1U

#define slw 1U
#define mid 2U
#define fst 3U

#define shm 1U
#define flt 1U

#define P16(a,b,c,d, e,f,g,h, i,j,k,l, m,n,o,p) (unsigned)				\
	((a<<0)|(b<<1)|(c<< 2)|(d<< 3)|(e<< 4)|(f<< 5)|(g<< 6)|(h<< 7)|		\
	 (i<<8)|(j<<9)|(k<<10)|(l<<11)|(m<<12)|(n<<13)|(o<<14)|(p<<15))
#define P32(a,b,c,d, e,f,g,h, i,j,k,l, m,n,o,p) (unsigned)				\
	((a<< 0)|(b<< 2)|(c<< 4)|(d<< 6)|(e<< 8)|(f<<10)|(g<<12)|(h<<14)|	\
	 (i<<16)|(j<<18)|(k<<20)|(l<<22)|(m<<24)|(n<<26)|(o<<28)|(p<<30))

//#define PORTA
//#define PORTB
//#define PORTC
#define PORTD
#define PORTE
#define PORTF

void GPIO_Init()
{
#ifdef  PORTA
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 21;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTA->OE		= P16(	in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTA->FUNC		= P32(	prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTA->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTA->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTA->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTA->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTA->PWR		= P32(	slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw	);		// slw/mid/fst
	MDR_PORTA->RXTX		= P16(	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
#ifdef  PORTB
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 22;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTB->OE		= P16(	in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTB->FUNC		= P32(	prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTB->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTB->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTB->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTB->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	pp ,pp ,pp ,pp ,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTB->PWR		= P32(	fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst	);		// slw/mid/fst
	MDR_PORTB->RXTX		= P16(	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
#ifdef  PORTC
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 23;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTC->OE		= P16(	in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTC->FUNC		= P32(	prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTC->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTC->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTC->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTC->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	pp ,pp ,pp ,pp ,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTC->PWR		= P32(	fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst,fst	);		// slw/mid/fst
	MDR_PORTC->RXTX		= P16(	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
#ifdef  PORTD
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 24;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTD->OE		= P16(	in ,out,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTD->FUNC		= P32(	alt,alt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTD->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTD->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTD->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTD->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	odr,pp ,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTD->PWR		= P32(	fst,fst,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw	);		// slw/mid/fst
	MDR_PORTD->RXTX		= P16(	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
//								UART2_RX
//									UART2_TX
#ifdef  PORTE
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 25;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTE->OE		= P16(	out,out,out,out,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTE->FUNC		= P32(	prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTE->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,alg,alg,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTE->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTE->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTE->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	pp ,pp ,pp ,pp ,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTE->PWR		= P32(	fst,fst,fst,fst,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw	);		// slw/mid/fst
	MDR_PORTE->RXTX		= P16(	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
//								LED4	LED2			OSC32_IN
//									LED3	LED1			OSC32_OUT
//				!!!ALL PACKAGE AVAILABLE PINS ARE USED!!!
#ifdef  PORTF
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 29;
	//////////////////////////	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15
	MDR_PORTF->OE		= P16(	out,out,out,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in ,in	);		// in/out
	MDR_PORTF->FUNC		= P32(	alt,alt,prt,alt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt,prt	);		// prt/pri/alt/ovr
	MDR_PORTF->ANALOG 	= P16(	dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig,dig );		// dig/alg
	MDR_PORTF->PULL		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15 	// up/no
						| P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// dwn/no
	MDR_PORTF->GFEN		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	);		// flt/no
	MDR_PORTF->PD		= P16(	no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no ,no	)<<15	// shm/no
						| P16(	pp ,pp ,pp ,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr,odr	);		// pp/od
	MDR_PORTF->PWR		= P32(	fst,fst,fst,fst,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw,slw	);		// slw/mid/fst
	MDR_PORTF->RXTX		= P16(	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	);
#endif
//								SSP1_TX					LOAD_BTN
//									SSP1_CLK
//										SSP1_CS
//											SSP1_RX
}
