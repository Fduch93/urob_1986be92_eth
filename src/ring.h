#ifndef RING_H
#define RING_H

#include <stdint.h>

typedef struct
{
	uint16_t readPos;
	uint16_t writePos;
	uint16_t size;
	uint8_t *buf;
} Ring_T;

char RingIsEmpty(Ring_T *ring);
char RingWrite(Ring_T *ring, const uint8_t* buf, uint16_t len);
uint16_t RingRead(Ring_T *ring, uint8_t* buf, uint16_t maxLen);

#endif
