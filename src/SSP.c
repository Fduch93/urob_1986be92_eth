#include "SSP.h"
#include "MDR32Fx.h"

void SSP_Init()
{
	MDR_RST_CLK->PER_CLOCK	|= 	1 << 8;
	MDR_RST_CLK->SSP_CLOCK	|=	1 << 24	// SSP1_CLK_EN
							|	0 << 0; // SSP1_BRG = pclock_divisor - 1
	MDR_SSP1->CPSR = 2;					// ClkPreScalerReg >= 2 and always even
	MDR_SSP1->DMACR	= 0 << 0			// Rx DMARQ enable
					| 0 << 1;			// Tx DMARQ enable
	MDR_SSP1->CR0 =	7 << 0				// DSS = data bits per frame - 1
				  |	0 << 4				// Motorola SPI mode
				  |	0 << 6				// data clock polarity
				  | 0 << 7				// data clock phase
				  | (4-1)<< 8;			// data clock divisor - 1 // = 64 MHz / pclk_div / CPSR / [8MHz data clock]
	MDR_SSP1->CR1 =	0 << 0				// no cable test
				  | 1 << 1				// TxRx enable
				  | 0 << 2				// master mode
				  | 0 << 3;				// no Tx drive in slave mode
}
