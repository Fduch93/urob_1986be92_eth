#ifndef SCPI_H
#define SCPI_H

#include <stdint.h>

typedef struct {
	char *command;
	char *(*callback)(char *command, uint16_t length);
} SCPI_Callback_T;

char *SCPI_HandleCommand(char *command, uint16_t length);

extern const SCPI_Callback_T *SCPI_GetCallbacks(void);
extern uint32_t SCPI_GetCallbackCount(void);

#endif
