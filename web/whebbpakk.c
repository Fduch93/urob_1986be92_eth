#include "whebbpakk.h"
#include <string.h>

static const char mimeJs[] = "text/javascript";
static const char mimeHtml[] = "text/html";
static const char mimeCss[] = "text/css";

static const WP_Resource_T resources[] = {
	{
		.path = "/logHandler.js", .mime = mimeJs,
		.rsrc = "var logCnt=0;function logg(line){const log=document.getElementById('log');const autoscroll=log.scrollTop==log.scrollTopMax;document.getElementById('log').innerHTML+=line+'<br/>';if(autoscroll){log.scrollTop=log.scrollTopMax;}}function clearLog(){document.getElementById('log').innerHTML='';}function logHandle(logTotal){let diff=logTotal-logCnt;if(diff>100){logg('Lost'+(diff-100)+'log entries'+'<br/>');logCnt=logTotal-100;}if(logTotal>logCnt){const request=new XMLHttpRequest();request.open('POST','/log');request.timeout=50;request.onreadystatechange=()=>{if(request.readyState===4&&request.status===200){logg(request.responseText);logCnt++;logHandle(logTotal);}};request.send(''+logCnt);}}",
	},{
		.path = "/logTask.js", .mime = mimeJs,
		.rsrc = "var logTask;window.addEventListener('load',()=>{document.getElementById('log').innerHTML='';startLogTask();});function startLogTask(){clearInterval(logTask);logTask=window.setInterval(()=>{const reqLog=new XMLHttpRequest();reqLog.open('GET','/log');reqLog.timeout=100;reqLog.onreadystatechange=()=>{if(reqLog.readyState===4&&reqLog.status===200){logHandle(reqLog.responseText);}};reqLog.send();},500);}function stopLogTask(){clearInterval(logTask);}",
	},{
		.path = "/notfound.html", .mime = mimeHtml,
		.rsrc = "<html><head><link rel=\"icon\" href=\"https://milandr.ru/bitrix/templates/.default/favicon.ico\" type=\"image/x-icon\"></head><body>qwe! (not found)</body></html>",
	},{
		.path = "/root.css", .mime = mimeCss,
		.rsrc = "body{background:#CC6600;}section{display:flex;flex-flow:column;height:100%;}header{background:#ff8000;}#log{flex:2;background:#ECECEC;overflow:auto;}footer{background:#ff8000;min-height:60px;display:flex;justify-content:center;align-items:center;}",
	},{
		.path = "/", .mime = mimeHtml,
		.rsrc = "<html><head><link rel=\"icon\" href=\"https://milandr.ru/bitrix/templates/.default/favicon.ico\" type=\"image/x-icon\"><script type=\"text/javascript\" src=\"/secTask.js\"></script><script type=\"text/javascript\" src=\"/logTask.js\"></script><script type=\"text/javascript\" src=\"/logHandler.js\"></script><script type=\"text/javascript\" src=\"/toggle.js\"></script><script type=\"text/javascript\" src=\"/webSocket.js\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\"/root.css\"></head><body><section><header><input type=\"button\" onclick=\"toggle()\" value=\"Toggle LED\"/><input type=\"button\" onclick=\"clearLog()()\" value=\"Clear log\"/><br/>Background:<input type=\"button\" onclick=\"processXHR()\" value=\"XHR\"/><input type=\"button\" onclick=\"processWS()\" value=\"WebSocket\"/><br/><div id=\"sec\"></div></header><div id=\"log\"></div><footer>MDR32 Web Server</footer></section></body></html>",
	},{
		.path = "/secTask.js", .mime = mimeJs,
		.rsrc = "var sec=0;var secTask;window.addEventListener('load',()=>{document.getElementById('sec').innerHTML='seconds';startSecTask();});function startSecTask(){clearInterval(secTask);secTask=window.setInterval(()=>{const reqSec=new XMLHttpRequest();reqSec.open('GET','/sec');reqSec.timeout=100;reqSec.onreadystatechange=()=>{if(reqSec.readyState===4&&reqSec.status===200){secHandle(reqSec.responseText);}};reqSec.send();},200);}function stopSecTask(){clearInterval(secTask);}function secHandle(newSec){document.getElementById('sec').innerHTML=newSec;if((+sec>+newSec)&&!(+sec==8191&&+newSec==0)){logg('RESET DETECTED');logCnt=0;}sec=+newSec;}",
	},{
		.path = "/toggle.js", .mime = mimeJs,
		.rsrc = "function toggle(){const request=new XMLHttpRequest();request.open('POST',\"/led\");request.timeout=100;request.send();}function processXHR(){stopWebSocket();startSecTask();startLogTask();}function processWS(){stopSecTask();stopLogTask();startWebSocket();}",
	},{
		.path = "/webSocket.js", .mime = mimeJs,
		.rsrc = "var websocket;var wsLog;function startWebSocket(){stopWebSocket();websocket=new WebSocket('ws://'+location.hostname+'/ws');websocket.onmessage=(msgEv)=>{if(msgEv.data.toString().startsWith('sec')){secHandle(msgEv.data.substring(3));}else if(msgEv.data.toString().startsWith('log')){if(msgEv.data.length>3){clearInterval(wsLog);logg(msgEv.data.substring(3));logCnt++;websocket.send('log'+logCnt);wsLog=setInterval(()=>{websocket.send('log'+logCnt);},1000);}}};websocket.onerror=(errEv)=>{stopWebSocket();startWebSocket();};wsLog=setInterval(()=>{websocket.send('log'+logCnt);},1000);}function stopWebSocket(){clearInterval(wsLog);if(websocket!==undefined&&websocket.readyState<2){websocket.close();}}",
	}
};

static inline char CompareUrl(const char *path, uint16_t pathLen, const char *url);

const WP_Resource_T *WP_GetResource(const char *path, uint16_t pathLen)
{
	for (int i = 0; i < sizeof(resources)/sizeof(WP_Resource_T); i++)
	{
		if (CompareUrl(path, pathLen, resources[i].path))
			return &resources[i];
	}
	return 0;
}

static inline char CompareUrl(const char *path, uint16_t pathLen, const char *url)
{
	return !strncmp(path, url, pathLen) && pathLen == strlen(url);
}
