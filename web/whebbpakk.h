#ifndef WHEBBPAKK_H
#define WHEBBPAKK_H

#include <stdint.h>

typedef struct
{
	const char *path;
	const char *mime;
	const char *rsrc;
} WP_Resource_T;

const WP_Resource_T *WP_GetResource(const char *path, uint16_t pathLen);

#endif
