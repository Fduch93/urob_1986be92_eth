var logTask;

window.addEventListener('load', () => {
	
	document.getElementById('log').innerHTML = '';
	startLogTask();
});

function startLogTask() {
	
	clearInterval(logTask);
	
	logTask = window.setInterval( () => {
		const reqLog = new XMLHttpRequest();
		reqLog.open('GET', '/log');
		reqLog.timeout = 100;
		reqLog.onreadystatechange = () => {
			if (reqLog.readyState === 4 && reqLog.status === 200) {
				logHandle(reqLog.responseText);
			}
		};
		reqLog.send();
	}, 500);
}

function stopLogTask() {
	clearInterval(logTask);
}
