var websocket;

var wsLog;

function startWebSocket() {
	stopWebSocket();
	
	websocket = new WebSocket('ws://'+location.hostname+'/ws');
	websocket.onmessage = (msgEv) => {
		if (msgEv.data.toString().startsWith('sec')) {
			secHandle(msgEv.data.substring(3));
		} else if (msgEv.data.toString().startsWith('log')) {
			if (msgEv.data.length > 3) {
				clearInterval(wsLog);
				logg(msgEv.data.substring(3));
				logCnt++;
				websocket.send('log'+logCnt);
				wsLog = setInterval(() => {
					websocket.send('log'+logCnt);
				}, 1000);
			}
		}
	};
	websocket.onerror = (errEv) => {
		stopWebSocket();
		startWebSocket();
	};
	
	wsLog = setInterval(() => {
		websocket.send('log'+logCnt);
	}, 1000);
}

function stopWebSocket() {
	clearInterval(wsLog);
	if (websocket !== undefined && websocket.readyState < 2) {
		websocket.close();
	}
}	
