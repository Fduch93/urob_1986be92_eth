function toggle() {
	const request = new XMLHttpRequest();
	request.open('POST', "/led");
	request.timeout = 100;
	request.send();
}

function processXHR() {
	stopWebSocket();
	startSecTask();
	startLogTask();
}

function processWS() {
	stopSecTask();
	stopLogTask();
	startWebSocket();
}
