var logCnt = 0;

function logg(line) {
	const log = document.getElementById('log');
	const autoscroll = log.scrollTop == log.scrollTopMax;
	document.getElementById('log').innerHTML += line + '<br/>';
	if (autoscroll) {
		log.scrollTop = log.scrollTopMax;
	}
}

function clearLog() {
	document.getElementById('log').innerHTML = '';
}

function logHandle(logTotal) {
	let diff = logTotal - logCnt;
	if (diff > 100) {
		logg('Lost ' + (diff - 100) + ' log entries' + '<br/>');
		logCnt = logTotal - 100;
	}
	if (logTotal > logCnt)
	{
		const request = new XMLHttpRequest();
		request.open('POST', '/log');
		request.timeout = 50;
		request.onreadystatechange = () => {
			if (request.readyState === 4 && request.status === 200) {
				logg(request.responseText);
				logCnt++;
				logHandle(logTotal);
			}
		};
		request.send('' + logCnt);
	}
}
