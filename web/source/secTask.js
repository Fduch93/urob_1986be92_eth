var sec = 0;
var secTask;

window.addEventListener('load', () => {
	
	document.getElementById('sec').innerHTML = 'seconds';
	startSecTask();
});

function startSecTask() {
	
	clearInterval(secTask);
	
	secTask = window.setInterval( () => {
		const reqSec = new XMLHttpRequest();
		reqSec.open('GET', '/sec');
		reqSec.timeout = 100;
		reqSec.onreadystatechange = () => {
			if (reqSec.readyState === 4 && reqSec.status === 200) {
				secHandle(reqSec.responseText);
			}
		};
		reqSec.send();
	}, 200);
}

function stopSecTask() {
	clearInterval(secTask);
}

function secHandle(newSec) {
	document.getElementById('sec').innerHTML = newSec;
	if ((+sec > +newSec) && !(+sec == 8191 && +newSec == 0)) {
		logg('RESET DETECTED');
		logCnt = 0;
	}
	sec = +newSec;
}
