﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml;

namespace WhebbPakk
{
    static class WhebbPakk
    {
        class Entry
        {
            public string filename;
            public string value;

            public Entry(string filename, string value)
            {
                this.filename = filename;
                this.value = value;
            }
        }

        static void Main(string[] args)
        {
            if (args.Length != 2 ||
                !Directory.Exists(args[0]) ||
                !Directory.Exists(args[1]))
            {
                //Application.Exit();
                args = new string[2]
                {
                    @"F:\Projects\uROB_1986ВЕ92_ETH\web\source",
                    @"F:\Projects\uROB_1986ВЕ92_ETH\web"
                };
            }

            string srcDir = args[0];
            string dstDir = args[1];

            List<Entry> rsrcs = new List<Entry>();

            PopulateResources(srcDir, rsrcs);

            int max = rsrcs.Max(entry => entry.value.Length);
            Entry maxEntry = rsrcs.Find(entry => entry.value.Length == max);
            Console.WriteLine($"Max resource is {maxEntry.filename}, length = {max}");

            if (!File.ReadAllText(Path.Combine(dstDir, "whebbpakk.h")).Equals(header))
            {
                Console.WriteLine("Overwriting whebbpakk.h ...");
                File.WriteAllText(Path.Combine(dstDir, "whebbpakk.h"), header);
            }
            else
                Console.WriteLine("Header whebbpakk.h is up-to-date");

            string source =
                string.Format(
                    sourceFormat,
                    string.Join("\n",
                        rsrcs.Select(entry => Path.GetExtension(entry.filename))
                             .Distinct(StringComparer.OrdinalIgnoreCase)
                             .Select(ext => string.Format(
                                 mimeFormat,
                                 SelectMime(ext),
                                 SelectMimeString(ext)))),
                    string.Join(",",
                        rsrcs.Select(entry =>
                             {
                                 if (entry.filename.Equals("root.html"))
                                     entry.filename = "";
                                 return entry;
                             })
                             .Select(entry => string.Format(resourceFormat,
                                entry.filename,
                                SelectMime(entry.filename.Equals("")
                                    ? ".html"
                                    : Path.GetExtension(entry.filename)),
                                entry.value))));

            if (!File.ReadAllText(Path.Combine(dstDir, "whebbpakk.c")).Equals(source))
            {
                Console.WriteLine("Overwriting whebbpakk.c ...");
                File.WriteAllText(Path.Combine(dstDir, "whebbpakk.c"), source);
            }
            else
                Console.WriteLine("Source whebbpakk.c is up-to-date");

            Console.WriteLine("WhebbPakk complete!");
        }

        private static void PopulateResources(string srcDir, List<Entry> rsrcs)
        {
            foreach (string filename in Directory.EnumerateFiles(srcDir))
            {
                switch (Path.GetExtension(filename).ToLower())
                {
                    case ".html":
                        string html = File.ReadAllText(filename);
                        html = Regex.Replace(html, @"(\r|\n|\t)+", "");
                        html = Regex.Replace(html, @"\s+", " ");
                        html = html.Replace("\"", "\\\"");

                        rsrcs.Add(new Entry(Path.GetFileName(filename), html));
                        break;
                    case ".js":
                    case ".css":
                        string js = File.ReadAllText(filename);
                        js = Regex.Replace(js, @"(\r|\n|\t)+", "");
                        js = Regex.Replace(js, @"\B\s+|\s+\B", "");
                        js = Regex.Replace(js, @"\b\s+\b", " ");
                        js = Regex.Replace(js, @"\s+", " ");
                        js = js.Replace("\"", "\\\"");

                        rsrcs.Add(new Entry(Path.GetFileName(filename), js));
                        break;
                    default:
                        break;
                }
            }
        }

        private static string SelectMime(string ext)
        {
            switch (ext.ToLower())
            {
                case ".html":
                    return "mimeHtml"; ;
                case ".js":
                    return "mimeJs";
                case ".css":
                    return "mimeCss";
                default:
                    return "mimeData";
            }
        }
        private static string SelectMimeString(string ext)
        {
            switch (ext.ToLower())
            {
                case ".html":
                    return "text/html"; ;
                case ".js":
                    return "text/javascript";
                case ".css":
                    return "text/css";
                default:
                    return "application/octet-stream";
            }
        }

        private static readonly string header = "" +
            "#ifndef WHEBBPAKK_H\n" +
            "#define WHEBBPAKK_H\n" +
            "\n" +
            "#include <stdint.h>\n" +
            "\n" +
            "typedef struct\n" +
            "{\n" +
            "\tconst char *path;\n" +
            "\tconst char *mime;\n" +
            "\tconst char *rsrc;\n" +
            "} WP_Resource_T;\n" +
            "\n" +
            "const WP_Resource_T *WP_GetResource(const char *path, uint16_t pathLen);\n" +
            "\n" +
            "#endif\n";

        private static readonly string sourceFormat = "" +
            "#include \"whebbpakk.h\"\n" +
            "#include <string.h>\n" +
            "\n" +
            "{0}\n" +
            "\n" +
            "static const WP_Resource_T resources[] = {{\n" +
            "\t{1}\n" +
            "}};\n" +
            "\n" +
            "static inline char CompareUrl(const char *path, uint16_t pathLen, const char *url);\n" +
            "\n" +
            "const WP_Resource_T *WP_GetResource(const char *path, uint16_t pathLen)\n" +
            "{{\n" +
            "\tfor (int i = 0; i < sizeof(resources)/sizeof(WP_Resource_T); i++)\n" +
            "\t{{\n" +
            "\t\tif (CompareUrl(path, pathLen, resources[i].path))\n" +
            "\t\t\treturn &resources[i];\n" +
            "\t}}\n" +
            "\treturn 0;\n" +
            "}}\n" +
            "\n" +
            "static inline char CompareUrl(const char *path, uint16_t pathLen, const char *url)\n" +
            "{{\n" +
            "\treturn !strncmp(path, url, pathLen) && pathLen == strlen(url);\n" +
            "}}\n";

        private static readonly string mimeFormat = "static const char {0}[] = \"{1}\";";

        private static readonly string resourceFormat = "" +
            "{{\n" +
            "\t\t.path = \"/{0}\", .mime = {1},\n" +
            "\t\t.rsrc = \"{2}\",\n" +
            "\t}}";
    }
}
